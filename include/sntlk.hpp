#ifndef SNTLK_HPP
#define SNTLK_HPP
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <memory>
#include <mutex>
#include <sys/socket.h>

/*!
 * @class sntlk
 * @brief Simple NETLINK Library
 *
 * Used to communicate with the kernel for network management related operations
 */
class sntlk
{

public:
  /*!
   * @struct linkInfo
   * @brief Used as a generic means of obtaining information from the Kernel
   * back to the user
   *
   * It can store both IPv4 and IPv6 information. It is currently used by \code
   * getRoute \endcode
   */
  struct linkInfo
  {
    struct sockaddr_storage
      src; /*!< Source IP address corresponding to a RTM entry */
    struct sockaddr_storage
      dst; /*!< Destination IP address corresponding to a RTM entry */
    struct sockaddr_storage
      gateway;   /*!< Gateway IP address corresponding to a RTM entry */
    int ifindex; /*!< Interface Index corresponding to a RTM entry */
  };

public:
  /*!
   * @brief Initialize the sntlk library.
   *
   * It creates a NETLINK socket and initializes the structures for future
   * communication.
   */
  sntlk();

  /*!
   * @brief Free resources
   */
  virtual ~sntlk();

  /*!
   * @brief Obtain a route from the RTM.
   *
   * Given a struct sockaddr that can be either IPv4 or IPv6 we check if we
   * have a route for that destination retrieving the data back in linkInfo
   * structure
   *
   * @param[in] dst IP destination address - IPv4 or IPv6
   * @param[out] rtnfo Route info retrieved from the RTM
   * @return
   *    -   EXIT_SUCCESS
   *    -   EXIT_FAILURE
   */
  int_fast8_t getRoute(const struct sockaddr* dst, struct linkInfo* rtnfo);

  /*!
   * @brief Add an IP address to an interface.
   *
   * @param[in] addr IP address to get set - IPv4 or IPv6. The
   * prefixlen/masklen is given in the sin{6}_port field.
   * @param[in] ifIndex Interface index usually given with \code
   * if_nametoindex \endcode
   * @return
   *    -   EXIT_FAILURE - already set, incorrect data, or communication error
   *    -   EXIT_SUCCESS
   */
  int_fast8_t addIp(const struct sockaddr* addr, int ifIndex);

  /*!
   * @brief Delete an IP address from an interface.
   *
   * @param[in] addr IP address to get removed - IPv4 or IPv6. The
   * prefixlen/masklen is given in the sin{6}_port field.
   * @param[in] ifIndex Interface index usually given with \code
   * if_nametoindex \endcode
   * @return
   *    -   EXIT_FAILURE - address not configured, incorrect data, or
   * communication error
   *    -   EXIT_SUCCESS
   */
  int_fast8_t delIp(const struct sockaddr* addr, int ifIndex);

  /*!
   * @brief Pretty Print the linkInfo structure
   * @param[in] linfo struct linkInfo to get printed
   * @return
   *    -   EXIT_FAILURE - linfo is null
   *    -   EXIT_SUCCESS
   */
  static int_fast8_t printLinkInfo(const struct linkInfo* linfo);

private:
  std::mutex _lock;        /*!< Lock used for sensitive operations */
  int _sofd;               /*!< NETLINK socket used for communication */
  struct sockaddr_nl _snl; /*!< Socket name used to send data */
  const size_t _buffSize;  /*!< Buffer size used for read/write */
  std::unique_ptr<uint8_t[]> _buffer; /*!< Buffer used for read/write */

private:
  /*!
   * @brief Operations given to \code ipOperationHandler \endcode
   */
  enum
  {
    IP_ADD, /*!< Add IP */
    IP_DEL  /*!< Remove IP */
  };

  /*!
   * @brief Handler function used to read TLVs from RTA
   *
   * This functions is used to populate the data in linkInfo with the info
   * from RTAs
   * @param[in] rta TLV data containing RTM data
   * @param[out] rtnfo Structure that we populate with data extracted from
   * multiple RTAs
   * @return
   *    -   EXIT_SUCCESS
   *    -   EXIT_FAILURE
   */
  int_fast8_t handleRta(const struct rtattr* rta, struct linkInfo* rtnfo);

  /*!
   * @brief Backend function used to handle IP operations.
   *
   * This gets called by either \code addIp \endcode or \code delIp \endcode
   * in order to handle the operation. Much of the code-flow is the same for
   * both Add and Delete, thus we use a generic function.
   *
   * @param[in] addr IP address to get added/removed
   * @param[in] ifIndex Index of the interface on which we process the
   * operation
   * @param[in] operation Operation type - either IP_ADD or IP_DEL
   * @return
   *    -   EXIT_SUCCESS
   *    -   EXIT_FAILURE
   */
  int_fast8_t ipOperationHandler(const struct sockaddr* addr,
                                 int ifIndex,
                                 int_fast8_t operation);
};

#endif
