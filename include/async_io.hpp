#ifndef __ASYNC_IO_HPP
#define __ASYNC_IO_HPP
#include "net_utils.hpp"
#include <arpa/inet.h>
#include <cstring>
#include <map>
#include <memory>
#include <mutex>
#include <netinet/if_ether.h>
#include <sys/epoll.h>
#include <unistd.h>

/*!
 * @class packHandler
 * @brief Interface class used by 'asyncIo' to handle the file descriptors
 * @note The idea is to inherit from this class in order to overload 'process'
 */
class packHandler
{
protected:
  int _sofd{ -1 }; /**< File descriptor that could be registered in epoll */
  uint_fast32_t _events{
    0
  }; /**< Events bitmask that could be registered in epoll */

public:
  packHandler() = default;          /**< Default constructor */
  virtual ~packHandler() = default; /**< Default virtual destructor */

  /*!
   * @brief Pure virtual function used by asyncIo as callback
   * @warning Ideally this function should not block!
   * @warning IT MUST NOT CALL FUNCTIONS FROM 'asyncIo'
   * @param sofd File descriptor registered in epoll for which we can handle IO
   * @param events Events returned by epoll_wait for this specific descriptor
   * @return Currenty unused*/
  virtual uint_fast32_t process(int sofd, uint_fast32_t events) = 0;
};

/*!
 * @class asyncIo
 * @brief Singleton async file descriptor management library
 */
class asyncIo
{
private:
  std::mutex _lock;   /*!< Mutex used in sensitive areas in order to
                         add/remove/poll file descriptors */
  int _pollFd{ 0 };   /*!< File descriptor used by epoll_* family functions */
  size_t _qSize{ 0 }; /*!< Maximum queue for the epoll_events */
  /*! List of events registered by the user key: file descriptor value:
   * epoll_event */
  std::map<int, struct epoll_event> _userEvents;
  /*! Array of events returned as ready from epoll_wait */
  std::unique_ptr<struct epoll_event[]> _recvEvents;
  /*!
  * @struct Callback
  * @brief Data describing a registered event
  * @note We're keeping the callback structure even though we have sofd member
  for packHandler because we don't want to tie the packet handler class to a
  single sofd
  */
  /*! In every struct epoll_event, the data field shall host a pointer a
   * structure of this kind that has the file descriptor and handler callback.
   */
  struct Callback
  {
    int _sofd{ -1 }; /*!< Registered file descriptor*/
    packHandler* _handler{
      nullptr
    }; /*!< Callback object associated with descriptor */

    /*!
     * @brief Constructor that initializes members
     * @param sofd Registered file descriptor
     * @param p Pointer to packHandler type class that shall process data
     */
    Callback(int sofd, packHandler* p)
      : _sofd(sofd)
      , _handler(p){};
    /*!
     * @brief Default destructor
     */
    virtual ~Callback() = default;
  };

private:
  /*!
   * @brief Function that takes a file descriptor and makes it non-blocking
   * @param sofd File descriptor to be made non-blocking
   * @return
   *    -   EXIT_FAILURE
   *    -   EXIT_SUCCESS
   */
  int_fast8_t prepareSocket(int sofd);
  /*!
   * @brief Private constructor that initializes the epoll descriptor and the
   * containers keeping track of registered events
   * @param qSize Size of max possible polled events at once
   */
  asyncIo(size_t qSize);
  /*!
   * @brief Private destructor that closes the epoll fd
   */
  ~asyncIo();

public:
  /*!
   * @brief Function that polls the registered file descriptors for a period of
   * time and calls packHandler::process on ready descriptors
   * @param timeout Miliseconds that this function would block for events
   * @return
   *    -   EXIT_SUCCESS    Everything went ok or some other thread is already
   * polling
   *    -   EXIT_FAILURE    Something really wrong has happened in epoll_wait
   */
  int_fast8_t pollDescriptors(int timeout);
  /*!
   * @brief Function that registers a file descriptor with given events and
   * callback handler
   * @param sofd File descriptor for which we poll events
   * @param events The events associated with the file descriptor
   * @param pack Pointer to packHandler callback class that shall call process
   * when data is available
   * @return
   *    -   EXIT_SUCCESS
   *    -   EXIT_FAILURE    Already registered or could not register
   */
  int_fast8_t registerDescriptor(int sofd,
                                  uint_fast32_t events,
                                  class packHandler* pack);
  /*!
   * @brief Function that removes file descriptor from poll event queue
   * @param sofd File descriptor to be removed
   * @return
   *    -   EXIT_SUCCESS
   *    -   EXIT_FAILURE Could not remove or not registered
   */
  int_fast8_t unregisterDescriptor(int sofd);
  /*!
   * @brief Singleton instantiation function
   * @return
   *    -   Reference to asyncIo object
   */
  static asyncIo& getInstance(void);
};

#endif
