#ifndef DEBUG_HPP
#define DEBUG_HPP
#include <cstdarg>
#include <cstring>
#include <exception>
#include <iostream>

/*! @brief Function used for printing errors.
 *
 *  It takes arguments like printf-like functions.
 */
#if 0
#define ERROR(fmt, ...)                                                        \
  do {                                                                         \
    fprintf(stderr,                                                            \
            "%s:%d\n\t\'" fmt "\'\n",                                          \
            __PRETTY_FUNCTION__,                                               \
            __LINE__,                                                          \
            __VA_ARGS__);                                                      \
  } while (0)
#endif

/*! @brief Function used for printing errors.
 *
 *  It takes arguments like printf-like functions.
 *  This variant takes the advantage of vulnyErr's feature to print errno.
 */
#define ERROR(fmt, ...)                                                        \
  do {                                                                         \
    printf("%s",                                                               \
           vulnyErr(__PRETTY_FUNCTION__, __LINE__, fmt, __VA_ARGS__).what());  \
  } while (0)

#ifdef DEBUG

/*! @brief Debug function used to print messages.
 *  @note You activate it by defining DEBUG
 *
 *  It takes arguments like printf-like functions.
 */
#define DBG(fmt, ...)                                                          \
  do {                                                                         \
    fprintf(stderr,                                                            \
            "%s:%d\n\t\'" fmt "\'\n",                                          \
            __PRETTY_FUNCTION__,                                               \
            __LINE__,                                                          \
            __VA_ARGS__);                                                      \
  } while (0)

#else

/*! @brief Debug function used to print messages.
 *  @note You activate it by defining DEBUG
 *
 *  It takes arguments like printf-like functions.
 */
#define DBG(fmt, ...)                                                          \
  do {                                                                         \
  } while (0)

#endif // DEBUG

/*!
 * @class vulnyErr
 * @brief Exception type class used to create vulny exceptions
 *
 * Maximum message length is (512 - __PRETTY_FUNCTION__ - __LINE__)
 */
class vulnyErr : public std::exception
{
private:
  char _buffer[512]{ 0x00 }; /*!< Message buffer */
  char _errBuffer[100]{
    0x00
  }; /*!< Error message buffer populated by \code strerror_r \endcode */

public:
  /*!
   * @brief Constructor used to generate printf-like message.
   * @warning Call using VULNYEX
   * @param func __PRETTY_FUNCTION__
   * @param line __LINE__
   * @param fmt User-defined format
   * @param ... Variables
   */
  vulnyErr(const char* func, unsigned line, const char* fmt, ...)
  {
    const int savedErrno = errno;
    ssize_t buffSize = (sizeof _buffer) - 1;
    ssize_t len{ 0 };
    char* ptr = _buffer;
    const char* strErrPtr{ nullptr };

    if (savedErrno > 0) {
      strErrPtr = strerror_r(savedErrno, _errBuffer, sizeof _errBuffer);
      errno = 0;
    }

    len = snprintf(ptr, buffSize, "%s:%u\n\t\'", func, line);
    buffSize -= len;
    ptr += len;

    if (buffSize > 0 && len >= 0) {
      va_list vlist;
      va_start(vlist, fmt);
      len = vsnprintf(ptr, buffSize, fmt, vlist);
      va_end(vlist);

      buffSize -= len;
      ptr += len;

      if (buffSize > 0 && len >= 0) {
        len =
          snprintf(ptr, buffSize, "\' %c", strErrPtr != nullptr ? ':' : '\n');
        buffSize -= len;
        ptr += len;
      }

      if (buffSize > 0 && strErrPtr && len >= 0) {
        snprintf(ptr, buffSize, " errno(%d) - %s\n", savedErrno, strErrPtr);
      }
    }

    _buffer[(sizeof _buffer) - 1] = '\0';
  }

/*! @brief Macro used to generate a vulnyErr object with current function
 * and line
 *
 * @param[in] fmt Printf-like format specified by user
 * @param[in] ... Variables
 */
#define VULNYEX(fmt, ...)                                                      \
  vulnyErr(__PRETTY_FUNCTION__, __LINE__, fmt, __VA_ARGS__)

  /*! @brief Called in exception handler
   * @return
   *    -   Error message of type 'FUNCTION:LINE:Message'
   */
  virtual const char* what() const noexcept { return _buffer; }

  /*! @brief Default destructor
   */
  virtual ~vulnyErr() = default;
};

#endif // DEBUG_LIB_H
