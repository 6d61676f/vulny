#ifndef _HANDLERS_HPP
#define _HANDLERS_HPP
#include "async_io.hpp"
#include "linux/if_packet.h"
#include <map>
#include <set>
#include <thread>
#include <vector>

/*!
 * @class genericV4Handler
 * @brief Wrapper class for packHandler designed to ease the implementation of
 * IPV4 handlers
 *
 * It declares a reference to asyncIo, IPv4 source/destination, port
 * source/destination, read/write locks, read/write buffers and sizes, and
 * setters.
 */
class genericV4Handler : public packHandler
{
protected:
  /*! @brief Flags that resemble the IP modifications made by the user*/
  enum
  {
    mSrcIp = 1,  /*!< Source IP has been changed */
    mDstIp = 2,  /*!< Destination IP has been changed */
    mSrcPrt = 4, /*!< Source Port has been changed */
    mDstPrt = 8, /*!< Destination Port has been changed */
    mMax = 0xff  /*!< Max supported change value */
  };

protected:
  asyncIo& _IO;                  /*!< reference to async IO library */
  struct in_addr _ipSrc = { 0 }; /*!< Source IPv4 address      */
  struct in_addr _ipDst = { 0 }; /*!< Destination IPv4 address */
  uint16_t _dport{ 0 };          /*!< Destination port         */
  uint16_t _sport{ 0 };          /*!< Source port              */
  std::mutex _ingressLock;       /*!< Mutex used in sensitive read areas */
  std::mutex _egressLock;        /*!< Mutex used in sensitive write ares */
  bool _registered{ false };     /*!< Flag that tells if our file descriptor is
                                    registered in \code asyncIo \endcode */
  /*! Flag that describes if we have any modifications to:
   *     -   source IPv4      : mSrcIp
   *     -   destination IPv4 : mDstIp
   *     -   source port      : mSrcPrt
   *     -   destination port : mDstPrt
   */
  uint8_t _modified{ 0xff };
  const size_t _inBuffsize;              /*!< Read buffer size */
  const size_t _outBuffsize;             /*!< Write buffer size */
  std::unique_ptr<uint8_t[]> _inBuffer;  /*!< Read buffer */
  std::unique_ptr<uint8_t[]> _outBuffer; /*!< Write buffer */
  /*! Structure that keeps destination information */
  struct sockaddr_in _sockin;

protected:
  /*! @brief Process any generic user changes to the IPs or ports.
   *
   * It updates the target information and output buffer.
   * @return
   *    -   EXIT_SUCCESS
   */
  virtual int_fast8_t _processChanges(void);

public:
  /*!
   * @brief Initialize generic IPv4 handler.
   *
   * Specify the read and write buffer sizes.
   * outBuffSize should at minimum be the size of expected L4 header.
   * inBuffSize should be something like sizeof(iphdr+{udp,tcp}hdr+etc)
   * @param[in] inBuffSize Size of the buffer used to read replies
   * @param[in] outBuffSize Size of the buffer used to write requests
   */
  genericV4Handler(size_t inBuffSize, size_t outBuffSize);

  virtual ~genericV4Handler(); /*!< Unregister socket and free
                                           resources */

  /*! @brief Set destination IPv4 and trigger buffer updates.
   *  @param[in] ipDst Destination IPv4 address
   *  @return
   *    -   EXIT_SUCCESS
   *    -   EXIT_FAILURE
   */
  virtual int_fast8_t setIpDst(std::string ipDst);

  /*! @brief Set source IPv4 and trigger buffer updates.
   *  @param[in] ipSrc Source IPv4 address
   *  @return
   *    -   EXIT_SUCCESS
   *    -   EXIT_FAILURE
   */
  virtual int_fast8_t setIpSrc(std::string ipSrc);

  /*! @brief Set destination IPv4 and trigger buffer updates.
   *  @param[in] ipDst Destination IPv4 address
   *  @return
   *    -   EXIT_SUCCESS
   *    -   EXIT_FAILURE
   */
  virtual int_fast8_t setIpDst(uint32_t ipDst);

  /*! @brief Set source IPv4 and trigger buffer updates.
   *  @param[in] ipSrc Source IPv4 address
   *  @return
   *    -   EXIT_SUCCESS
   *    -   EXIT_FAILURE
   */
  virtual int_fast8_t setIpSrc(uint32_t ipSrc);

  /*! @brief Set destination port and trigger buffer updates
   *  @param[in] dport Destination port
   *  @return
   *    -   EXIT_SUCCESS
   */
  virtual int_fast8_t setPortDst(uint_fast16_t dport);

  /*! @brief Set source port and trigger buffer updates
   *  @param[in] sport Source port
   *  @return
   *    -   EXIT_SUCCESS
   */
  virtual int_fast8_t setPortSrc(uint_fast16_t sport);

  /*!@brief Register socket to asyncIo library
   * @return
   *     -   EXIT_SUCCESS
   *     -   EXIT_FAILURE
   */
  virtual uint_fast8_t registerSocket();

  /*! @brief Unregister the socket from the asyncIo library.
   * @return
   *    -   EXIT_SUCCESS
   *    -   EXIT_FAILURE
   */
  virtual uint_fast8_t unregisterSocket();

  /*!@brief Return the associated file descriptor
   * @return
   *    -   File descriptor
   */
  virtual int getFd() const;

  /*!@brief Return the associated events
   * @return
   *    -   Epoll events
   */
  virtual uint_fast32_t getEvents() const;
};

/*!
 * @class l2Handler
 * @brief Dump frames to stdout starting at ethernet layer
 *
 * Initialize a \code socket(PF_INET, SOCK_RAW, htons(ETH_P_ALL)) \endcode and
 * process packets with \code printPacket \endcode
 */
class l2Handler : public packHandler
{
private:
  uint_fast8_t
    _layer; /*!< The TCP/IP layer used in printPacket. It is inferred from the
               constructor parameters. */

public:
  /*! @brief Initialize a generic handler that prints to stdout
   *
   * You specify what layer to dump, and corresponding protocol.
   * E.g:
   * - AF_PACKET        - ETH_P_ARP
   * - AF_INET          - IPPROTO_UDP
   * - AF_PACKET        - ETH_P_ALL
   * - AF_PACKET        - ETH_P_*
   * - AF_INET/AF_INET6 - IPPROTO_*
   * - 0                - if socket is external - the domain shall default to
   * AF_PACKET
   *
   * @param[in] domain TCP/IP domain from which to capture
   * @param[in] protocol Corresponding protocol for that layer
   */
  l2Handler(int domain,
            int protocol); /*!< Initialize a layer 2 packet socket */
  virtual ~l2Handler();    /*!< Close the socket */

  /*! @brief Handle frame
   *  @param[in] sofd File descriptor from which we read frames
   *  @param[in] events Epoll events to handle
   *  @return
   *    -   EXIT_SUCCESS
   *    -   EXIT_FAILURE
   */
  virtual uint_fast32_t process(int sofd, uint_fast32_t events) override;

  /*!@brief Get file descriptor.
   * @return
   *    -   layer 2 packet file descriptor
   */
  int getSoFd() const { return _sofd; };

  /*!@brief Get epoll events.
   * @return
   *    -   epoll events
   */
  uint_fast32_t getEvents() const { return _events; };
};

/*!
 * @class ipSniffer
 * @brief Handler class used to intercept ARP packets.
 *
 * It saves the ARP packets in a vector.
 */
class ipSniffer : public packHandler
{
private:
  std::mutex _lock; /*!< Mutex used to lock sensitive operations */
  std::vector<struct ether_arp> _arps; /*!< Vector of ARP packets */

public:
  /*!@brief Straightforward constructor.
   * @param[in] sofd You can supply an existing file descriptor or leave it
   * default to create one
   */
  ipSniffer(int sofd = -1);
  virtual ~ipSniffer(); /*!< Close file descriptor */
  /*!@brief Process ARP traffic, adding to the vector.
   * @param[in] sofd File descriptor to read from
   * @param[in] events Epoll events to handle. Only EPOLLIN supported.
   * @return
   *    -   EXIT_FAILURE
   *    -   EXIT_SUCCESS
   */
  virtual uint_fast32_t process(int sofd, uint_fast32_t events) override;

  /*!@brief Print the ARP packet using \code printARP \endcode */
  void printArps();
};

/*!
 * @class arPing
 * @brief Helper class used to emit ARP Requests and keep track of Replies.
 */
class arPing : public genericV4Handler
{
private:
  struct sockaddr_ll _sockl; /*!< Sockname used to send data */
  /*! @brief Map Destination IP to number of replies
   *
   * key: destination IPv4@n
   * value: number of replies
   */
  std::map<uint_fast32_t, uint_fast32_t> _arpReplies;
  std::string _ifName; /*!< Name of interface used to send data */

private:
  /*!
   * @brief Used to handle ARP reply
   *
   * If we find the response in our map of sent requests, we increment its
   * count.
   * @param[in] arp ARP reply received for handling
   * @return
   *    -   EXIT_FAILURE
   *    -   EXIT_SUCCESS
   */
  uint_fast8_t _handleArpReply(const struct ether_arp* arp);

  /*!
   * @brief Delegating constructor.
   *
   * Used to initialize events, socket, and sockname
   */
  arPing();

public:
  /*!
   * @brief Send an ARP Request
   *
   * It takes the data from _ipSrc and _ipDst and crafts and ARP REQUEST to
   * BROADCAST
   * @return
   *    -   EXIT_SUCCESS
   *    -   EXIT_FAILURE
   */
  uint_fast8_t sendArpPing();

  /**
   * @brief Return the interface index associated with this instance of arPing
   *
   * @return
   *    -   ifIndex
   */
  int getIfIndex(void) const;

  /**
   * @brief Return the interface index associated with this instance of arPing
   * both in numeric and name format
   *
   * @param[out] intName Reference to string in which copy the interface name
   *
   * @return
   *    -   ifIndex
   */
  int getIfIndex(std::string& intName) const;

  /*!
   * @brief Constructor using name
   * @param[in] ifName Name of the interface we shall use to send packets
   */
  arPing(std::string ifName);

  /*!
   * @brief Constructor using index
   * @param[in] ifIndex Index of the interface we shall use to send packets
   */
  arPing(int ifIndex);

  /*!
   * @brief Process ARP Replies
   *
   * Increment replies if we've sent a request to that destination.
   * @param[in] sofd Socket used to read ARPs from.
   * @param[in] events Event that we shall handler - currently just EPOLLIN
   * @return
   *    -   EXIT_FAILURE
   *    -   EXIT_SUCCESS
   */
  virtual uint_fast32_t process(int sofd, uint_fast32_t events) override;
  virtual ~arPing(); /*!< Free resources */
};

/*!@class ipSniffer2
 * @todo This name is stupid...change it!
 * @brief Handler class used to sniff IPv4s and MACs from ARP traffic.
 *
 * This class is useful because it's used to infer the network mask, network
 * address, and gateway.
 */
class ipSniffer2 : public genericV4Handler
{
private:
  /*!@brief Map decimal IPv4 to MAC
   *
   * key: Decimal IPv4@n
   * value: MAC address returned by \code net_utils::charToMac \endcode
   */
  std::map<uint_fast32_t, uint_fast64_t> _arps;

  /*! @brief Map decimal IPv4 to count
   *
   * key: Decimal IPv4@n
   * value: Number of appearances
   */
  std::map<uint_fast32_t, uint_fast32_t> _ips;

  /**
   * @brief Keep a list of IPs to disregard when infering gateway.
   * @todo Provide a function so that the user can add IPs to the ignored list
   *
   * We keep in here our locally configured IPs and the one used by getUnusedIps
   * to arPing
   */
  std::set<uint_fast32_t> _ignoredIps;

private:
  /*!@brief Add ARP info to structures
   *
   * Checks if IPv4 and MAC are valid beforehand.
   * @param[in] buffer ARP packet
   * @note No locks are taken
   */
  void _processArp(uint8_t* buffer);

  /*! @brief Try to obtain network address and mask.
   *
   * This method does a XOR between the biggest and smallest mapped IPv4
   * addresses. log2 for that value represents the host number of bits in the
   * mask.
   * @warning Works in most cases, but I've seen subnets with strange
   * behaviour...
   * @note No locks are taken
   * @return
   *    -   least significant 32 bits are the network address, the next 32 are
   * the extended mask
   *    -   0 if not enough input
   */
  uint_fast64_t _getPotentialNetworkV1();

  /*! @brief Try to obtain network address and mask.
   *
   * This method uses the normal distribution and makes the XOR between
   * [stdev - 3 * avg ; stdev + 3 * avg]  addresses. log2 for that value
   * represents the host number of bits in the mask.
   * @todo Consider removing this because of all the floating point
   * operations.
   * @warning Works in more cases than the first, but it still can't eliminate
   * outliers.
   * @note No locks are taken
   * @return
   *    -   least significant 32 bits are the network address, the next 32 are
   * the extended mask
   *    -   0 if not enough input
   */
  uint_fast64_t _getPotentialNetworkV2();

  /*! @brief Try to obtain network address and mask.
   *
   * This method seemed initially the most retarded, but after some tests it
   * seems to work best. It takes all IPv4s two-by-two and keeps tracks of the
   * most common mask.
   * @note No locks are taken
   * @return
   *    -   least significant 32 bits are the network address, the next 32 are
   * the extended mask
   *    -   0 if not enough input
   */
  uint_fast64_t _getPotentialNetworkV3();

  /*! @brief Try to obtain the IPv4 gateway address.
   * @note No locks are taken here.
   *
   * It tries this by returning the most frequent IPv4 seen in packets.
   * The more you leave it to process the better is the guess.
   * @return
   *    -   IPv4 gateway address
   *    -   zero if no data is available
   */
  uint_fast32_t _getPotentialGateway();

public:
  ipSniffer2(); /*!< @brief Initialize the socket, buffer, and asyncIo */
  virtual ~ipSniffer2(); /*!< @brief Deregister socket and close it */

  /*! @brief Return IPv4 count.
   *  @return
   *     -   constant reference to _ips.
   */
  const std::map<uint_fast32_t, uint_fast32_t>& getIps();

  /*! @brief Return IPv4 - MAC mapping.
   *  @return
   *    -   constant reference to _arps.
   */
  const std::map<uint_fast32_t, uint_fast64_t>& getArps();

  /*! @brief Handle ARP packets by saving IPv4s, their count, and their MACs
   * @param[in] sofd Socket file descriptor used to read from.
   * @param[in] events Epoll events to handle
   * @return
   *    -   EXIT_FAILURE
   *    -   EXIT_SUCCESS
   */
  virtual uint_fast32_t process(int sofd, uint_fast32_t events) override;

  /*! @brief Try to obtain the IPv4 gateway address.
   *
   * It tries this by returning the most frequent IPv4 seen in packets.
   * The more you leave it to process the better is the guess.
   * @return
   *    -   IPv4 gateway address
   *    -   zero if no data is available
   */
  uint_fast32_t getPotentialGateway();

  /*! @brief Try to obtain network address and mask.
   *
   * Call one of the private methods.
   * @param[in] method
   *    1 - easy on the CPU, but it's not that good in multicast
   * environment@n
   *    2 - hard on the CPU, uses normal distributions to infer, still
   * bad@n
   *    3 - hard on the CPU, the best so far@n
   * @return
   *    -   least significant 32 bits are the network address, the next 32 are
   * the extended mask
   *    -   0 if not enough input
   */
  uint_fast64_t getPotentialNetwork(int_fast8_t method = 3);

  /**
   * @brief Try and fill up the supplied vector with available IPv4 addresses
   * @todo Should we arping in here or not?
   *
   * @param[out] ips List of available IPv4 addresses
   * @param[in] arpy arPing instance used to generate ARP traffic
   *
   * @return
   *    -   number of IPs in the list
   */
  size_t getUnusedIps(std::set<uint_fast32_t>& ips, class arPing& arpy);

  /*!@brief Print the intercepted IP->ARP maping */
  void printClients();
};

/*! @class icmpHandler
 *  @brief Handler class for ICMP traffic
 *
 *  It currently keeps track of ICMP PORT UNREACHABLE, and ICMP ECHO REPLY.
 *  It can also craft and ICMP ECHO REQUEST.
 */
class icmpHandler : public genericV4Handler
{
private:
  /*!@brief Map IPv4 - response count
   *
   * key: IPv4 address@n
   * value: Number of ICMP ECHO REPLIES
   */
  std::map<uint_fast32_t, uint_fast32_t> _pingCount;

  /*!@brief Map IPv4 - unreachable port
   *
   * key: IPv4 source address@n
   * value: Set of unreachable ports. Least significant 16 bits are the port,
   * the most significant bits are the protocol (IPPROTO_*)
   */
  std::map<uint_fast32_t, std::set<uint_fast32_t>> _portUrch;

private:
  /*! @brief Process ICMP data accordingly.
   *
   * For ICMP ECHO REPLY, we update the count.
   * FOR ICMP PORT UNREACHABLE, we save the information associated.
   * @param[in] buffer ICMP data buffer
   * @param[in] len Buffer length
   * @return
   *    -   EXIT_FAILURE
   *    -   EXIT_SUCCESS
   */
  uint_fast8_t _processIcmp(uint8_t* buffer, ssize_t len);

  /*! @brief Extract port/protocol information from ICMP PORT UNREACHABLE
   * @param[in] buffer ICMP data buffer
   * @param[in] len Buffer length
   * @param[out] dport Variable in which we save the port/protocol that are
   * unreachable
   * @return
   *    -   EXIT_SUCCESS
   *    -   EXIT_FAILURE
   */
  uint_fast8_t _getDstPortFromIcmp(uint8_t* buffer,
                                   ssize_t len,
                                   uint_fast32_t* dport);

public:
  icmpHandler(); /*!< Initialize socket, buffer, asyncIo library handler */
  virtual ~icmpHandler(); /*!< Deregister and close socket */

  /*!@brief Proccess incoming ICMP packet
   * @param[in] sofd File descriptor to read from
   * @param[in] events Epoll events to handle
   * @return
   *    -   EXIT_SUCCESS
   *    -   EXIT_FAILURE
   */
  virtual uint_fast32_t process(int sofd, uint_fast32_t events) override;

  /*!@brief Obtain reference to ECHO REPLIES mapping
   * @return
   *    -   const reference to _pingCount
   */
  const std::map<uint_fast32_t, uint_fast32_t>& getPingCount();

  /*!
   * @brief Print ICMP ECHO Responses and counts
   */
  void printPingCount();

  /*!@brief Obtain reference to PORT UNREACHABLE mapping
   * @return
   *    -   const reference to _portUrch
   */
  const std::map<uint_fast32_t, std::set<uint_fast32_t>>& getPortUnreach();

  /*!
   * @brief Print all the ICMP PORT UNREACHABLE saved responses
   */
  void printPortUnreachable();

  /*! @brief Check if we have any information regarding the reachability of a
   * port.
   *  @param[in] ipSrc IPv4 address of host
   *  @param[in] portSrc Port in which we are interested
   *  @param[in] type Type of transport layer protocol. e.g: IPPROTO_TCP
   *  @return
   *    -   true - is unreachable
   *    -   false - no data
   */
  bool isUnreachable(uint_fast32_t ipSrc, uint_fast16_t portSrc, uint8_t type);

  /*! @brief Send ICMP ECHO REQUEST
   *  @param[in] count Number of ping packets to send. Defaults to 1.
   *  @return
   *    -   EXIT_SUCCESS
   *    -   EXIT_FAILURE
   */
  uint_fast8_t pingHost(size_t count = 1);
};

/*! @class udpV4Knocker
 *  @brief Handler designed to craft/send/receive UDP datagrams.
 *
 *  We can send UDP datagrams with cooked source/destination IPv4 and Port.
 *  It also keeps track of the received responses.
 */
class udpV4Knocker : public genericV4Handler
{
private:
  /*!@brief Map UDP Destination Info - Source Info
   *
   * key: Least significant 32 bits are Destination IPv4, after that 16 bits
   * are the target port@n
   * value: Least significant 32 bits are the Source IPV4,
   * after that 16 bits are the source port
   */
  std::map<uint_fast64_t, uint_fast64_t> _portResp;

  /*!
   * @brief Buffer used to compute the checksum.
   *
   * We declared it as a member because having net_utils allocating and freeing
   * every time we changed anything in the members increased the memory
   * footprint.
   */
  std::unique_ptr<std::uint8_t[]> _pseudoBuffer;

private:
  /*! @brief Process UDP packet adding the _portRest value if we have
   * corresponding key.
   *  @param[in] data UDP data buffer
   *  @param[in] len Buffer length
   *  @return
   *    -   EXIT_SUCCESS
   *    -   EXIT_FAILURE - no match or invalid arguments
   */
  int_fast8_t processRawUdp(uint8_t* data, size_t len);

  /*! @brief Process any user changes to the IPs or ports.
   *
   * It updates the target information and output buffer.
   * @return
   *    -   EXIT_SUCCESS
   *    -   EXIT_FAILURE
   */
  int_fast8_t _processChanges() override;

public:
  /*! @brief Send UDP datagram with user defined IPs and Ports
   *  @return
   *    -   EXIT_SUCCESS
   *    -   EXIT_FAILURE
   */
  uint_fast8_t sendUdpKnock();

  /*! @brief Initialize the UDP handler/crafter.
   *  @param[in] sofd Optional file descriptor. If no file descriptor is given
   * we create one.
   *  @note We set IP_HDRINCL on the socket in order to spoof source IPv4
   */
  udpV4Knocker(int sofd = -1);

  /*! @brief Print a list of all IPv4 and Ports that have responded */
  void printUdpResponses();

  /*! @brief Print a list of unreachable IPv4 and Ports for our send UDP
   * datagrams
   *
   *  @note We need to supply a reference to an icmpHandler that has kept
   * track of this in the meanwhile.
   *  @param[in] icmp reference to icmpHandler that has info about unreachable
   * ports
   */
  void printUdpUnreachable(icmpHandler& icmp);

  virtual ~udpV4Knocker();

  /*! @brief Handle UDP data
   *  @param[in] sofd File descriptor from which to read
   *  @param[in] events Epoll events to process
   *  @return
   *    -   EXIT_FAILURE
   *    -   EXIT_SUCCESS
   */
  virtual uint_fast32_t process(int sofd, uint_fast32_t events) override;
};

/*!
 * @class tcpV4Knocker
 * @brief Handler class used to SYN Scan and process responses from targets
 */
class tcpV4Knocker : public genericV4Handler
{
private:
  /*!
   * @brief Keep handshake states
   *
   * key: destination IP(32 lsb), destination port(16 bits)@n
   * value: source port(16 lsb), IP flags(8 bits)
   */
  std::map<uint_fast64_t, uint_fast32_t> _portResp;

  /*!
   * @brief Buffer used to compute the checksum.
   *
   * We declared it as a member because having net_utils allocating and freeing
   * every time we changed anything in the members increased the memory
   * footprint.
   */
  std::unique_ptr<std::uint8_t[]> _pseudoBuffer;

private:
  /*! @brief Process TCP packet adding in the _portResp the value if we have
   * corresponding key.
   *  @param[in] data RAW IP data buffer
   *  @param[in] len Buffer length
   *  @return
   *    -   EXIT_SUCCESS
   *    -   EXIT_FAILURE - no match or invalid arguments
   */
  int_fast8_t _processRawTcp(uint8_t* data, size_t len);

  /*! @brief Process any user changes to the IPs or ports.
   *
   * It updates the target information and output buffer.
   * @return
   *    -   EXIT_SUCCESS
   *    -   EXIT_FAILURE
   */
  int_fast8_t _processChanges(void) override;

public:
  /*!
   * @brief Initialize the TCPv4 Handler
   *
   * @param[in] sofd Optional socket used to send/receive. If left to default we
   * create one.
   */
  tcpV4Knocker(int sofd = -1);

  virtual ~tcpV4Knocker(); /*!< Unregister socket and free resources */

  /*!
   * @brief SYN scan specified target.
   *
   * It takes its info from the inherited members and initiates a SYN scan of
   * the form: SYN, SYN/ACK, RST
   * @return
   *    -   EXIT_SUCCESS
   *    -   EXIT_FAILURE
   */
  int_fast8_t sendSynKnock();

  /*!
   * @brief Print information about initiated scans
   */
  void printTcpResponses();

  /*! @brief Print a list of unreachable IPv4 and Ports for our send TCP
   * segments
   *
   *  @note We need to supply a reference to an icmpHandler that has kept
   * track of this in the meanwhile.
   *  @param[in] icmp reference to icmpHandler that has info about unreachable
   * ports
   */
  void printTcpUnreach(class icmpHandler& icmp);

  /*! @brief Handle TCP data
   *  @param[in] sofd File descriptor from which to read
   *  @param[in] events Epoll events to process
   *  @return
   *    -   EXIT_FAILURE
   *    -   EXIT_SUCCESS
   */
  virtual uint_fast32_t process(int sofd, uint_fast32_t events) override;
};

#endif
