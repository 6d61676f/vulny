#ifndef _JSONNY_HPP_
#define _JSONNY_HPP_

#include <future>
#include <json/json.h>
#include <mutex>
#include <set>
#include <string>
#include <thread>
#include <unordered_map>

/**
 * @class jSonny
 * @brief Class used to open a Json file and parse it's content based on the
 * rules defined in jVally object.
 *
 * The normal use case is to create a jSonny object and after that call
 * parseValue with an object of type jVally in which we define the actual
 * parsing mechanism.
 */
class jSonny
{
private:
  std::string _configFile; /**< String containing the path of the JSON file */
  Json::Value _jsonData;   /**< JSON data value holding the entire document */

public:
  class jVally;

public:
  /**
   * @brief Constructor that takes the name of the JSON file to get parsed
   *
   * @param[in] file Name of the JSON file to get opened
   */
  jSonny(std::string file);

  /**
   * @brief Default destructor
   */
  virtual ~jSonny() = default;
  /**
   * @brief Actual parsing function that populates a jVally object with the
   * content available in the file
   *
   * @param[in, out] value jVally reference specifiying the correct parsing
   * method. Usually jVally gets populated after this call.
   * @return
   *    -   EXIT_FAILURE
   *    -   EXIT_SUCCESS
   */
  int_fast8_t parseValue(class jSonny::jVally& value);
};

/**
 * @class jSonny::jVally
 * @brief JSON parsing subclass inherited by the user in order to define the
 * actual parsing method.
 */
class jSonny::jVally
{
protected:
  std::string
    _typeName; /**< Name of the JSON value/object to retrieve from the whole
                  JSON document */

public:
  /**
   * @brief Getter for the jVally property/object/value name
   *
   * @return
   *    -   Object name
   */
  const std::string getName() const { return this->_typeName; }
  /**
   * @brief Constructor that specifies the object name.
   *
   * @param property Object name that we retrieve from the whole JSON document.
   */
  jVally(std::string property);
  virtual ~jVally() = default; /**< Default destructor */
  /**
   * @brief Pure virtual method that gets called by jSonny in order to parse a
   * certain JSON object.
   *
   * @param val JSON object corresponding to the property given in the
   * constructor.
   *
   * @return
   *    -   EXIT_FAILURE
   *    -   EXIT_SUCCESS
   *    -   USER DEFINED
   */
  virtual int_fast8_t parse(const Json::Value& val) = 0;
};

/**
 * @class VulnyScenario
 * @brief Abstract class used in order to parse a JSON object and deploy it's
 * scenario.
 */
class VulnyScenario : public jSonny::jVally
{
protected:
  bool _sync{ true }; /**<Launch sync or async*/

public:
  /**
   * @brief Enum used by the build method in order to create the proper
   * scenario.
   */
  enum ScanType
  {
    Stealth, /**<Stealth Scenario */
    Knock,   /**<Knock Scenario */
    Audit,   /**<Audit Scenario */
  };

public:
  /**
   * @brief Pure virtual method that deploys the scenario.
   *
   * @return
   *    -   EXIT_FAILURE
   *    -   EXIT_SUCCESS
   *    -   USER DEFINED
   */
  virtual int_fast8_t deploy() = 0;

  /**
   * @brief Pure virtual method that waits for the scenario to finish executing.
   *
   * @return
   *    -   EXIT_FAILURE
   *    -   EXIT_SUCCESS
   *    -   USER DEFINED
   */
  virtual int_fast8_t wait() = 0;

  /**
   * @brief Constructor that initializes the object name and sync type
   *
   * @param[in] propname Name of the property to get fetched from the JSON document.
   * @param[in] sync    Execution type - true for sync
   */
  VulnyScenario(const std::string& propname, bool sync = false);
  virtual ~VulnyScenario() = default; /**< Default destructor */

  /**
   * @brief Builder function that creates a proper VulnyScenario object based on the given type.
   * @param[in] type    Enum defining the type of the Scenario to get created.
   * @return
   *    -   pointer to VulnyScenario subobject
   *    -   nullptr in case of unknown type
   */
  static class VulnyScenario* build(VulnyScenario::ScanType type);

  /**
   * @brief Builder function that creates a proper VulnyScenario object based on the given type and custom JSON object name.
   * @param[in] type    Enum defining the type of the Scenario to get created.
   * @param[in] customPropName  Custom JSON object name to get retrieved from the document.
   * @return
   *    -   pointer to VulnyScenario subobject
   *    -   nullptr in case of unknown type
   */
  static class VulnyScenario* build(VulnyScenario::ScanType type,
                                     const std::string& customPropName);
};

#endif //_JSONNY_HPP_
