#ifndef _NET_UTILS_HPP
#define _NET_UTILS_HPP

#include "debug.hpp"
#include <functional>
#include <iostream>
#include <net/if.h>
#include <random>
#include <set>
#include <vector>

#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
/*!
 * @brief Macro to extract octets from network order IPv4 for printf
 * @param decimalIP Numerical IPv4 to be printed
 * @return
 *      - (unsigned)(1st octet), (unsigned)(2nd octet), (unsigned)(3rd octet),
 * (unsigned)(4th octet)
 */
#define IP_FORMAT_NO(decimalIP)                                                \
  (unsigned)(decimalIP & 0xFF), (unsigned)((decimalIP >> 8) & 0xFF),           \
    (unsigned)((decimalIP >> 16) & 0xff), (unsigned)((decimalIP >> 24) & 0xff)
#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
#define IP_FORMAT_NO(decimalIP) IP_FORMAT(decimalIP)
#endif

/*!
 * @brief Macro to extract octets from IPv4 for printf
 * @param decimalIP Numerical IPv4 to be printed
 * @return
 *      - (unsigned)(1st octet), (unsigned)(2nd octet), (unsigned)(3rd octet),
 * (unsigned)(4th octet)
 */
#define IP_FORMAT(decimalIP)                                                   \
  (unsigned)((decimalIP >> 24) & 0xff), (unsigned)((decimalIP >> 16) & 0xff),  \
    (unsigned)((decimalIP >> 8) & 0xFF), (unsigned)(decimalIP & 0xFF)

/*!
 * @namespace net_utils
 * @brief Useful networking functions.
 * Everything that I could think of as useful functions for networking are
 * implemented here. That's not to say that this namespace contains just
 * intelligent, reusable stuff. Basically, all refactoring of code happens by
 * creating a smaller function and adding it here.
 * @warning Biggest part of the code and in continous expansion.
 */
namespace net_utils {

/*!
 *  @struct transportPseudoHeader
 *  @brief Used in order to compute TCP/UDP Checksum
 *  @note Everything is in Network Order
 */
struct transportPseudoHeader
{
  uint32_t sourceIp; /*!< Source IPv4 taken from IP header */
  uint32_t destIp;   /*!< Destination IPv4 taken from IP header */
  uint8_t zero{ 0 }; /*!< Reserved */
  uint8_t protocol;  /*!< Protocol value taken from IP Header - IPPROTO_UDP or
                        IPPROTO_TCP */
  uint16_t len; /*!< Length of TCP/UDP Header + Payload @warning Without this
                   Pseudo-Header */
};

#ifdef MSN_RND_FUNC
/*!
 * @brief Create a Mersenne twister PRNG using a random device
 * @return
 *      -   Mersenne twister object
 */
std::mt19937_64
getRandomGenerator();
#else  // MSN_RND_FUNC
/*!
 * @brief Create a PRNG using something else than mersenne twister that
 * profiles poorly.@n srandom(3) and random(3) are currently used.
 * @return
 *      -   random(3) function
 */
std::function<long int(void)>
getRandomGenerator();
#endif // MSN_RND_FUNC

/*! @brief Given an interface name, get associated MAC address
 * @param ifname Interface Name
 * @param addr Array containing the MAC address
 * @return
 *    -   EXIT_FAILURE
 *    -   EXIT_SUCCESS
 */
int
getHwAddr(const char ifname[IFNAMSIZ], uint8_t addr[IFHWADDRLEN]);

/*! @brief Given an interface name set its MAC address
 * @param[in] ifname Interface Name
 * @param[in,out] addr Array containing the new MAC address
 * @param[in] randomize Disregard what's in the array and generate randomly
 * putting the new MAC in addr
 * @return
 *    -   EXIT_FAILURE
 *    -   EXIT_SUCCESS
 */
int
setHwAddr(const char ifname[IFNAMSIZ],
          uint8_t addr[IFHWADDRLEN],
          uint8_t randomize);

/*! @brief Given an interface name reset its MAC address to the default one.
 * @param[in] ifname Interface Name
 * @return
 *    -   EXIT_FAILURE
 *    -   EXIT_SUCCESS
 */
int
resetHwAddr(const char ifname[IFNAMSIZ]);

/*! @brief Check is MAC address is valid. Basically not zero of broadcast
 *  @param[in] hwaddr Array containing the MAC address
 *  @return
 *    -   true
 *    -   false
 */
bool
isValidMac(const uint8_t* hwaddr);

/*! @brief Check if IPv4 is valid, i.e. not zero or all ones.
 * @param ip Numeric IPv4
 * @return
 *    -   true
 *    -   false
 */
bool
isValidIpV4(uint_fast32_t ip);

/*! @brief Given two IPv4s get their common mask.
 * @param ipA First IPv4
 * @param ipB Seconf IPv4
 * @return
 *    -   Number describing the mask. e.g: 23,8,16
 */
uint_fast8_t
getIpV4Mask(uint_fast32_t ipA, uint_fast32_t ipB);

/*! @brief Convert number describing the mask in extended value or vice-versa.
 * e.g 24 -> 4294967040(255.255.255.0)
 * @param[in,out] cidr Pointer to number describing mask or zero
 * @param[in,out] extended Pointer to number of extended mask or zero
 * @note This function tests to see which is zero and converts from the
 * other argument.
 * @return
 *    -   EXIT_FAILURE
 *    -   EXIT_SUCCESS
 */
uint_fast8_t
convertIpV4Mask(uint_fast8_t* cidr, uint_fast32_t* extended);
/*! @brief Given an interface name and flags apply them to the interface.
 * @param[in] ifname Interface Name
 * @param[in] flags Flags to be applies e.g: IFF_PROMISC
 * @return
 *    -   EXIT_SUCCESS
 *    -   EXIT_FAILURE
 */
int
setIntFlags(const char ifname[IFNAMSIZ], uint_fast32_t flags);

/*! @brief Given an interface name return the interface flags.
 * @param[in] ifname Interface Name
 * @param[out] flags Flags describing the interface
 * @return
 *    -   EXIT_SUCCESS
 *    -   EXIT_FAILURE
 */
int
getIntFlags(const char ifname[IFNAMSIZ], uint_fast32_t* flags);

/*! @brief Given an interface name and flags unsets them from the interface.
 * @param[in] ifname Interface Name
 * @param[in] flags Flags to be unset e.g: IFF_PROMISC
 * @return
 *    -   EXIT_SUCCESS
 *    -   EXIT_FAILURE
 */
int
unsetIntFlags(const char ifname[IFNAMSIZ], uint_fast32_t flags);

/*! @brief Pretty Print interface flags
 * @param[in] flags Bitmask of flags to be printed
 * @return
 *    -   EXIT_SUCCESS
 *    -   EXIT_FAILURE
 */
int
printIntFlags(uint_fast32_t flags);

/*! @brief Given an interface name put it in Monitor Mode
 * @param ifname Interface Name
 * @return
 *    -   EXIT_SUCCESS
 *    -   EXIT_FAILURE
 */
int
monitorMode(const char ifname[IFNAMSIZ]);

/*! @brief Given an interface name put it in Managed Mode
 * @param ifname Interface Name
 * @return
 *    -   EXIT_SUCCESS
 *    -   EXIT_FAILURE
 */
int
managedMode(const char ifname[IFNAMSIZ]);

/*! @brief Convert array of octets to decimal IPv4
 * @param[in] ip Array of IPv4 octets
 * @param[out] numeric Decimal value to be created from array
 * @note Maps arr[0] to the most significant byte of the uint
 * @return
 *    -   EXIT_FAILURE
 *    -   EXIT_SUCCESS
 */
int
charToIpV4(const uint8_t ip[4], uint_fast32_t* numeric);

/*! @brief Convert decimal IPv4 to array of octets
 * @param[in] numeric Decimal IPv4 value
 * @param[out] ip Array of IPv4 octets
 * @note Maps arr[0] to the most significant byte of the uint
 * @return
 *    -   EXIT_FAILURE
 *    -   EXIT_SUCCESS
 */
int
ipV4ToChar(uint8_t ip[4], uint_fast32_t numeric);

/*! @brief Convert MAC from array to number
 * @note Least significant 48 bits are the MAC
 * @param[in] mac Array of octets from MAC
 * @param[out] numeric Pointer to integer holding the result
 * @return
 *    -   EXIT_SUCCESS
 *    -   EXIT_FAILURE
 */
/*! We map mac[5] to the least significant octet and so on*/
int
charToMac(const uint8_t mac[IFHWADDRLEN], uint_fast64_t* numeric);

/*! @brief Convert MAC from number to array
 * @note Least significant 48 bits are the MAC
 * @param[out] mac Array of octets describing MAC
 * @param[in] numeric Integer holding the MAC
 * @return
 *    -   EXIT_SUCCESS
 *    -   EXIT_FAILURE
 */
/*! We map mac[5] to the least significant octet and so on*/
int
macToChar(uint8_t mac[IFHWADDRLEN], uint_fast64_t numeric);

/**
 * @brief Convert a mac stored in an array to a string representation
 *
 * @param[out]  mac  String that we populate with the human representation
 * @param[in]   arr  Array holding the MAC bytes
 *
 * @return
 *      -   EXIT_FAILURE
 *      -   EXIT_SUCCESS
 */
int
macToString(std::string& mac, const uint8_t arr[IFHWADDRLEN]);

/**
 * @brief Convert the string representation of a MAC address to an array of bytes
 *
 * @param[in]   mac String holding the human readable representation
 * @param[out]  arr Array that we populate with the MAC bytes
 *
 * @return
 *      -   EXIT_FAILURE
 *      -   EXIT_SUCCESS
 */
int
stringToMac(const std::string& mac, uint8_t arr[IFHWADDRLEN]);

/*! @brief Obtain a list of all IPv4 addresses configured on all interfaces.
 * @note The IPv4s are stored in host order.
 * @param ips Reference to list in which we add the configured IPv4s
 * @return
 *    -   EXIT_FAILURE
 *    -   EXIT_SUCCES
 */
int
getMyIps4(std::set<uint_fast32_t>& ips);

/**
 * @brief Obtain a list of Interface Names present on the device.
 * @param ifNames Vector of strings containing the Interface Names
 * @return
 *  -   EXIT_SUCCESS
 *  -   EXIT_FAILURE
 */
int
getMyIfNames(std::vector<std::string>& ifNames);

/*! @brief Compute checksum for a buffer.
 * @note RFC-1071
 * @param[in] addr Buffer address
 * @param[in] count Buffer length in bytes
 * @return
 *    -   checksum
 */
long
checksum(uint16_t* addr, ssize_t count);

/*! @brief Compute checksum for a UDP packet.
 * @note RFC-768
 * @param[in] data Buffer address
 * @param[in] len Buffer length in bytes
 * @param[in] pseudoBuffer Pointer to use allocked memory used to compute
 * checksum.@n
 * Added because in certain situations this functions does a lot of
 * allocs/frees. The buffer should be @p len sized.
 * @return
 *    -   checksum
 *    -   0 on error
 */
long
udpChecksum(uint8_t* data, ssize_t len, uint8_t* pseudoBuffer = nullptr);

/*! @brief Compute checksum for a TCP packet.
 * @note RFC-793
 * @param[in] data Buffer address
 * @param[in] len Buffer length in bytes
 * @param[in] pseudoBuffer Pointer to use allocked memory used to compute
 * checksum.@n
 * Added because in certain situations this functions does a lot of
 * allocs/frees. The buffer should be @p len sized.
 * @return
 *    -   checksum
 *    -   0 on error
 */
long
tcpChecksum(uint8_t* data, ssize_t len, uint8_t* pseudoBuffer = nullptr);

/*! @brief Populate buffer with ARP request sane defaults.
 * @note We complete everything besides source MAC.
 * @note Destination MAC shall be broadcast.
 * @param[out] data Buffer that gets prepared
 * @param[in] linkLayer Specify if we also take in account link-layer header
 * @param[in] len Buffer length in bytes
 * @return
 *    -   EXIT_SUCCESS
 *    -   EXIT_FAILURE
 */
uint32_t
prepareArpRequest(uint8_t* data, bool linkLayer, ssize_t len);

/*! @brief Populate buffer with IPv4 header sane defaults.
 * @param[out] data Buffer that gets prepared
 * @param[in] len Buffer length in bytes
 * @return
 *    -   EXIT_FAILURE
 *    -   EXIT_SUCCESS
 *
 * Identifier, fragmentation offset, and addresses are left untouched.
 * We set the DON'T FRAGMENT flag
 */
uint32_t
prepareIpHdr(uint8_t* data, ssize_t len);

/*! @brief Hex dump buffer
 * @param[in] data Buffer to get printed
 * @param[in] len Buffer length in bytes
 * @return
 *    -   EXIT_FAILURE
 *    -   EXIT_SUCCESS
 */
int_fast8_t
dumpPacket(const uint8_t* data, ssize_t len);

/*! @brief Try to print buffer as network packet starting from a given stack
 * level
 * @param[in] data Buffer to get interpreted
 * @param[in] len Buffer length in bytes
 * @param[in] layer Layer describing the buffer e.g. 4 - try and interpret as
 * transport layer
 */
void
printPacket(const uint8_t* data, ssize_t len, int layer);

/*! @brief Pretty print Ethernet Header
 * @param[in] data Ethernet buffer address
 * @param[in] len Ethernet buffer length in bytes
 * @return
 *    -   Least significant 16 bits are the ethertype, after that we have
 * the size of the Ethernet header.
 * @note ethertype is in host order
 */
uint32_t
printEthernet(const uint8_t* data, ssize_t len);

/*! @brief Pretty print LLC/SNAP Header
 * @param[in] data LLC buffer address
 * @param[in] len Buffer length in bytes
 * @return
 *    -   Least significant 16 bits are 0, after that we the consumed size (3).
 */
uint32_t
printLLC(const uint8_t* data, ssize_t len);

/*! @brief Pretty print ARP Header and Message
 * @param[in] data ARP buffer address
 * @param[in] len ARP buffer length in bytes
 * @return
 *    -   Least significant 16 bits are reserved(0), after that we have the
 * size of the ARP packet.
 */
uint32_t
printArp(const uint8_t* data, ssize_t len);

/*! @brief Pretty print IPv4 Header
 * @param[in] data IPv4 buffer address
 * @param[in] len IPv4 buffer length in bytes
 * @return
 *    -   Least significant 16 bits are the protocol value, after that we have
 * the size of the IPv4 header + options.
 */
uint32_t
printIpV4(const uint8_t* data, ssize_t len);

/*! @brief Pretty print IPv6 Header
 * @param[in] data IPv6 buffer address
 * @param[in] len IPv6 buffer length in bytes
 * @return
 *    -   Least significant 16 bits are the next-header value, after that we
 * have the size of the IPv6 header.
 */
uint32_t
printIpV6(const uint8_t* data, ssize_t len);

/*! @brief Calls one of net_utils::printIpV6 or net_utils::printIpV4
 * @param[in] data IP buffer address
 * @param[in] len IP buffer length
 * @return 16 LSB are the protocol/next-header value, after that we have the
 * count of consumed bytes
 * @see printIpV4
 * @see printIpV6
 */
uint32_t
printIP(const uint8_t* data, ssize_t len);

/*! @brief Pretty print ICMPv4 Header
 * @param[in] data ICMPv4 buffer address
 * @param[in] len ICMPv4 buffer length in bytes
 * @return
 *    -   Least significant 16 bits are reserved(0), after that we have the
 * size of the ICMPv4 header.
 */
uint32_t
printIcmpV4(const uint8_t* data, ssize_t len);

/*! @brief Pretty print ICMPv6 Header
 * @param[in] data ICMPv6 buffer address
 * @param[in] len ICMPv6 buffer length in bytes
 * @return
 *    -   Least significant 16 bits are reserved(0), after that we have the
 * size of the ICMPv6 header.
 */
uint32_t
printIcmpV6(const uint8_t* data, ssize_t len);

/*! @brief Pretty print UDP Header
 * @param[in] data UDP buffer address
 * @param[in] len UDP buffer length in bytes
 * @return
 *    -   Least significant 16 bits are reserved(0), after that we have the
 * size of the UDP header.
 */
uint32_t
printUDP(const uint8_t* data, ssize_t len);

/*! @brief Pretty print TCP Header
 * @param[in] data TCP buffer address
 * @param[in] len TCP buffer length in bytes
 * @return
 *    -   Least significant 16 bits are reserved(0), after that we have the
 * data offset value in bytes
 */
uint32_t
printTCP(const uint8_t* data, ssize_t len);

/*!
 * @brief Print banner specified in msg
 * @param[in] msg Banner title
 * @param[in] width Banner width
 * @param[in] comm Comment character used in the banner
 */
void
printBanner(const std::string& msg, size_t width = 0, char comm = '#');

/*!
 * @brief Print banner specified in msg
 * @param[in] msg Banner title
 * @param[in] width Banner width
 * @param[in] comm Comment character used in the banner
 */
void
printBanner(const char* msg, size_t width = 0, char comm = '#');

/**
 * @brief Does a primtive check in order to verify if a file descriptor is in
 * usage.@n
 * Just a wrapper over fcntl(fd, F_GETFD).
 * This doesn't mean that the file descriptor has not been closed and
 * refurbished in the meanwhile and is pointing to something else entirely.
 *
 * @param fd File descriptor to get tested
 *
 * @return
 *      -   TRUE
 *      -   FALSE
 */
bool
isValidFd(int fd);

/**
 * @brief Redirect an I/O stream to another I/O stream and create a backup to
 * the original one.
 * @note The user is responsible with the provided FDs.
 *
 * @param[in] origFd Stream that gets redirected
 * @param[in] newFd Stream to which origFD gets redirected
 *
 * @return
 *      -  ( >=0 ) Backup file descriptor that points to the original stream
 *      -  ( < 0 ) Failure
 */
int
redirectStream(int origFd, int newFd);

/**
 * @brief Restore/Redirect an I/O stream to the one specified in the second
 * argument
 * @note The user is responsible with the provided FDs.
 *
 * @param[in] Fd        Stream that gets redirected to its potential original value
 * @param[in] origFd    Original/New destination for the stream
 *
 * @return
 *      -   EXIT_FAILURE
 *      -   EXIT_SUCCESS
 */
int_fast8_t
restoreStream(int Fd, int origFd);

} // namespace net_utils

#endif
