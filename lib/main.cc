#include "async_io.hpp"
#include "handlers.hpp"
#include "jsonny.hpp"
#include "sntlk.hpp"
#include <algorithm>
#include <fstream>
#include <iostream>
#include <json/json.h>

const char* const CFG_NAME = "vulny.json";

static void
help(const char* exec_name, const char* cfg_name)
{
  fprintf(stderr,
          "Please provide a configuration file for Vulny.\n"
          "You can either call %s <cfg_file.json>, or place '%s' in "
          "either $XDG_CONFIG_PATH or $HOME/.config\n"
          "Thank You!\n",
          exec_name,
          cfg_name);
}

int
main(int argc, char* argv[], char* envp[])
{
  try {
    std::string configPath;
    auto ret = EXIT_SUCCESS;
    if (argc == 2) {
      DBG("%s", "Received cfg from user");
      configPath = argv[1];
    } else {
      DBG("%s", "Trying config from env");
      for (char** envy = envp; *envy != nullptr; envy++) {
        if (strcasestr(*envy, "XDG_CONFIG_HOME=") != nullptr ||
            strcasestr(*envy, "HOME=") != nullptr) {
          DBG("Found %s", *envy);
          configPath = *envy;
          auto eq = configPath.find("=");
          if (eq != std::string::npos) {
            configPath = configPath.substr(++eq) + "/.config/" + CFG_NAME;
          }
        }
      }
    }

    if (configPath.empty()) {
      throw VULNYEX("%s", "No Config Specified");
    }
    jSonny sonny(configPath);
    auto vals = { VulnyScenario::ScanType::Stealth,
                  VulnyScenario::ScanType::Knock,
                  VulnyScenario::ScanType::Audit };
    std::vector<VulnyScenario*> scenarios;
    for (const auto& i : vals) {
      auto* scenario = VulnyScenario::build(i);
      if (scenario) {
        if (sonny.parseValue(*scenario) == EXIT_SUCCESS) {
          scenarios.push_back(scenario);
        } else {
          DBG("Could not parse %s -- probably not available in the config",
              scenario->getName().c_str());
          delete scenario;
        }
      }
    }
    for (auto* i : scenarios) {
      i->deploy();
      i->wait();
      delete i;
    }

    return ret;
  } catch (const std::exception& ex) {
    fprintf(stderr, "%s\n", ex.what());
    help(argv[0], CFG_NAME);
    return EXIT_FAILURE;
  }
}
