#include "sntlk.hpp"
#include "debug.hpp"
#include "net_utils.hpp"
#include <arpa/inet.h>
#include <cstring>
#include <netinet/in.h>
#include <unistd.h>

sntlk::sntlk()
  : _buffSize(0x100)
  , _buffer(new uint8_t[_buffSize])
{
  _sofd = socket(AF_NETLINK, SOCK_RAW | SOCK_CLOEXEC, NETLINK_ROUTE);
  if (_sofd < 0) {
    throw VULNYEX("%s",
                  "Could not create socket AF_NETLINK, SOCK_RAW | "
                  "SOCK_CLOEXEC, NETLINK_ROUTE");
  }
  struct sockaddr_nl snlGet;
  socklen_t snlGetSize = sizeof snlGet;
  memset(&snlGet, 0x00, sizeof snlGet);
  memset(&_snl, 0x00, sizeof _snl);
  _snl.nl_family = AF_NETLINK;
  if (bind(_sofd, (const struct sockaddr*)&_snl, sizeof _snl) < 0) {
    throw VULNYEX("%s", "Could not bind NETLINK socket");
  }
  if (getsockname(_sofd, (struct sockaddr*)&snlGet, &snlGetSize) < 0) {
    throw VULNYEX("%s", "Could not getsockname for NETLINK socket");
  } else {
    if (((_snl.nl_family == snlGet.nl_family &&
          _snl.nl_groups == snlGet.nl_groups)) == 0) {
      throw VULNYEX("%s", "Socket name is not the same");
    }
  }
}

sntlk::~sntlk()
{
  if (_sofd >= 0) {
    close(_sofd);
  }
}

int_fast8_t
sntlk::printLinkInfo(const struct linkInfo* linfo)
{
  if (!linfo) {
    ERROR("%s", "Linfo is nullptr");
    return EXIT_FAILURE;
  }
  const struct sockaddr_in* sockin{ nullptr };
  const struct sockaddr_in6* sockin6{ nullptr };
  char ipBuffer[40];
  char ifName[IFNAMSIZ];

  printf("Interface Index %d -- ", linfo->ifindex);
  if (if_indextoname(linfo->ifindex, ifName) != nullptr) {
    ifName[IFNAMSIZ - 1] = '\0';
    printf("Name '%s' -- ", ifName);
  }

  if (linfo->dst.ss_family == AF_INET) {
    sockin = (const struct sockaddr_in*)&linfo->dst;
    printf("Dest Addr %u.%u.%u.%u -- ", IP_FORMAT_NO(sockin->sin_addr.s_addr));
  } else if (linfo->dst.ss_family == AF_INET6) {
    sockin6 = (const struct sockaddr_in6*)&linfo->dst;
    inet_ntop(
      AF_INET6, (const void*)&sockin6->sin6_addr, ipBuffer, sizeof ipBuffer);
    printf("Dest Addr %s -- ", ipBuffer);
  } else {
    DBG("Dst sock type is %d -- ", linfo->dst.ss_family);
  }

  if (linfo->src.ss_family == AF_INET) {
    sockin = (const struct sockaddr_in*)&linfo->src;
    printf("Src Addr %u.%u.%u.%u -- ", IP_FORMAT_NO(sockin->sin_addr.s_addr));
  } else if (linfo->src.ss_family == AF_INET6) {
    sockin6 = (const struct sockaddr_in6*)&linfo->src;
    inet_ntop(
      AF_INET6, (const void*)&sockin6->sin6_addr, ipBuffer, sizeof ipBuffer);
    printf("Src Addr %s -- ", ipBuffer);
  } else {
    DBG("Src sock type is %d", linfo->src.ss_family);
  }

  if (linfo->gateway.ss_family == AF_INET) {
    sockin = (const struct sockaddr_in*)&linfo->gateway;
    printf("Gateway Addr %u.%u.%u.%u -- ",
           IP_FORMAT_NO(sockin->sin_addr.s_addr));
  } else if (linfo->gateway.ss_family == AF_INET6) {
    sockin6 = (const struct sockaddr_in6*)&linfo->gateway;
    inet_ntop(
      AF_INET6, (const void*)&sockin6->sin6_addr, ipBuffer, sizeof ipBuffer);
    printf("Gateway Addr %s -- ", ipBuffer);
  } else {
    DBG("Gateway sock type is %d", linfo->gateway.ss_family);
  }

  printf("\n");

  return EXIT_SUCCESS;
}

int_fast8_t
sntlk::handleRta(const struct rtattr* rta, struct linkInfo* rtnfo)
{
  if (!(rta && rtnfo)) {
    ERROR("Args are nullptr -- rta(%p) rtnfo(%p)",
          (const void*)rta,
          (const void*)rtnfo);
    return EXIT_FAILURE;
  }
  struct in_addr* sina{ nullptr };
  struct in6_addr* sina6{ nullptr };
  struct sockaddr_in* sockin{ nullptr };
  struct sockaddr_in6* sockin6{ nullptr };
  switch (rta->rta_type) {
    case RTA_DST: {
      if (rta->rta_len == RTA_LENGTH(4)) {
        rtnfo->dst.ss_family = AF_INET;
        sockin = (struct sockaddr_in*)&rtnfo->dst;
        sina = (struct in_addr*)RTA_DATA(rta);
        memcpy(&sockin->sin_addr, sina, sizeof *sina);
      } else if (rta->rta_len == RTA_LENGTH(16)) {
        rtnfo->dst.ss_family = AF_INET6;
        sockin6 = (struct sockaddr_in6*)&rtnfo->dst;
        sina6 = (struct in6_addr*)RTA_DATA(rta);
        memcpy(&sockin6->sin6_addr, sina6, sizeof *sina6);
      } else {
        ERROR("Unknown Type for that len %d", rta->rta_len);
      }
      break;
    }

    case RTA_PREFSRC: {
      if (rta->rta_len == RTA_LENGTH(4)) {
        rtnfo->src.ss_family = AF_INET;
        sockin = (struct sockaddr_in*)&rtnfo->src;
        sina = (struct in_addr*)RTA_DATA(rta);
        memcpy(&sockin->sin_addr, sina, sizeof *sina);
      } else if (rta->rta_len == RTA_LENGTH(16)) {
        rtnfo->src.ss_family = AF_INET6;
        sockin6 = (struct sockaddr_in6*)&rtnfo->src;
        sina6 = (struct in6_addr*)RTA_DATA(rta);
        memcpy(
          sockin6->sin6_addr.s6_addr, sina6->s6_addr, sizeof sina6->s6_addr);
      } else {
        ERROR("Unknown Type for that len %d", rta->rta_len);
      }
      break;
    }

    case RTA_GATEWAY: {
      if (rta->rta_len == RTA_LENGTH(4)) {
        rtnfo->gateway.ss_family = AF_INET;
        sockin = (struct sockaddr_in*)&rtnfo->gateway;
        sina = (struct in_addr*)RTA_DATA(rta);
        memcpy(&sockin->sin_addr, sina, sizeof *sina);
      } else if (rta->rta_len == RTA_LENGTH(16)) {
        ERROR("%s", "RTA_GATEWAY");
        rtnfo->gateway.ss_family = AF_INET6;
        sockin6 = (struct sockaddr_in6*)&rtnfo->gateway;
        sina6 = (struct in6_addr*)RTA_DATA(rta);
        memcpy(
          sockin6->sin6_addr.s6_addr, sina6->s6_addr, sizeof sina6->s6_addr);
      } else {
        ERROR("Unknown Type for that len %d", rta->rta_len);
      }
      break;
    }

    case RTA_OIF: {
      rtnfo->ifindex = *(int*)RTA_DATA(rta);
      break;
    }
    default:
      break;
  }
  return EXIT_SUCCESS;
}

int_fast8_t
sntlk::getRoute(const struct sockaddr* dst, struct linkInfo* rtnfo)
{
  std::lock_guard<std::mutex> lg(_lock);

  if (!(dst && rtnfo)) {
    ERROR("%s", "dst pointer is nullptr");
    return EXIT_FAILURE;
  }

  memset(_buffer.get(), 0x00, _buffSize);

  struct nlmsghdr* const nlms = (struct nlmsghdr*)_buffer.get();
  struct rtmsg* const rtms = (struct rtmsg*)NLMSG_DATA(nlms);
  struct rtattr* const rta = (struct rtattr*)RTM_RTA(rtms);
  ssize_t recvLen{ -1 };

  nlms->nlmsg_flags = NLM_F_REQUEST;
  nlms->nlmsg_type = RTM_GETROUTE;
  nlms->nlmsg_seq = net_utils::getRandomGenerator()();
  nlms->nlmsg_pid = getpid();

  rtms->rtm_family = dst->sa_family;
  rtms->rtm_protocol = RTPROT_UNSPEC;
  rtms->rtm_table = RT_TABLE_UNSPEC;
  rtms->rtm_flags = RTM_F_LOOKUP_TABLE;

  rta->rta_type = RTA_DST;

  if (rtms->rtm_family == PF_INET) {

    const struct sockaddr_in* sockin = (const struct sockaddr_in*)dst;
    nlms->nlmsg_len =
      NLMSG_LENGTH(sizeof(struct rtmsg) + sizeof(struct rtattr) + 4);
    rtms->rtm_dst_len = 32;
    rta->rta_len = RTA_LENGTH(4);
    *(uint32_t*)RTA_DATA(rta) = sockin->sin_addr.s_addr;

  } else if (rtms->rtm_family == PF_INET6) {

    const struct sockaddr_in6* sockin6 = (const struct sockaddr_in6*)dst;
    nlms->nlmsg_len =
      NLMSG_LENGTH(sizeof(struct rtmsg) + sizeof(struct rtattr) + 16);
    rtms->rtm_dst_len = 128;
    rta->rta_len = RTA_LENGTH(16);
    memcpy(RTA_DATA(rta),
           sockin6->sin6_addr.s6_addr,
           sizeof sockin6->sin6_addr.s6_addr);

  } else {

    ERROR("Unknown Protocol Family %d", rtms->rtm_family);
    return EXIT_FAILURE;
  }

  struct iovec iov
  {
    nlms, nlms->nlmsg_len
  };
  struct msghdr msg
  {
    &_snl, sizeof _snl, &iov, 1, nullptr, 0, 0
  };

  if (sendmsg(_sofd, &msg, 0) <= 0) {
    ERROR("Could not set routeGet request: %d", errno);
    return EXIT_FAILURE;
  }

  iov.iov_base = _buffer.get();
  iov.iov_len = _buffSize;

  if ((recvLen = recvmsg(_sofd, &msg, 0)) <= 0) {
    ERROR("Did not receive routeGet response %d", errno);
    return EXIT_FAILURE;
  }

  if (nlms->nlmsg_type == NLMSG_ERROR) {
    const struct nlmsgerr* const nlmserr = (struct nlmsgerr*)NLMSG_DATA(nlms);
    DBG("routeGet received error %d", nlmserr->error);
    return EXIT_FAILURE;
  }

  ssize_t rtasize = nlms->nlmsg_len;
  for (struct rtattr* rti = RTM_RTA(rtms); RTA_OK(rti, rtasize);
       rti = RTA_NEXT(rti, rtasize)) {
    handleRta(rti, rtnfo);
  }
  return EXIT_SUCCESS;
}

int_fast8_t
sntlk::ipOperationHandler(const struct sockaddr* addr,
                          int ifIndex,
                          int_fast8_t operation)
{
  std::lock_guard<std::mutex> lg(_lock);

  if (!addr) {
    ERROR("%s", "addr is nullptr");
    return EXIT_FAILURE;
  }

  memset(_buffer.get(), 0x00, _buffSize);
  struct nlmsghdr* const nlms = (struct nlmsghdr*)_buffer.get();
  struct ifaddrmsg* const ifaddr = (struct ifaddrmsg*)NLMSG_DATA(nlms);
  struct rtattr* rta = (struct rtattr*)IFA_RTA(ifaddr);
  ssize_t recvLen{ -1 };
  int_fast8_t ret = EXIT_SUCCESS;

  if (operation == sntlk::IP_ADD) {
    nlms->nlmsg_type = RTM_NEWADDR;
    nlms->nlmsg_flags = NLM_F_REQUEST | NLM_F_ACK | NLM_F_CREATE | NLM_F_EXCL;
  } else if (operation == sntlk::IP_DEL) {
    nlms->nlmsg_type = RTM_DELADDR;
    nlms->nlmsg_flags = NLM_F_REQUEST | NLM_F_ACK;
  } else {
    ERROR("Unknown operation type %d", (int)operation);
    return EXIT_FAILURE;
  }

  nlms->nlmsg_seq = net_utils::getRandomGenerator()();
  nlms->nlmsg_pid = getpid();

  ifaddr->ifa_index = ifIndex;

  if (addr->sa_family == AF_INET) {

    nlms->nlmsg_len = NLMSG_LENGTH(sizeof(struct ifaddrmsg) +
                                   2 * sizeof(struct rtattr) + 2 * 4);

    const struct sockaddr_in* const socka = (const struct sockaddr_in*)addr;
    ifaddr->ifa_prefixlen = socka->sin_port;
    ifaddr->ifa_scope = RT_SCOPE_UNIVERSE;
    ifaddr->ifa_family = AF_INET;

    rta->rta_len = RTA_LENGTH(4);
    rta->rta_type = IFA_LOCAL;
    *((uint32_t*)RTA_DATA(rta)) = socka->sin_addr.s_addr;

    {
      ssize_t rtalen = _buffSize - NLMSG_LENGTH(sizeof(struct ifaddrmsg) +
                                                sizeof(struct rtattr) + 4);
      rta = RTA_NEXT(rta, rtalen);
    }

    rta->rta_len = RTA_LENGTH(4);
    rta->rta_type = IFA_ADDRESS;
    *((uint32_t*)RTA_DATA(rta)) = socka->sin_addr.s_addr;

  } else if (addr->sa_family == AF_INET6) {

    nlms->nlmsg_len = NLMSG_LENGTH(sizeof(struct ifaddrmsg) +
                                   2 * sizeof(struct rtattr) + 2 * 16);

    const struct sockaddr_in6* const socka6 = (const struct sockaddr_in6*)addr;
    ifaddr->ifa_prefixlen = socka6->sin6_port;
    ifaddr->ifa_scope = RT_SCOPE_UNIVERSE;
    ifaddr->ifa_family = AF_INET6;

    rta->rta_len = RTA_LENGTH(16);
    rta->rta_type = IFA_LOCAL;
    memcpy(RTA_DATA(rta),
           &socka6->sin6_addr.s6_addr,
           sizeof socka6->sin6_addr.s6_addr);

    {
      ssize_t rtalen = _buffSize - NLMSG_LENGTH(sizeof(struct ifaddrmsg) +
                                                sizeof(struct rtattr) + 16);
      rta = RTA_NEXT(rta, rtalen);
    }

    rta->rta_len = RTA_LENGTH(16);
    rta->rta_type = IFA_ADDRESS;
    memcpy(RTA_DATA(rta),
           &socka6->sin6_addr.s6_addr,
           sizeof socka6->sin6_addr.s6_addr);
  }

  struct iovec iov
  {
    nlms, nlms->nlmsg_len
  };
  struct msghdr msg
  {
    &_snl, sizeof _snl, &iov, 1, nullptr, 0, 0
  };

  if (sendmsg(_sofd, &msg, 0) <= 0) {
    ERROR("Could not set routeGet request: %d", errno);
    return EXIT_FAILURE;
  }

  iov.iov_base = _buffer.get();
  iov.iov_len = _buffSize;

  if ((recvLen = recvmsg(_sofd, &msg, 0)) <= 0) {
    ERROR("Did not receive routeGet responde %d", errno);
    return EXIT_FAILURE;
  }

  if (nlms->nlmsg_type == NLMSG_ERROR) {
    struct nlmsgerr* const nlserr = (struct nlmsgerr*)NLMSG_DATA(nlms);
    if (nlserr->error == 0) {
      ret = EXIT_SUCCESS;
    } else {
      ERROR("Received error from kernel %d", nlserr->error);
      ret = EXIT_FAILURE;
    }
  }

  return ret;
}

int_fast8_t
sntlk::delIp(const struct sockaddr* addr, int ifIndex)
{
  if (ipOperationHandler(addr, ifIndex, sntlk::IP_DEL) == EXIT_SUCCESS) {
    return EXIT_SUCCESS;
  } else {
    ERROR("Failed to delete address for ifIndex(%d)", ifIndex);
    return EXIT_FAILURE;
  }
}

int_fast8_t
sntlk::addIp(const struct sockaddr* addr, int ifIndex)
{
  if (ipOperationHandler(addr, ifIndex, sntlk::IP_ADD) == EXIT_SUCCESS) {
    return EXIT_SUCCESS;
  } else {
    ERROR("Failed to add address for ifIndex(%d)", ifIndex);
    return EXIT_FAILURE;
  }
}
