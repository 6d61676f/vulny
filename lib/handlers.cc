#include "handlers.hpp"
#include "sntlk.hpp"
#include <algorithm>
#include <cinttypes>
#include <cmath>
#include <limits>
#include <netinet/ip_icmp.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <thread>

/**
 * @file handlers.cc
 * @todo Check that all public API functions are thread safe
 */

genericV4Handler::genericV4Handler(size_t inBuffSize, size_t outBuffSize)
  : _IO(asyncIo::getInstance())
  , _inBuffsize(inBuffSize)
  , _outBuffsize(sizeof(struct iphdr) + outBuffSize)
  , _inBuffer(std::unique_ptr<uint8_t[]>(new uint8_t[_inBuffsize]()))
  , _outBuffer(std::unique_ptr<uint8_t[]>(new uint8_t[_outBuffsize]()))

{
  memset(&_sockin, 0x00, sizeof _sockin);
  _sockin.sin_family = AF_INET;
}

genericV4Handler::~genericV4Handler()
{
  if (_registered && _sofd >= 0) {
    unregisterSocket();
  }
  if (_sofd >= 0) {
    close(_sofd);
  }
}

uint_fast8_t
genericV4Handler::registerSocket()
{
  std::unique_lock<std::mutex> li(_ingressLock, std::defer_lock);
  std::unique_lock<std::mutex> le(_egressLock, std::defer_lock);
  std::lock(li, le);
  if (_IO.registerDescriptor(_sofd, _events, this) == EXIT_SUCCESS) {
    _registered = true;
    return EXIT_SUCCESS;
  }
  return EXIT_FAILURE;
}

uint_fast8_t
genericV4Handler::unregisterSocket()
{
  std::unique_lock<std::mutex> li(_ingressLock, std::defer_lock);
  std::unique_lock<std::mutex> le(_egressLock, std::defer_lock);
  std::lock(li, le);
  if (_IO.unregisterDescriptor(_sofd) == EXIT_SUCCESS) {
    _registered = false;
    return EXIT_SUCCESS;
  }
  return EXIT_FAILURE;
}

int_fast8_t
genericV4Handler::setIpDst(std::string ipDst)
{
  struct in_addr newVal
  {
    0
  };
  if (inet_pton(AF_INET, ipDst.c_str(), &newVal) == 1) {
    if (newVal.s_addr == _ipDst.s_addr) {
      return EXIT_SUCCESS;
    }
  } else {
    ERROR("Could not set destination IP address %s", ipDst.c_str());
    return EXIT_FAILURE;
  }

  std::lock_guard<std::mutex> lg(_egressLock);
  _modified |= mDstIp;
  _ipDst.s_addr = newVal.s_addr;
  return EXIT_SUCCESS;
}

int_fast8_t
genericV4Handler::setIpSrc(std::string ipSrc)
{
  struct in_addr newVal
  {
    0
  };

  if (inet_pton(AF_INET, ipSrc.c_str(), &newVal) == 1) {
    if (newVal.s_addr == _ipSrc.s_addr) {
      return EXIT_SUCCESS;
    }
  } else {
    ERROR("Could not set destination IP address %s", ipSrc.c_str());
    return EXIT_FAILURE;
  }

  std::lock_guard<std::mutex> lg(_egressLock);
  _modified |= mSrcIp;
  _ipSrc.s_addr = newVal.s_addr;
  return EXIT_SUCCESS;
}

int_fast8_t
genericV4Handler::setIpDst(uint32_t ipDst)
{
  std::lock_guard<std::mutex> lg(_egressLock);
  _modified |= mDstIp;
  _ipDst.s_addr = htonl(ipDst);
  return EXIT_SUCCESS;
}

int_fast8_t
genericV4Handler::setIpSrc(uint32_t ipSrc)
{
  std::lock_guard<std::mutex> lg(_egressLock);
  _modified |= mSrcIp;
  _ipSrc.s_addr = htonl(ipSrc);
  return EXIT_SUCCESS;
}

int_fast8_t
genericV4Handler::setPortDst(uint_fast16_t dport)
{
  if (dport == ntohs(_dport)) {
    return EXIT_SUCCESS;
  }
  std::lock_guard<std::mutex> lg(_egressLock);
  _dport = htons(dport);
  _modified |= mDstPrt;
  return EXIT_SUCCESS;
}

int_fast8_t
genericV4Handler::setPortSrc(uint_fast16_t sport)
{
  if (sport == ntohs(_sport)) {
    return EXIT_SUCCESS;
  }
  std::lock_guard<std::mutex> lg(_egressLock);
  _sport = htons(sport);
  _modified |= mSrcPrt;
  return EXIT_SUCCESS;
}

int_fast8_t
genericV4Handler::_processChanges(void)
{
  uint8_t* const buffer = _outBuffer.get();
  struct iphdr* const ip = (struct iphdr*)buffer;

  // Anything IP Related
  if (_modified & (mSrcIp | mDstIp)) {
    if (net_utils::prepareIpHdr((uint8_t*)ip, _outBuffsize) == EXIT_FAILURE) {
      ERROR("%s", "Could not prepare iphdr");
    }
  }

  // Modified ipSrc
  if (_modified & mSrcIp) {
    ip->saddr = _ipSrc.s_addr;
  }

  // Modified ipDst
  if (_modified & mDstIp) {
    ip->daddr = _ipDst.s_addr;
    _sockin.sin_addr = _ipDst;
  }

  // Modified dport
  if (_modified & mDstPrt) {
    _sockin.sin_port = _dport;
  }

  if (ip->saddr == 0) {
    struct sockaddr_in sockin;
    struct sntlk::linkInfo linfo;
    memset(&sockin, 0x00, sizeof sockin);
    memset(&linfo, 0x00, sizeof linfo);
    sockin.sin_family = AF_INET;
    sockin.sin_addr.s_addr = _ipDst.s_addr;

    if (sntlk().getRoute((const struct sockaddr*)&sockin, &linfo) ==
        EXIT_SUCCESS) {
      _ipSrc.s_addr = ((struct sockaddr_in*)&(linfo.src))->sin_addr.s_addr;
      ip->saddr = _ipSrc.s_addr;
    } else {
      DBG("Could not get source IP for destination IP %u.%u.%u.%u",
          IP_FORMAT_NO(_ipDst.s_addr));
    }
  }
  return EXIT_SUCCESS;
}

int
genericV4Handler::getFd() const
{
  return _sofd;
}

uint_fast32_t
genericV4Handler::getEvents() const
{
  return _events;
}

l2Handler::l2Handler(int domain, int protocol)
{
  _events = EPOLLIN;
  if (!(domain && protocol)) {
    _sofd = -1;
    _layer = 2;
    return;
  }

  if (domain == AF_PACKET) {
    _layer = 2;
    protocol = htons(protocol);
  } else if (domain == AF_INET || domain == AF_INET6) {
    _layer = 3;
  } else {
    throw VULNYEX("Domain supplied is not supported %d", domain);
  }

  _sofd = socket(domain, SOCK_RAW, protocol);
  if (_sofd < 0) {
    throw VULNYEX(
      "Could not create socket with supplied domain(%d) and protocol(%d)",
      domain,
      protocol);
  }
}

l2Handler::~l2Handler()
{
  if (_sofd >= 0) {
    close(_sofd);
  }
}

uint_fast32_t
l2Handler::process(int sofd, uint_fast32_t events)
{
  if (events & EPOLLIN) {
    ssize_t recv{ 0 };
    uint8_t buffer[1000];
    while ((recv = read(sofd, buffer, sizeof buffer)) > 0) {
      net_utils::printPacket(buffer, recv, _layer);
    }
  }
  return EXIT_SUCCESS;
}

ipSniffer::ipSniffer(int sofd)
{
  if (sofd == -1) {
    _sofd = socket(AF_PACKET, SOCK_DGRAM, htons(0x0806));
  } else {
    _sofd = sofd;
  }
  if (_sofd == -1) {
    throw VULNYEX("%s", "Could not create socket");
  }
  _events = EPOLLIN;
}

ipSniffer::~ipSniffer()
{
  if (_sofd != -1) {
    close(_sofd);
  }
}

uint_fast32_t
ipSniffer::process(int sofd, uint_fast32_t events)
{
  std::unique_lock<std::mutex> lg(_lock, std::defer_lock);
  if (lg.try_lock() == false) {
    DBG("%s", "Already processing");
    return EXIT_SUCCESS;
  }
  if (events & EPOLLIN) {
    ssize_t recvLen = 0;
    uint8_t buffer[100]{ 0x00 };
    while ((recvLen = recv(sofd, buffer, sizeof buffer, 0)) > 0) {
      if (recvLen >= (ssize_t)sizeof(struct ether_arp)) {
        struct ether_arp arp;
        memcpy(&arp, buffer, sizeof arp);
        _arps.push_back(arp);
        DBG("%s", "Pushed arp to vector");
      }
      memset(buffer, 0x00, sizeof buffer);
    }
    if (errno == EWOULDBLOCK || errno == EAGAIN) {
      DBG("%s", "Would block");
      return EXIT_SUCCESS;
    } else {
      DBG("Errno %d", errno);
      return EXIT_FAILURE;
    }
  } else {
    ERROR("Only EPOLLIN is supported. Supplied %u.", (unsigned)events);
    return EXIT_FAILURE;
  }
}

void
ipSniffer::printArps()
{
  std::lock_guard<std::mutex> lg(_lock);
  for (const auto& i : _arps) {
    net_utils::printArp((const uint8_t*)&i, sizeof i);
  }
}

arPing::arPing()
  : genericV4Handler(1000, 1000)
{
  _events = EPOLLIN;
  _sofd = socket(AF_PACKET, SOCK_DGRAM, htons(ETH_P_ARP));
  if (_sofd < 0) {
    throw VULNYEX("%s", "Could not create ARP PACKET socket");
  }
  memset(&_sockl, 0x00, sizeof _sockl);
  _sockl.sll_family = AF_PACKET;
  _sockl.sll_protocol = htons(ETH_P_ARP);
  _sockl.sll_halen = ETHER_ADDR_LEN;
}

arPing::arPing(std::string ifName)
  : arPing()
{
  if (net_utils::getHwAddr(ifName.c_str(), _sockl.sll_addr) != EXIT_SUCCESS) {
    throw VULNYEX("Invalid ifName %s", ifName.c_str());
  }
  _ifName = ifName;
  _sockl.sll_ifindex = if_nametoindex(ifName.c_str());
}

arPing::arPing(int ifIndex)
  : arPing()
{
  char ifName[IFNAMSIZ]{ 0x00 };
  if (if_indextoname(ifIndex, ifName) == 0) {
    throw VULNYEX("Invalid ifIndex %d", ifIndex);
  }
  if (net_utils::getHwAddr(ifName, _sockl.sll_addr) != EXIT_SUCCESS) {
    throw VULNYEX("Invalid ifName %s", ifName);
  }
  _ifName = ifName;
  _sockl.sll_ifindex = ifIndex;
}

int
arPing::getIfIndex(void) const
{
  DBG("Interface %s Index %d", this->_ifName.c_str(), this->_sockl.sll_ifindex);
  return this->_sockl.sll_ifindex;
}

int
arPing::getIfIndex(std::string& intName) const
{
  DBG("Interface %s Index %d", this->_ifName.c_str(), this->_sockl.sll_ifindex);
  intName = this->_ifName;
  return this->_sockl.sll_ifindex;
}

uint_fast8_t
arPing::sendArpPing()
{
  std::unique_lock<std::mutex> eg(_egressLock, std::defer_lock);
  std::unique_lock<std::mutex> ig(_ingressLock, std::defer_lock);
  std::lock(eg, ig);

  ssize_t sndLen{ 0 };
  struct ether_arp arp;
  memset(&arp, 0x00, sizeof arp);
  struct sockaddr_ll sockSend;

  memset(&sockSend, 0x00, sizeof sockSend);
  memcpy(&sockSend, &_sockl, sizeof sockSend);
  memset(sockSend.sll_addr, 0xff, ETHER_ADDR_LEN);

  if (net_utils::prepareArpRequest((uint8_t*)&arp, 0, sizeof arp) ==
      EXIT_FAILURE) {
    ERROR("%s", "Could not prepare arp request packet");
    return EXIT_FAILURE;
  }

  if (net_utils::getHwAddr(_ifName.c_str(), arp.arp_sha) == EXIT_FAILURE) {
    ERROR("Could not get MAC for %s", _ifName.c_str());
    return EXIT_FAILURE;
  }

  if (net_utils::ipV4ToChar(arp.arp_spa, ntohl(_ipSrc.s_addr)) ==
      EXIT_FAILURE) {
    ERROR("Could not set %u.%u.%u.%u", IP_FORMAT_NO(_ipSrc.s_addr));
    return EXIT_FAILURE;
  }
  if (net_utils::ipV4ToChar(arp.arp_tpa, ntohl(_ipDst.s_addr)) ==
      EXIT_FAILURE) {
    ERROR("Could not set %u.%u.%u.%u", IP_FORMAT_NO(_ipDst.s_addr));
    return EXIT_FAILURE;
  }

  if ((sndLen = sendto(_sofd,
                       &arp,
                       sizeof arp,
                       0,
                       (const struct sockaddr*)&sockSend,
                       sizeof sockSend)) <= 0) {

    if (errno == EAGAIN || errno == EWOULDBLOCK) {
      return EXIT_SUCCESS;
    } else {
      ERROR("%s", "Could not send ARP packet");
      return EXIT_FAILURE;
    }
  } else {
    if (_arpReplies.find(_ipDst.s_addr) == _arpReplies.end()) {
      _arpReplies[_ipDst.s_addr] = 0;
    }
    return EXIT_SUCCESS;
  }
}

uint_fast8_t
arPing::_handleArpReply(const struct ether_arp* arp)
{
  uint_fast32_t numeric{ 0 };
  if (net_utils::charToIpV4(arp->arp_spa, &numeric) == EXIT_SUCCESS &&
      net_utils::isValidMac(arp->arp_spa) == true) {
    if (_arpReplies.find(htonl(numeric)) != _arpReplies.end()) {
      net_utils::printArp((const uint8_t*)arp, sizeof *arp);
      _arpReplies[htonl(numeric)]++;
    } else {
      DBG("Not my required IP:%u.%u.%u.%u instead of %u.%u.%u.%u",
          IP_FORMAT_NO(numeric),
          IP_FORMAT_NO(_ipDst.s_addr));
    }
    return EXIT_SUCCESS;
  } else {
    return EXIT_FAILURE;
  }
}

uint_fast32_t
arPing::process(int sofd, uint_fast32_t events)
{
  std::unique_lock<std::mutex> eg(_egressLock, std::defer_lock);
  std::unique_lock<std::mutex> ig(_ingressLock, std::defer_lock);
  if (std::try_lock(eg, ig) == -1) {
    DBG("%s", "Already processing somehting else");
    return EXIT_SUCCESS;
  }
  if (events & EPOLLIN) {
    struct ether_arp arp;
    ssize_t rcvLen{ 0 };
    while ((rcvLen = read(sofd, &arp, sizeof arp)) == sizeof arp) {
      this->_handleArpReply(&arp);
    }
    if (errno == EWOULDBLOCK || errno == EAGAIN) {
      DBG("%s", "Would block");
      return EXIT_SUCCESS;
    } else {
      ERROR("%s", "recv error");
      return EXIT_FAILURE;
    }
  } else {
    ERROR("Received some strange events(%u)", (unsigned)events);
    return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}

arPing::~arPing() {}

ipSniffer2::ipSniffer2()
  : genericV4Handler(1000, 0)
{
  _events = EPOLLIN;
  _sofd = socket(PF_PACKET, SOCK_DGRAM | SOCK_NONBLOCK, htons(ETH_P_ARP));
  if (net_utils::getMyIps4(_ignoredIps) == EXIT_FAILURE) {
    ERROR("%s", "Could not detect locally configured IPv4s");
  }
  if (_sofd < 0) {
    throw VULNYEX("%s",
                  "Could not create socket PF_PACKET, SOCK_DGRAM, ETH_P_ARP");
  }
}

void
ipSniffer2::_processArp(uint8_t* buffer)
{
  struct ether_arp* arp = (struct ether_arp*)buffer;
  uint_fast32_t ipSrc{ 0 }, ipDst{ 0 };
  uint_fast64_t hwaddr{ 0x00 };

  if (net_utils::charToIpV4((const uint8_t*)arp->arp_spa, &ipSrc) ==
      EXIT_SUCCESS) {
    if (net_utils::isValidIpV4(ipSrc) == true) {
      _ips[ipSrc]++;
      DBG("Adding IPv4 %u.%u.%u.%u", IP_FORMAT(ipSrc));
      if (net_utils::isValidMac(arp->arp_sha) == true) {
        net_utils::charToMac(arp->arp_sha, &hwaddr);
        _arps[ipSrc] = hwaddr;
        DBG("Adding MAC for IPv4 %u.%u.%u.%u", IP_FORMAT(ipSrc));
      }
    }
  }

  if (net_utils::charToIpV4((const uint8_t*)arp->arp_tpa, &ipDst) ==
      EXIT_SUCCESS) {
    if (net_utils::isValidIpV4(ipDst) == true) {
      _ips[ipDst]++;
      DBG("Adding IPv4 %u.%u.%u.%u", IP_FORMAT(ipDst));
      if (net_utils::isValidMac(arp->arp_tha) == true) {
        net_utils::charToMac(arp->arp_tha, &hwaddr);
        _arps[ipDst] = hwaddr;
        DBG("Adding MAC for IPv4 %u.%u.%u.%u", IP_FORMAT(ipDst));
      }
    }
  }
}

uint_fast32_t
ipSniffer2::process(int sofd, uint_fast32_t events)
{
  std::unique_lock<std::mutex> lg(_egressLock, std::defer_lock);
  if (lg.try_lock() == false) {
    DBG("%s", "Already processing");
    return EXIT_SUCCESS;
  }
  uint8_t* const buffer{ _inBuffer.get() };
  ssize_t recvLen{ 0 };
  if (events) {
    DBG("Events are %u", (unsigned)events);
  } else {
    ERROR("%s", "Events is zero");
  }
  while ((recvLen = read(sofd, buffer, _inBuffsize)) > 0) {
    if (recvLen < (ssize_t)sizeof(struct ether_arp)) {
      ERROR("%s", "Received some strange packet");
      net_utils::dumpPacket(buffer, recvLen);
    } else {
      this->_processArp(buffer);
    }
  }
  if (recvLen < 0 && (errno == EWOULDBLOCK || errno == EAGAIN)) {
    DBG("%s", "Would have blocked...");
  } else {
    ERROR("%s", "Something is wrong with the file descriptor");
  }
  return EXIT_SUCCESS;
}

const std::map<uint_fast32_t, uint_fast64_t>&
ipSniffer2::getArps()
{
  return _arps;
}

ipSniffer2::~ipSniffer2() {}

const std::map<uint_fast32_t, uint_fast32_t>&
ipSniffer2::getIps()
{
  return _ips;
}

uint_fast32_t
ipSniffer2::_getPotentialGateway()
{
  std::pair<uint_fast32_t, uint_fast32_t> max{ 0, 0 };
  uint_fast32_t gateway{ 0 };
  std::for_each(_ips.begin(), _ips.end(), [&](decltype(*_ips.begin()) elem) {
    if (elem.second >= max.second &&
        _ignoredIps.find(elem.first) == _ignoredIps.end()) {
      gateway = elem.first;
      max = elem;
    }
  });

  if (_arps.find(gateway) == _arps.end()) {
    DBG("%s", "No Gateway Found");
    gateway = 0;
  }

  return gateway;
}

uint_fast32_t
ipSniffer2::getPotentialGateway()
{
  std::unique_lock<std::mutex> le(_egressLock, std::defer_lock);
  std::unique_lock<std::mutex> li(_ingressLock, std::defer_lock);
  std::lock(le, li);

  return this->_getPotentialGateway();
}

uint_fast64_t
ipSniffer2::getPotentialNetwork(int_fast8_t method)
{
  std::unique_lock<std::mutex> le(_egressLock, std::defer_lock);
  std::unique_lock<std::mutex> li(_ingressLock, std::defer_lock);
  std::lock(le, li);

  uint_fast64_t val{ 0 };
  DBG("Getting network info using method %d", method);
  switch (method) {
    case 1: {
      val = this->_getPotentialNetworkV1();
      break;
    }
    case 2: {
      val = this->_getPotentialNetworkV2();
      break;
    }
    default:
    case 3: {
      val = this->_getPotentialNetworkV3();
      break;
    }
  }
  return val;
}

uint_fast64_t
ipSniffer2::_getPotentialNetworkV1()
{
  if (_arps.size() < 2) {
    DBG("%s", "Not enough IPs to determine network");
    return 0;
  }
  // XXX Hackish as fuck but works on simple networks
  // I've seen that the xor between the biggest and smallest ips gives us a
  // number whose (width) represents the zero portion of the netmask
  auto lowEnd = _arps.begin();
  auto highEnd = _arps.end();
  highEnd--;
  uint8_t lowHigh{ 1 };
  uint_fast32_t minIp = lowEnd->first;
  uint_fast32_t maxIp = highEnd->first;
  uint_fast32_t xorry = minIp ^ maxIp;

  DBG("xorry is %u", (unsigned)xorry);

  // XXX If the xor would occupy less than 3 bits or more than 30
  while ((xorry <= 3 || xorry >= 0x40000000) && highEnd != lowEnd) {

    if (lowHigh) {
      highEnd--;
    } else {
      lowEnd++;
    }

    lowHigh = !lowHigh;
    maxIp = highEnd->first;
    minIp = lowEnd->first;
    xorry = maxIp ^ minIp;
    DBG("Max Ip %u.%u.%u.%u", IP_FORMAT(maxIp));
    DBG("Min Ip %u.%u.%u.%u", IP_FORMAT(minIp));
    DBG("new xorry is %u", (unsigned)xorry);
  }

  if (xorry == 0) {
    DBG("%s", "Not enough IPs to determine network");
    return 0;
  }

  uint8_t bits{ 0 };
  uint_fast32_t mask{ 3 };
  uint_fast32_t network = maxIp;

  while (xorry) {
    mask |= (1 << bits++);
    xorry >>= 1;
  }

  mask = ~mask;
  network &= mask;

  DBG("network could be %u.%u.%u.%u", IP_FORMAT(network));
  DBG("mask could be %u.%u.%u.%u", IP_FORMAT(mask));

  return ((uint_fast64_t)mask << 32 | network);
}

uint_fast64_t
ipSniffer2::_getPotentialNetworkV2()
{
  if (_arps.size() < 2) {
    DBG("%s", "Not enough IPs to determine network");
    return 0;
  }
  // Let's do it the Gauss way
  uint_fast32_t N = _arps.size();
  uint_fast64_t sum = 0;
  double media = 0.0;
  double dev = 0.0;
  std::for_each(_arps.begin(),
                _arps.end(),
                [&](const std::pair<uint_fast32_t, uint_fast64_t>& it) {
                  sum += it.first;
                });
  media = (double)sum / N;

  std::for_each(_arps.begin(),
                _arps.end(),
                [&](const std::pair<uint_fast32_t, uint_fast64_t>& it) {
                  dev += ((it.first - media) * (it.first - media));
                });
  dev /= (N - 1);
  dev = sqrt(dev);

  uint_fast32_t maxIp{ 0 }, minIp{ 0 };
  auto lowEnd = _arps.lower_bound(media - 3 * dev);
  auto highEnd = _arps.lower_bound(media + 3 * dev);
  double auxDev = dev;

  while (lowEnd == _arps.end()) {
    auxDev--;
    lowEnd = _arps.lower_bound(media - 3 * auxDev);
  }

  auxDev = dev;
  while (highEnd == _arps.end()) {
    auxDev--;
    highEnd = _arps.lower_bound(media + 3 * auxDev);
  }

  maxIp = highEnd->first;
  minIp = lowEnd->first;
  uint_fast32_t xorry = maxIp ^ minIp;

  if (xorry == 0) {
    ERROR("%s", "Not enough IPs to determine");
    return 0;
  }

  uint_fast8_t bits{ 0 };
  uint_fast32_t mask{ 3 };
  uint_fast32_t network = maxIp;

  while (xorry) {
    mask |= (1 << bits++);
    xorry >>= 1;
  }

  mask = ~mask;
  network &= mask;

  DBG("Network could be %u.%u.%u.%u", IP_FORMAT(network));
  DBG("Mask could be %u.%u.%u.%u", IP_FORMAT(mask));

  return ((uint_fast64_t)mask << 32 | network);
}

uint_fast64_t
ipSniffer2::_getPotentialNetworkV3()
{
  uint_fast8_t mask{ 0 };
  std::map<uint_fast8_t, uint_fast16_t> masks;
  std::pair<uint_fast8_t, uint_fast16_t> max{ 0, 0 };
  uint_fast32_t gateway{ 0 };
  uint_fast32_t maskExtended{ 0 };

  if (_arps.size() < 2) {
    DBG("%s", "Not enough IPs to determine network");
    return 0;
  }

  for (auto i = _arps.begin(); i != _arps.end(); i++) {
    for (auto j = i; j != _arps.end(); j++) {
      if (i == j) {
        continue;
      }

      mask = net_utils::getIpV4Mask(i->first, j->first);
      if (mask && mask <= 30) {
        masks[mask]++;
      }
    }
  }

  std::for_each(
    masks.begin(),
    masks.end(),
    [&](std::pair<uint_fast8_t, uint_fast16_t> elem) {
      if (elem.second > max.second) {
        max = elem;
        DBG("New max is %u - %u", (unsigned)max.first, (unsigned)max.second);
      }
    });

  gateway = _getPotentialGateway();
  mask = max.first;
  if (net_utils::convertIpV4Mask(&mask, &maskExtended) == EXIT_FAILURE) {
    ERROR("%s", "IpV4 Mask conversion has failed");
    return 0;
  } else {
    gateway &= maskExtended;
    DBG("Network: %u.%u.%u.%u - Netmask: %u.%u.%u.%u",
        IP_FORMAT(gateway),
        IP_FORMAT(maskExtended));
    return ((uint_fast64_t)maskExtended << 32 | gateway);
  }
}

size_t
ipSniffer2::getUnusedIps(std::set<uint_fast32_t>& ips, class arPing& arpy)
{
  uint_fast64_t netInfo{ 0 };
  uint_fast32_t network{ 0 };
  uint_fast32_t netmask{ 0 };
  uint_fast32_t broadcast{ 0 };
  const struct timespec wait
  {
    0, 500000000
  };

  {
    std::unique_lock<std::mutex> le(_egressLock, std::defer_lock);
    std::unique_lock<std::mutex> li(_ingressLock, std::defer_lock);
    std::lock(le, li);

    if (_arps.size() < 2) {
      DBG("%s", "Not enough ARPs to determine IPs");
      return 0;
    }

    netInfo = _getPotentialNetworkV3();
    network = netInfo & 0xFFFFFFFF;
    netmask = (netInfo >> 32) & 0xFFFFFFFF;
    broadcast = (network | (~netmask)) & 0xFFFFFFFF;

    DBG("NETWORK %u.%u.%u.%u  BROADCAST %u.%u.%u.%u",
        IP_FORMAT((network)),
        IP_FORMAT(broadcast));

    arpy.setIpSrc(broadcast - 1);
    _ignoredIps.insert(broadcast - 1);
    for (uint_fast32_t ip = network + 1; ip < broadcast; ip++) {
      if (this->_arps.find(ip) == std::end(_arps)) {
        DBG("arPinging: %u.%u.%u.%u", IP_FORMAT(ip));
        nanosleep(&wait, nullptr);
        arpy.setIpDst(ip);
        arpy.sendArpPing();
      }
    }
  }

  // NOTE We fuck up if we try to reacquire the lock here...
  // TODO I should rethink this here...it looks like crap
  this->process(this->_sofd, this->_events);

  {
    std::unique_lock<std::mutex> le(_egressLock, std::defer_lock);
    std::unique_lock<std::mutex> li(_ingressLock, std::defer_lock);
    std::lock(le, li);

    std::string ifName;
    uint8_t hwAddr[IFHWADDRLEN]{ 0 };
    uint_fast64_t macAddr{ 0 };
    arpy.getIfIndex(ifName);

    for (uint_fast32_t ip = network + 1; ip < broadcast; ip++) {
      if (this->_arps.find(ip) == std::end(_arps)) {
        ips.insert(ip);
        DBG("Adding IPv4(%u.%u.%u.%u)", IP_FORMAT(ip));
      }
    }
    if (net_utils::getHwAddr(ifName.c_str(), hwAddr) == EXIT_FAILURE) {
      ERROR("%s\n%s",
            "getHwAddr has failed with arpy provided interface."
            "This should not happen...");
    } else {
      net_utils::charToMac(hwAddr, &macAddr);
      auto brd = _arps.find(broadcast - 1);
      if (brd != _arps.end() && brd->second == macAddr) {
        ips.insert(broadcast - 1);
        DBG("Inserting source address used for arPing(%u%u%u%u)",
            IP_FORMAT((broadcast - 1)));
      } else {
        printf("Address used for arPing(%u.%u.%u.%u) was already in the "
               "network. Caution!\n",
               IP_FORMAT((broadcast - 1)));
        printf("Our MAC:%" PRIuFAST64 " ,theirs: %" PRIuFAST64 "\n",
               macAddr,
               brd->second);
      }
    }
    return ips.size();
  }
}

void
ipSniffer2::printClients()
{
  std::unique_lock<std::mutex> li(this->_ingressLock, std::defer_lock);
  std::unique_lock<std::mutex> le(this->_egressLock, std::defer_lock);
  std::lock(le, li);

  uint_fast32_t gateway{ 0 };
  uint_fast64_t network{ 0 };

  net_utils::printBanner(std::string("IPv4 Heatmap"));
  std::for_each(_arps.begin(), _arps.end(), [&](decltype(*_arps.begin())& val) {
    printf("%u.%u.%u.%u => %#6" PRIxFAST64 " => %-5" PRIuFAST32 "\n",
           IP_FORMAT(val.first),
           val.second,
           _ips[val.first]);
  });
  if ((gateway = this->_getPotentialGateway()) > 0) {
    printf("Gateway could be %u.%u.%u.%u\n", IP_FORMAT(gateway));
  }
  if ((network = this->_getPotentialNetworkV3()) > 0) {
    printf("Network could be %u.%u.%u.%u\n", IP_FORMAT(network));
    printf("Netmask could be %u.%u.%u.%u\n", IP_FORMAT(network >> 32));
  }
}

icmpHandler::icmpHandler()
  : genericV4Handler(1000, 1000)
{
  _events = EPOLLIN;
  _sofd = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
  if (_sofd < 0) {
    throw VULNYEX("%s", "Could not create AF_INET, SOCK_RAW, IPPROTO_ICMP");
  }

  {
    const int ok = 1;
    if (setsockopt(_sofd, IPPROTO_IP, IP_HDRINCL, &ok, sizeof ok) < 0) {
      throw VULNYEX("%s", "Could not set IP_HDRINCL on socket");
    }
  }
}

icmpHandler::~icmpHandler() {}

uint_fast8_t
icmpHandler::pingHost(size_t count)
{
  std::unique_lock<std::mutex> eg(_egressLock, std::defer_lock);
  std::unique_lock<std::mutex> ig(_ingressLock, std::defer_lock);
  std::lock(eg, ig);

  struct iphdr* const ipHdr = (struct iphdr*)_outBuffer.get();
  struct sockaddr_in soin;
  uint8_t* const buffer = _outBuffer.get();
  static auto&& rnd = net_utils::getRandomGenerator();

  if (net_utils::prepareIpHdr((uint8_t*)ipHdr, _outBuffsize) != EXIT_SUCCESS) {
    ERROR("%s", "Could not init ipHdr");
    return EXIT_FAILURE;
  }

  ipHdr->saddr = _ipSrc.s_addr;
  ipHdr->daddr = _ipDst.s_addr;
  ipHdr->protocol = IPPROTO_ICMP;

  struct icmphdr* const icmpHdr =
    (struct icmphdr*)(((uint8_t*)ipHdr) + ipHdr->ihl * 4);

  memset(&soin, 0x00, sizeof soin);
  soin.sin_family = AF_INET;
  soin.sin_addr = _ipDst;

  icmpHdr->type = ICMP_ECHO;
  icmpHdr->code = 0;

  for (size_t i = sizeof(struct icmphdr) + ipHdr->ihl * 4; i < _outBuffsize;
       i++) {
    buffer[i] = rnd();
  }

  icmpHdr->checksum =
    net_utils::checksum((uint16_t*)icmpHdr, _outBuffsize - sizeof *ipHdr);
  for (size_t i = 0; i < count; i++) {
    if (sendto(_sofd,
               buffer,
               _outBuffsize,
               0,
               (const struct sockaddr*)&soin,
               sizeof soin) <= 0) {
      ERROR("%s", "Could not send ping");
      return EXIT_FAILURE;
    }
  }

  return EXIT_SUCCESS;
}

uint_fast32_t
icmpHandler::process(int sofd, uint_fast32_t events)
{
  std::unique_lock<std::mutex> eg(_egressLock, std::defer_lock);
  std::unique_lock<std::mutex> ig(_ingressLock, std::defer_lock);
  if (std::try_lock(eg, ig) != -1) {
    DBG("%s", "Already processing");
    return EXIT_SUCCESS;
  }

  if (events & EPOLLIN) {
    ssize_t recvLen{ 0 };
    uint8_t* const buffer{ _inBuffer.get() };
    while ((recvLen = recv(sofd, buffer, _inBuffsize, 0)) > 0) {
      this->_processIcmp(buffer, recvLen);
    }
    if (recvLen == -1 && (errno == EAGAIN || errno == EWOULDBLOCK)) {
      DBG("%s", "Would have blocked");
      return EXIT_SUCCESS;
    } else {
      ERROR("%s", "recv");
      return EXIT_FAILURE;
    }
  } else {
    ERROR("Wtf events are these %u", (unsigned)events);
    return EXIT_FAILURE;
  }
}

uint_fast8_t
icmpHandler::_getDstPortFromIcmp(uint8_t* buffer,
                                 ssize_t len,
                                 uint_fast32_t* dport)
{
  if (!(buffer && dport)) {
    ERROR("Args are nullptr buffer(%p) dport(%p)", (void*)buffer, (void*)dport);
    return EXIT_FAILURE;
  }

  if (len < (ssize_t)sizeof(struct iphdr)) {
    ERROR("Too small %zi < %zi", len, sizeof(iphdr));
    return EXIT_FAILURE;
  }

  uint_fast8_t ret = EXIT_SUCCESS;
  struct iphdr* const ipHdr = (struct iphdr*)buffer;

  len -= sizeof(struct iphdr);
  if (ipHdr->protocol == IPPROTO_TCP) {
    if (len < (ssize_t)sizeof(struct tcphdr)) {
      ERROR("Not big enough for TCP %zi < %zi", len, sizeof(struct tcphdr));
      return EXIT_FAILURE;
    } else {
      struct tcphdr* tcp = (struct tcphdr*)((uint8_t*)ipHdr + ipHdr->ihl * 4);
      *dport = ntohs(tcp->dest);
    }
  } else if (ipHdr->protocol == IPPROTO_UDP) {
    if (len < (ssize_t)sizeof(struct udphdr)) {
      ERROR("Not big enough for UDP %zi < %zi", len, sizeof(struct udphdr));
      return EXIT_FAILURE;
    } else {
      struct udphdr* udp = (struct udphdr*)((uint8_t*)ipHdr + ipHdr->ihl * 4);
      *dport = ntohs(udp->dest);
    }
  } else {
    ERROR("Unknown protocol %d", (int)ipHdr->protocol);
    ret = EXIT_FAILURE;
  }

  *dport |= (ipHdr->protocol << 16);
  return ret;
}

uint_fast8_t
icmpHandler::_processIcmp(uint8_t* buffer, ssize_t len)
{
  if (!buffer) {
    ERROR("%s", "Buffer is nullptr");
    return EXIT_FAILURE;
  }

  struct iphdr* ip{ nullptr };
  struct icmphdr* icmpHdr{ nullptr };
  uint_fast32_t ipSrc{ 0 };
  if (len < (ssize_t)sizeof(struct iphdr)) {
    ERROR(
      "Packet is too small to fit IPv4 %zi < %zi", len, sizeof(struct iphdr));
    return EXIT_FAILURE;
  }

  ip = (struct iphdr*)buffer;
  ipSrc = ntohl(ip->saddr);

  len -= sizeof(struct iphdr);
  if (len < (ssize_t)sizeof(struct icmphdr)) {
    ERROR("Not big enough for icmp %zi < %zi", len, sizeof(struct icmphdr));
    return EXIT_FAILURE;
  }
  icmpHdr = (struct icmphdr*)((uint8_t*)ip + ip->ihl * 4);
  // NOTE We have an ECHO REPLY
  if (icmpHdr->type == ICMP_ECHOREPLY) {
    _pingCount[ipSrc]++;
    DBG("Added ip reply from %u.%u.%u.%u", IP_FORMAT(ipSrc));
  }

  // NOTE We have an ICMP PORT UNREACH
  // We need to extract the ports
  if (icmpHdr->type == ICMP_DEST_UNREACH &&
      icmpHdr->code == ICMP_PORT_UNREACH) {
    len -= sizeof(struct icmphdr);
    uint_fast32_t port{ 0 };
    if (_getDstPortFromIcmp((uint8_t*)(icmpHdr + 1), len, &port) ==
        EXIT_SUCCESS) {
      _portUrch[ipSrc].insert(port);
      DBG("Added unreach for %u.%u.%u.%u port %u %u",
          IP_FORMAT(ipSrc),
          (unsigned)(port),
          (unsigned)(port >> 16));
    } else {
      ERROR("%s", "Could not get dst port from icmp");
      return EXIT_FAILURE;
    }
  }
  return EXIT_SUCCESS;
}

const std::map<uint_fast32_t, uint_fast32_t>&
icmpHandler::getPingCount()
{
  return _pingCount;
}

void
icmpHandler::printPingCount()
{
  std::lock_guard<std::mutex> lg(_egressLock);
  const std::string banner = "ICMP ECHO Statistics";
  net_utils::printBanner(banner);
  for (const auto& it : _pingCount) {
    printf("%u.%u.%u.%u - %u\n", IP_FORMAT(it.first), unsigned(it.second));
  }
}

void
icmpHandler::printPortUnreachable()
{
  std::lock_guard<std::mutex> lg(_egressLock);
  const std::string banner = "ICMP Port Unreachable";
  net_utils::printBanner(banner);
  for (const auto& it : _portUrch) {
    printf("%u.%u.%u.%u:\n", IP_FORMAT(it.first));
    for (const auto& vals : it.second) {
      const uint8_t proto = vals >> 16;
      printf("\t%-5u - ", unsigned(vals & 0xffff));
      if (proto == IPPROTO_TCP) {
        printf("TCP\n");
      } else if (proto == IPPROTO_UDP) {
        printf("UDP\n");
      } else {
        printf("%u\n", unsigned(proto));
      }
    }
  }
}

const std::map<uint_fast32_t, std::set<uint_fast32_t>>&
icmpHandler::getPortUnreach()
{
  return _portUrch;
}

bool
icmpHandler::isUnreachable(uint_fast32_t ipSrc,
                           uint_fast16_t portSrc,
                           uint8_t type)
{
  std::lock_guard<std::mutex> lg(_egressLock);
  uint_fast8_t ret = false;
  const auto ports = _portUrch.find(ipSrc);
  if (ports != _portUrch.end()) {
    uint_fast32_t port = portSrc | (type << 16);
    if (ports->second.find(port) != ports->second.end()) {
      ret = true;
      DBG("Found %u.%u.%u.%u - %u", IP_FORMAT(ipSrc), (unsigned)(portSrc));
    }
  }
  return ret;
}

udpV4Knocker::udpV4Knocker(int sofd)
  : genericV4Handler(1000, sizeof(struct udphdr) + 128)
  , _pseudoBuffer(new uint8_t[_outBuffsize]())
{
  _sofd = sofd;
  _events = EPOLLIN;
  if (_sofd < 0) {
    // NOTE It doesn't receive stuff if we set IPPROTO_RAW
    _sofd = socket(AF_INET, SOCK_RAW, IPPROTO_UDP);
  }
  {
    int ok = 1;
    if (setsockopt(_sofd, IPPROTO_IP, IP_HDRINCL, &ok, sizeof(ok)) < 0) {
      throw VULNYEX("%s", "Could not set IP_HDRINCL on socket");
    }
  }
  if (_sofd < 0) {
    throw VULNYEX("%s",
                  "Could not create socket AF_INET, SOCK_RAW, IPPROTO_RAW");
  }
}

udpV4Knocker::~udpV4Knocker() {}

int_fast8_t
udpV4Knocker::processRawUdp(uint8_t* data, size_t len)
{
  int_fast8_t ret = EXIT_FAILURE;
  if (!data) {
    ERROR("%s", "data is nullptr");
    return EXIT_FAILURE;
  }
  struct iphdr* ip{ nullptr };
  struct udphdr* udp{ nullptr };
  if (len < sizeof(struct iphdr)) {
    ERROR("Too short for iphdr %zi < %zi", len, sizeof(struct iphdr));
    return ret;
  }
  ip = (struct iphdr*)data;
  if (len < sizeof(struct tcphdr) + ip->ihl * 4) {
    ERROR("Too short for iphdr + options +tcp %zi < %zi",
          len,
          sizeof(struct tcphdr) + ip->ihl * 4);
    return ret;
  }
  data += ip->ihl * 4;
  udp = (struct udphdr*)data;
  uint_fast64_t key = ntohl(ip->saddr) | (uint_fast64_t)ntohs(udp->source)
                                           << 32;
  uint_fast64_t val = ntohl(ip->daddr) | (uint_fast64_t)ntohs(udp->dest) << 32;
  if (_portResp.find(key) == _portResp.end()) {
    DBG("Not the combination we're seeking: %u - %u.%u.%u.%u",
        (unsigned)htons(udp->source),
        IP_FORMAT_NO(ip->saddr));
  } else {
    if (_portResp[key] == std::numeric_limits<uint_fast64_t>::max()) {
      _portResp[key] = val;
      DBG("Adding: %u - %u.%u.%u.%u",
          (unsigned)ntohs(udp->source),
          IP_FORMAT_NO(ip->saddr));
      ret = EXIT_SUCCESS;
    } else {
      DBG("Already processed: %u - %u.%u.%u.%u",
          (unsigned)ntohs(udp->source),
          IP_FORMAT_NO(ip->saddr));
    }
  }
  return ret;
}

uint_fast32_t
udpV4Knocker::process(int sofd, uint_fast32_t events)
{
  std::unique_lock<std::mutex> eg(_egressLock, std::defer_lock);
  std::unique_lock<std::mutex> ig(_ingressLock, std::defer_lock);
  if (std::try_lock(eg, ig) != -1) {
    DBG("%s", "Already processing");
    return EXIT_SUCCESS;
  }
  if (events & EPOLLIN) {
    ssize_t recvLen{ -1 };
    uint8_t* const buffer{ _inBuffer.get() };
    while ((recvLen = read(sofd, buffer, _inBuffsize)) > 0) {
      processRawUdp(buffer, recvLen);
    }
    if (recvLen < 0 && (errno == EAGAIN || errno == EWOULDBLOCK)) {
      DBG("Would have blocked fd(%d)", sofd);
      return EXIT_SUCCESS;
    } else {
      ERROR("Something is wrong with fd(%d) on read", sofd);
      return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
  } else {
    ERROR("Received strange events %u", (unsigned)events);
    return EXIT_FAILURE;
  }
}

int_fast8_t
udpV4Knocker::_processChanges()
{
  genericV4Handler::_processChanges();
  uint8_t* const buffer = _outBuffer.get();
  struct iphdr* const ip = (struct iphdr*)buffer;
  struct udphdr* const udp = (struct udphdr*)(buffer + ip->ihl * 4);

  // Anything IP Related
  if (_modified & (mDstIp | mSrcIp)) {
    ip->protocol = IPPROTO_UDP;
  }

  // Modified sport
  if (_modified & mSrcPrt) {
    udp->source = _sport;
  }

  // Modified dport
  if (_modified & mDstPrt) {
    udp->dest = _dport;
  }

  // Modified anything UDP related
  if (_modified) {
    static auto&& rnd = net_utils::getRandomGenerator();

    for (size_t i = sizeof *ip + sizeof *udp; i < _outBuffsize; i++) {
      buffer[i] = rnd();
    }
    udp->len = htons(_outBuffsize - sizeof *ip);
    udp->check =
      net_utils::udpChecksum((uint8_t*)ip, _outBuffsize, _pseudoBuffer.get());
  }
  _modified = 0;
  return EXIT_SUCCESS;
}

uint_fast8_t
udpV4Knocker::sendUdpKnock()
{
  std::lock_guard<std::mutex> lg(_egressLock);
  ssize_t recvLen{ -1 };
  if (_modified && _processChanges() == EXIT_FAILURE) {
    ERROR("%s", "Could not process the changes");
    return EXIT_FAILURE;
  }
  if ((recvLen = sendto(_sofd,
                        _outBuffer.get(),
                        _outBuffsize,
                        0,
                        (const struct sockaddr*)&_sockin,
                        sizeof _sockin)) > 0) {
    const uint_fast64_t val =
      ntohl(_ipDst.s_addr) | (uint_fast64_t)ntohs(_dport) << 32;
    if (_portResp.find(val) == _portResp.end()) {
      _portResp[val] = std::numeric_limits<uint_fast64_t>::max();
      DBG("Adding %u.%u.%u.%u - %u",
          IP_FORMAT((val & 0xFFFFFFFF)),
          (unsigned)(val >> 32));
    } else {
      DBG("%lu already present", (unsigned long)val);
    }
  }
  if (recvLen < 0) {
    if (errno == EAGAIN || errno == EWOULDBLOCK) {
      DBG("Would have blocked fd(%d)", _sofd);
      return EXIT_FAILURE + 1;
    } else {
      ERROR("Other error for fd(%d)", _sofd);
      return EXIT_FAILURE;
    }
  }
  return EXIT_SUCCESS;
}

void
udpV4Knocker::printUdpResponses()
{
  std::lock_guard<std::mutex> lg(_egressLock);
  const std::string banner = "UDP Knock Responses";
  net_utils::printBanner(banner);
  for (const auto& it : this->_portResp) {
    if (it.second != std::numeric_limits<uint_fast64_t>::max()) {
      printf("%u.%u.%u.%u - %-5u => %5u\n",
             IP_FORMAT(it.first),
             (unsigned)((it.first >> 32) & 0xFFFF),
             (unsigned)((it.second >> 32) & 0xFFFF));
    }
  }
}

void
udpV4Knocker::printUdpUnreachable(icmpHandler& icmp)
{
  std::lock_guard<std::mutex> lg(_egressLock);
  const std::string banner = "UDP Port Unreachable";
  net_utils::printBanner(banner);
  for (const auto& it : this->_portResp) {
    uint_fast32_t ip = it.first & 0xFFFFFFFF;
    uint_fast16_t port = (it.first >> 32) & 0xffff;
    if (icmp.isUnreachable(ip, port, IPPROTO_UDP) == true) {
      printf("IP %u.%u.%u.%u - %u UDP is unreachable\n",
             IP_FORMAT(ip),
             (unsigned)port);
    }
  }
}

tcpV4Knocker::tcpV4Knocker(int sofd)
  : genericV4Handler(1000, sizeof(tcphdr) + 128)
  , _pseudoBuffer(new uint8_t[_outBuffsize]())
{
  _events = EPOLLIN;
  _sofd = sofd;
  if (_sofd < 0) {
    _sofd = socket(AF_INET, SOCK_RAW | SOCK_NONBLOCK, IPPROTO_TCP);
  }
  if (_sofd < 0) {
    throw VULNYEX("%s", "Could not create RAW TCP SOCKET");
  }
  {
    int ok = 1;
    if (setsockopt(_sofd, IPPROTO_IP, IP_HDRINCL, &ok, sizeof ok) == -1) {
      throw VULNYEX("%s", "Could not set IP_HDRINCL");
    }
  }
  memset(_inBuffer.get(), 0x00, _inBuffsize);
}

tcpV4Knocker::~tcpV4Knocker() {}

int_fast8_t
tcpV4Knocker::_processChanges()
{
  genericV4Handler::_processChanges();
  auto const buffer = _outBuffer.get();
  auto const ip = (struct iphdr*)buffer;
  auto const tcp = (struct tcphdr*)(buffer + ip->ihl * 4);

  // Anything IP Related
  if (_modified & (mDstIp | mSrcIp)) {
    ip->protocol = IPPROTO_TCP;
  }

  // Modified dport
  if (_modified & mDstPrt) {
    tcp->dest = _dport;
  }

  // Modified sport
  if (_modified & mSrcPrt) {
    tcp->source = _sport;
  }

  // Modified anything TCP related
  if (_modified) {
    static auto&& rnd = net_utils::getRandomGenerator();

    for (size_t i = sizeof *ip + sizeof *tcp; i < _outBuffsize; i++) {
      buffer[i] = rnd();
    }

    tcp->doff = (sizeof *tcp) / (sizeof(uint32_t));
    tcp->th_seq = rnd();
    tcp->th_ack = 0;
    tcp->th_flags = TH_SYN;
    tcp->check =
      net_utils::tcpChecksum((uint8_t*)ip, _outBuffsize, _pseudoBuffer.get());
  }
  _modified = 0;
  return EXIT_SUCCESS;
}

int_fast8_t
tcpV4Knocker::sendSynKnock()
{
  std::unique_lock<std::mutex> eg(_egressLock, std::defer_lock);
  std::unique_lock<std::mutex> ig(_ingressLock, std::defer_lock);
  std::lock(eg, ig);
  ssize_t recvLen{ -1 };
  if (_modified && _processChanges() == EXIT_FAILURE) {
    ERROR("%s", "Error at packet preparation");
    return EXIT_FAILURE;
  }
  if ((recvLen = sendto(_sofd,
                        _outBuffer.get(),
                        _outBuffsize,
                        0,
                        (const struct sockaddr*)&_sockin,
                        sizeof _sockin)) > 0) {
    const uint_fast64_t val =
      ntohl(_ipDst.s_addr) | (uint_fast64_t)(ntohs(_dport)) << 32;

    if (_portResp.find(val) == _portResp.end()) {
      _portResp[val] = std::numeric_limits<uint_fast32_t>::max();
      DBG("Adding %u.%u.%u.%u %u",
          IP_FORMAT_NO(_ipDst.s_addr),
          unsigned(ntohs(_dport)));
    } else {
      DBG("%lu already present", (unsigned long)val);
    }
  }
  if (recvLen < 0) {
    if (errno == EAGAIN || errno == EWOULDBLOCK) {
      DBG("Would have blocked fd(%d)", _sofd);
      return EXIT_FAILURE + 1;
    } else {
      ERROR("Other error for fd(%d)", _sofd);
      return EXIT_FAILURE;
    }
  }
  return EXIT_SUCCESS;
}

int_fast8_t
tcpV4Knocker::_processRawTcp(uint8_t* data, size_t len)
{
  int_fast8_t ret = EXIT_FAILURE;
  if (len < sizeof(struct iphdr) + sizeof(struct tcphdr)) {
    ERROR("Buffer is too small %zu < %zu",
          len,
          sizeof(struct iphdr) + sizeof(struct tcphdr));
    return ret;
  }
  auto const ip = (struct iphdr*)data;
  auto const tcp = (struct tcphdr*)(data + ip->ihl * 4);
  const auto key = ntohl(ip->saddr) | (uint_fast64_t)ntohs(tcp->th_sport) << 32;
  if (_portResp.find(key) == _portResp.end()) {
    DBG("Not the combination we're looking for %u.%u.%u.%u - %u",
        IP_FORMAT_NO(ip->saddr),
        unsigned(ntohs(tcp->th_sport)));
  } else {
    if (_portResp[key] == std::numeric_limits<uint_fast32_t>::max()) {
      const auto val = ntohs(tcp->th_dport) | (uint_fast32_t)tcp->th_flags
                                                << 16;
      _portResp[key] = val;
      DBG("Found matching response %u.%u.%u.%u - %u",
          IP_FORMAT_NO(ip->saddr),
          unsigned(ntohs(tcp->th_sport)));
      ret = EXIT_SUCCESS;
    } else {
      DBG("Already present %u.%u.%u.%u - %u",
          IP_FORMAT_NO(ip->saddr),
          unsigned(ntohs(tcp->th_sport)));
    }
  }
  return ret;
}

uint_fast32_t
tcpV4Knocker::process(int sofd, uint_fast32_t events)
{
  std::unique_lock<std::mutex> eg(_egressLock, std::defer_lock);
  std::unique_lock<std::mutex> ig(_ingressLock, std::defer_lock);
  if (std::try_lock(eg, ig) == false) {
    DBG("%s", "Already processing something else...");
    return EXIT_FAILURE;
  }
  if (events & EPOLLIN) {
    uint8_t* const buffer{ _inBuffer.get() };
    ssize_t recvLen{ -1 };
    while ((recvLen = read(sofd, buffer, _inBuffsize)) > 0) {
      _processRawTcp(buffer, recvLen);
    }
    if (recvLen < 0) {
      if (errno == EWOULDBLOCK || errno == EAGAIN) {
        DBG("%s", "Would have blocked");
        return EXIT_SUCCESS;
      } else {
        ERROR("%s", "read has failed");
        return EXIT_FAILURE;
      }
    } else {
      ERROR("%s", "Received zero bytes message. NOT OK!");
      return EXIT_FAILURE;
    }
  } else {
    ERROR("Received some strange events %u", (unsigned)events);
    return EXIT_FAILURE;
  }
}

void
tcpV4Knocker::printTcpResponses()
{
  std::lock_guard<std::mutex> lg(_egressLock);
  const std::string banner = "TCP Knock Responses";
  net_utils::printBanner(banner);
  for (const auto it : _portResp) {
    const auto key = it.first;
    const auto val = it.second;
    printf(
      "%u.%u.%u.%u - %-5u", IP_FORMAT(key), unsigned((key >> 32) & 0xffff));
    if (val == std::numeric_limits<decltype(val)>::max()) {
      printf(": No response seen\n");
    } else {
      printf("=> %5u : Flags : ", unsigned(val & 0xffff));
      if (((val >> 16) ^ (TH_SYN | TH_ACK)) == 0) {
        printf("SYN|ACK\n");
      } else if ((val >> 16) & TH_RST) {
        printf("RST\n");
      } else {
        printf("%u\n", unsigned(val >> 16));
      }
    }
  }
}

void
tcpV4Knocker::printTcpUnreach(class icmpHandler& icmp)
{
  std::lock_guard<std::mutex> lg(_egressLock);
  const std::string banner = "TCP Port Unreachable";
  net_utils::printBanner(banner);
  for (const auto it : this->_portResp) {
    const auto ip = it.first & 0xFFFFFFFF;
    const auto port = (it.first >> 32) & 0xffff;
    if (icmp.isUnreachable(ip, port, IPPROTO_TCP) == true) {
      printf("%u.%u.%u.%u - %-5u : TCP is unreachable\n",
             IP_FORMAT(ip),
             (unsigned)port);
    }
  }
}
