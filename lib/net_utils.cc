#include "net_utils.hpp"
#include <arpa/inet.h>
#include <cstring>
#include <fcntl.h>
#include <ifaddrs.h>
#include <linux/ethtool.h>
#include <linux/if.h>
#include <linux/sockios.h>
#include <linux/wireless.h>
#include <memory>
#include <mutex>
#include <net/ethernet.h>
#include <netinet/ether.h>
#include <netinet/icmp6.h>
#include <netinet/if_ether.h>
#include <netinet/if_tr.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/ip6.h>
#include <netinet/ip_icmp.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <unistd.h>

static std::mutex _netUtilsLock;       /*!< Lock used in sensitive ares */
static const uint8_t bannerWidth = 40; /*!< Variable used to change banner width
                                          for the packet dumping functions*/

#ifdef MSN_RND_FUNC
std::mt19937_64
net_utils::getRandomGenerator()
{
  std::random_device rnd;
  std::mt19937_64 msn(rnd());
  return msn;
}
#else  // MSN_RND_FUNC
std::function<long int(void)>
net_utils::getRandomGenerator()
{
  static bool init = false;
  if (init == false) {
    init = true;
    srandom(time(nullptr));
  }
  return random; /* We return the 'random(3)' function */
}
#endif // MSN_RND_FUNC

int
net_utils::getHwAddr(const char ifname[IFNAMSIZ], uint8_t addr[IFHWADDRLEN])
{
  if (!(ifname && addr)) {
    fprintf(stderr, "Args are null\n");
    return EXIT_FAILURE;
  }

  std::lock_guard<std::mutex> lg(_netUtilsLock);
  struct ifreq intReq;
  int ret = EXIT_FAILURE;
  const int sofd = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
  if (sofd == -1) {
    ERROR("%s", "socket");
    goto fail;
  }
  memset(&intReq, 0x00, sizeof intReq);
  strncpy(intReq.ifr_name, (const char*)ifname, IFNAMSIZ - 1);
  if (ioctl(sofd, SIOCGIFHWADDR, &intReq) == -1) {
    ERROR("%s ifname(%s)", "ioctl SIOCGIFHWADDR", ifname);
    goto fail;
  }

  memcpy(addr, intReq.ifr_hwaddr.sa_data, IFHWADDRLEN);
  ret = EXIT_SUCCESS;

fail:
  if (sofd != 1) {
    close(sofd);
  }
  return ret;
}

int
net_utils::setHwAddr(const char ifname[IFNAMSIZ],
                     uint8_t addr[IFHWADDRLEN],
                     uint8_t randomize)
{
  if (!(ifname && addr)) {
    fprintf(stderr, "Args are null\n");
    return EXIT_FAILURE;
  }
  std::lock_guard<std::mutex> lg(_netUtilsLock);
  int ret = EXIT_FAILURE;
  struct ifreq intReq;
  const int sofd = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
  if (sofd == -1) {
    ERROR("%s", "socket");
    goto fail;
  }
  memset(&intReq, 0x00, sizeof intReq);
  strncpy(intReq.ifr_name, (const char*)ifname, IFNAMSIZ - 1);
  if (ioctl(sofd, SIOCGIFHWADDR, &intReq) == -1) {
    ERROR("%s ifname(%s)", "ioctl SIOCGIFHWADDR", ifname);
    goto fail;
  }
  if (randomize) {
    auto&& rnd = net_utils::getRandomGenerator();
    for (size_t i = 0; i < IFHWADDRLEN; i++) {
      addr[i] = rnd();
    }
  }
  addr[0] &= 0xFC; // Unset multicast and link-local bits
  memcpy(intReq.ifr_ifru.ifru_hwaddr.sa_data, (const char*)addr, IFHWADDRLEN);
  if (ioctl(sofd, SIOCSIFHWADDR, &intReq) == -1) {
    ERROR("%s ifname(%s)", "ioctl SIOCSIFHWADDR", ifname);
    goto fail;
  }
  ret = EXIT_SUCCESS;

fail:
  if (sofd != -1) {
    close(sofd);
  }
  return ret;
}

int
net_utils::resetHwAddr(const char ifname[IFNAMSIZ])
{
  if (!(ifname)) {
    fprintf(stderr, "Args are null\n");
    return EXIT_FAILURE;
  }
  std::lock_guard<std::mutex> lg(_netUtilsLock);
  int ret = EXIT_FAILURE;
  struct ifreq intReq;
  struct ethtool_perm_addr* epa = nullptr;
  const int sofd = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
  if (sofd == -1) {
    ERROR("%s", "socket");
    goto fail;
  }
  memset(&intReq, 0x00, sizeof intReq);
  strncpy(intReq.ifr_name, (const char*)ifname, IFNAMSIZ - 1);
  epa = (struct ethtool_perm_addr*)calloc(sizeof *epa + IFHWADDRLEN, 1);
  if (epa == nullptr) {
    ERROR("%s", "calloc");
    goto fail;
  }
  epa->cmd = ETHTOOL_GPERMADDR;
  epa->size = IFHWADDRLEN;
  intReq.ifr_data = (char*)epa;
  if (ioctl(sofd, SIOCETHTOOL, &intReq) == -1) {
    ERROR("%s", "ioctl SIOCETHTOOL");
    goto fail;
  }
  if (ioctl(sofd, SIOCGIFHWADDR, &intReq) == -1) {
    ERROR("%s", "ioctl SIOCGIFHWADDR");
    goto fail;
  }
  memcpy(intReq.ifr_ifru.ifru_hwaddr.sa_data, epa->data, IFHWADDRLEN);
  if (ioctl(sofd, SIOCSIFHWADDR, &intReq) == -1) {
    ERROR("%s", "ioctl SIOCSIFHWADDR");
    goto fail;
  }
  ret = EXIT_SUCCESS;

fail:
  if (sofd != -1) {
    close(sofd);
  }
  if (epa) {
    free(epa);
  }
  return ret;
}

long
net_utils::checksum(uint16_t* addr, ssize_t count)
{
  long sum = 0;
  while (count > 1) {
    /*  This is the inner loop */
    sum += *addr++;
    count -= 2;
  }

  /*  Add left-over byte, if any */
  if (count > 0) {
    sum += *(uint8_t*)addr;
  }

  /*  Fold 32-bit sum to 16 bits */
  while (sum >> 16) {
    sum = (sum & 0xffff) + (sum >> 16);
  }
  return ~sum;
}

long
net_utils::udpChecksum(uint8_t* data, ssize_t len, uint8_t* pseudoBuffer)
{
  if (!data) {
    ERROR("%s", "Data is nulltr");
    return 0;
  }

  if (len < (ssize_t)(sizeof(struct iphdr))) {
    ERROR("Size too small for IPv4 %zi < %zi", len, sizeof(struct iphdr));
    return 0;
  }

  auto const ip = (struct iphdr*)data;

  if (len < (ssize_t)(sizeof(struct udphdr) + ip->ihl * 4)) {
    ERROR("Size too small for IPv4 + Options + UDP %zi < %zi",
          len,
          sizeof(struct udphdr) + ip->ihl * 4);
    return 0;
  }

  auto const udp = (struct udphdr*)(data + ip->ihl * 4);
  const ssize_t pseudoLen =
    len - (ip->ihl * 4) + sizeof(struct transportPseudoHeader);
  bool allocked = false;

  if (!pseudoBuffer) {
    pseudoBuffer = new uint8_t[pseudoLen]();
    allocked = true;
  }

  if (!pseudoBuffer) {
    ERROR("%s", "Could not alloc pseudoBuffer");
    return 0;
  }

  auto const psh = (struct transportPseudoHeader*)pseudoBuffer;
  psh->destIp = ip->daddr;
  psh->sourceIp = ip->saddr;
  psh->zero = 0;
  psh->protocol = IPPROTO_UDP;
  psh->len = udp->len;
  udp->check = 0;

  memcpy(pseudoBuffer + sizeof *psh, udp, pseudoLen - sizeof *psh);
  auto chk = checksum((uint16_t*)pseudoBuffer, pseudoLen);

  if (allocked) {
    delete[] pseudoBuffer;
  }

  return chk;
}

long
net_utils::tcpChecksum(uint8_t* data, ssize_t len, uint8_t* pseudoBuffer)
{

  if (!data) {
    ERROR("%s", "Data is nulltr");
    return 0;
  }

  if (len < (ssize_t)(sizeof(struct iphdr))) {
    ERROR("Size too small for IPv4 %zi < %zi", len, sizeof(struct iphdr));
    return 0;
  }

  auto const* ip = (struct iphdr*)data;

  if (len < (ssize_t)(sizeof(struct tcphdr) + ip->ihl * 4)) {
    ERROR("Size too small for IPv4 + Options + Tcp %zi < %zi",
          len,
          sizeof(struct tcphdr) + ip->ihl * 4);
  }

  auto const tcp = (struct tcphdr*)(data + ip->ihl * 4);
  const ssize_t pseudoLen =
    len - (ip->ihl * 4) + sizeof(struct transportPseudoHeader);
  bool allocked = false;

  if (!pseudoBuffer) {
    pseudoBuffer = new uint8_t[pseudoLen]();
    allocked = true;
  }

  if (!pseudoBuffer) {
    ERROR("%s", "Could not alloc pseudoBuffer");
    return 0;
  }

  auto const psh = (struct transportPseudoHeader*)pseudoBuffer;
  psh->destIp = ip->daddr;
  psh->sourceIp = ip->saddr;
  psh->zero = 0;
  psh->protocol = IPPROTO_TCP;
  psh->len = htons(len - ip->ihl * 4);
  tcp->check = 0;

  memcpy(pseudoBuffer + sizeof *psh, tcp, pseudoLen - sizeof *psh);
  auto chk = checksum((uint16_t*)pseudoBuffer, pseudoLen);

  if (allocked) {
    delete[] pseudoBuffer;
  }

  return chk;
}

uint32_t
net_utils::printEthernet(const uint8_t* data, ssize_t len)
{
  int ret = 0;

  if (!data || len < (ssize_t)sizeof(struct ether_header)) {
    return ret;
  }

  const struct ether_header* ethhdr = (const struct ether_header*)data;
  uint16_t etherType = ntohs(ethhdr->ether_type);
  net_utils::printBanner("Ethernet", bannerWidth);
  printf("\nSrc MAC: ");
  for (size_t i = 0; i < ETHER_ADDR_LEN; i++) {
    printf("%#x ", (unsigned)ethhdr->ether_shost[i]);
  }
  printf("\nDst MAC: ");
  for (size_t i = 0; i < ETHER_ADDR_LEN; i++) {
    printf("%#x ", (unsigned)ethhdr->ether_dhost[i]);
  }

  if (etherType <= ETH_P_802_3_MIN) {
    printf("\nLength : %#x", etherType);
  } else {
    printf("\nEthertype: %#x", etherType);
  }
  printf("\n");

  len -= sizeof(struct ether_header);
  ret = (sizeof(struct ether_header) << 16);

  if (etherType < ETH_P_802_3_MIN) {
    ethhdr += 1;
    ret += printLLC((const uint8_t*)ethhdr, len);
  } else {
    ret |= etherType;
  }

  return ret;
}

uint32_t
net_utils::printLLC(const uint8_t* data, ssize_t len)
{
  uint32_t ret = 0;

  if (!data || len < (ssize_t)sizeof(struct trllc)) {
    return ret;
  }

  ret = (uint32_t)3 << 16;

  const auto* const llc = (const struct trllc*)data;
  net_utils::printBanner("LLC", bannerWidth);
  printf("\nDSAP %#x", unsigned(llc->dsap));
  printf("\nSSAP %#x", unsigned(llc->ssap));
  printf("\nLLC Control Field %#x", unsigned(llc->llc));
  printf("\nProtocol ID ");
  for (size_t i = 0; i < sizeof(llc->protid) / sizeof(llc->protid[0]); i++) {
    printf("%x", llc->protid[i]);
  }
  printf("\nEtherType %#x", unsigned(ntohs(llc->ethertype)));
  printf("\n");

  return ret;
}

uint32_t
net_utils::printArp(const uint8_t* data, ssize_t len)
{
  uint32_t ret = 0;
  if (!data || len < (ssize_t)sizeof(struct ether_arp)) {
    return ret;
  }
  const struct ether_arp* arpReq = (const struct ether_arp*)data;
  uint16_t opCode = ntohs(arpReq->ea_hdr.ar_op);
  char buffer[16] = { 0x00 };
  net_utils::printBanner("ARP", bannerWidth);
  printf("\nHardware address format: %#x", ntohs(arpReq->ea_hdr.ar_hrd));
  printf("\nProtocol address format: %#x", ntohs(arpReq->ea_hdr.ar_pro));
  printf("\nHardware address length: %#x", arpReq->ea_hdr.ar_hln);
  printf("\nProtocol address length: %#x", arpReq->ea_hdr.ar_pln);
  printf("\nARP Opcode: %#x %s",
         opCode,
         opCode == ARPOP_REPLY
           ? "ARPOP_REPLY"
           : (opCode == ARPOP_REQUEST ? "ARPOP_REQUEST" : "Other"));

  printf("\nSrc Hardware Address: ");
  for (size_t i = 0; i < ETHER_ADDR_LEN; i++) {
    printf("%#x ", arpReq->arp_sha[i]);
  }

  printf("\nDst Hardware Address: ");
  for (size_t i = 0; i < ETHER_ADDR_LEN; i++) {
    printf("%#x ", arpReq->arp_tha[i]);
  }

  if (inet_ntop(AF_INET, arpReq->arp_spa, buffer, sizeof buffer) != nullptr) {
    printf("\nSrc Protocol Address: %s", buffer);
  } else {
    fprintf(stderr, "%s:%d : inet_ntop\n", __func__, __LINE__);
    ret = 1;
  }
  if (inet_ntop(AF_INET, arpReq->arp_tpa, buffer, sizeof buffer) != nullptr) {
    printf("\nDst Protocol Address: %s", buffer);
  } else {
    fprintf(stderr, "%s:%d : inet_ntop\n", __func__, __LINE__);
    ret = 1;
  }
  // net_utils::printBanner("", bannerWidth);
  printf("\n");

  if (ret == 0) {
    ret = (sizeof(struct ether_arp)) << 16;
  }

  return ret;
}

void
net_utils::printPacket(const uint8_t* data, ssize_t len, int layer)
{
  uint32_t ret = 0;
  uint16_t next = 0;
  uint16_t consumed = 0;
  if (!data) {
    ERROR("%s", "Buffer is nullptr");
    return;
  }

  if (layer == 2) {
    ret = printEthernet(data, len);
    consumed = ret >> 16;
    next = ret;
    if (next && consumed) {
      layer++;
    } else {
      layer = 5;
    }
    len -= consumed;
    data += consumed;
  }

  if (layer == 3) {
    switch (next) {
      case 0:
      case ETHERTYPE_IP:
      case ETHERTYPE_IPV6: {
        ret = printIP(data, len);
        break;
      }
      case ETHERTYPE_ARP: {
        ret = printArp(data, len);
        break;
      }
      default: {
        printf("\n====Not implemented for ETHERTYPE:%#x====\n", next);
        ret = 0;
      }
    }
    consumed = ret >> 16;
    next = ret;
    if (next && consumed) {
      layer++;
    } else {
      layer = 5;
    }
    len -= consumed;
    data += consumed;
  }

  if (layer == 4) {
    switch (next) {
      case IPPROTO_ICMP:
        ret = printIcmpV4(data, len);
        break;
      case IPPROTO_ICMPV6:
        ret = printIcmpV6(data, len);
        break;
      case IPPROTO_TCP:
        ret = printTCP(data, len);
        break;
      case IPPROTO_UDP:
        ret = printUDP(data, len);
        break;
      default:
        printf("\n=====Protocol not implemented %#x=====\n", next);
        break;
    }
    consumed = ret >> 16;
    next = ret;
    if (next && consumed) {
      layer++;
    } else {
      layer = 5;
    }
    len -= consumed;
    data += consumed;
  }

  if (layer > 4) {
    net_utils::dumpPacket(data, len);
  }

  net_utils::printBanner("", bannerWidth);
}

uint32_t
net_utils::printIpV4(const uint8_t* data, ssize_t len)
{
  uint32_t ret = 0;
  char addr[16] = { 0x00 };
  if (!data || len < (ssize_t)sizeof(struct iphdr)) {
    return ret;
  }
  const struct iphdr* ipHdr = (const struct iphdr*)data;
  net_utils::printBanner("IPv4", bannerWidth);
  printf("\nIHL: %u", ipHdr->ihl);
  printf("\nVersion: %u", ipHdr->version);
  printf("\nType of Service: %#x", ipHdr->tos);
  printf("\nTotal Length: %u", ntohs(ipHdr->tot_len));
  printf("\nId: %u", ntohs(ipHdr->id));
  printf("\nFrag Offset: %u", ipHdr->frag_off);
  printf("\nTTL: %u", ipHdr->ttl);
  printf("\nProtocol: %#x", ipHdr->protocol);
  printf("\nChecksum: %#x", ipHdr->check);

  ret = ipHdr->protocol;

  if (inet_ntop(AF_INET, &ipHdr->saddr, addr, sizeof addr) != nullptr) {
    printf("\nSrc Address: %s", addr);
  } else {
    ret = 0;
  }

  if (inet_ntop(AF_INET, &ipHdr->daddr, addr, sizeof addr) != nullptr) {
    printf("\nDst Address: %s", addr);
  } else {
    ret = 0;
  }
  // net_utils::printBanner("", bannerWidth);
  printf("\n");

  if (ret != 0) {
    ret = ((ipHdr->ihl * 4) << 16) | ipHdr->protocol;
  }

  return ret;
}

uint32_t
net_utils::printIpV6(const uint8_t* data, ssize_t len)
{
  uint32_t ret = 0;
  if (!data || len < (ssize_t)sizeof(struct ip6_hdr)) {
    return ret;
  }
  const struct ip6_hdr* ip6 = (const struct ip6_hdr*)data;
  char buffer[40] = { 0x00 };
  uint32_t firstWord = ntohl(ip6->ip6_ctlun.ip6_un1.ip6_un1_flow);
  net_utils::printBanner("IPv6", bannerWidth);
  printf("\nVersion: %u", firstWord >> 28);
  printf("\nTraffic Class: %u", (firstWord >> 20) & 0x0ff);
  printf("\nFlow ID: %u", firstWord & 0x000fffff);
  printf("\nPayload Length: %u", ntohs(ip6->ip6_ctlun.ip6_un1.ip6_un1_plen));
  printf("\nNext Header: %u", (unsigned)ip6->ip6_ctlun.ip6_un1.ip6_un1_nxt);
  printf("\nHop Limit: %u", (unsigned)ip6->ip6_ctlun.ip6_un1.ip6_un1_hlim);

  if (inet_ntop(AF_INET6, &ip6->ip6_src, buffer, sizeof buffer) != nullptr) {
    printf("\nSrc Address: %s", buffer);
  } else {
    ERROR("%s:%d inet_ntop\n", __func__, __LINE__);
  }

  if (inet_ntop(AF_INET6, &ip6->ip6_dst, buffer, sizeof buffer) != nullptr) {
    printf("\nDst Address: %s", buffer);
  } else {
    ERROR("%s:%d inet_ntop\n", __func__, __LINE__);
  }
  // net_utils::printBanner("", bannerWidth);
  printf("\n");

  ret = sizeof(struct ip6_hdr) << 16 | ip6->ip6_ctlun.ip6_un1.ip6_un1_nxt;
  return ret;
}

uint32_t
net_utils::printIP(const uint8_t* data, ssize_t len)
{
  if (!data || len < (ssize_t)sizeof(struct iphdr)) {
    return 0;
  }
  const struct iphdr* ipHdr = (const struct iphdr*)data;
  if (ipHdr->version == 4) {
    return printIpV4(data, len);
  } else {
    return printIpV6(data, len);
  }
}

uint32_t
net_utils::printIcmpV4(const uint8_t* data, ssize_t len)
{
  int ret = 0;
  if (!data || len < (ssize_t)sizeof(struct icmphdr)) {
    return 0;
  }
  const struct icmphdr* icmp = (const struct icmphdr*)data;
  net_utils::printBanner("ICMPv4", bannerWidth);
  printf("\nType: %u", icmp->type);
  printf("\nCode: %u", icmp->code);
  printf("\nChecksum: %#x", icmp->checksum);
  // net_utils::printBanner("", bannerWidth);
  printf("\n");
  ret = (sizeof(struct icmphdr)) << 16;
  return ret;
}

uint32_t
net_utils::printIcmpV6(const uint8_t* data, ssize_t len)
{
  int ret = 0;
  if (!data || len < (ssize_t)sizeof(struct icmp6_hdr)) {
    return ret;
  }
  const struct icmp6_hdr* icmp6 = (const struct icmp6_hdr*)data;
  net_utils::printBanner("ICMPv6", bannerWidth);
  printf("\nType: %u", icmp6->icmp6_type);
  printf("\nCode: %u", icmp6->icmp6_code);
  printf("\nChecksum: %#x", icmp6->icmp6_cksum);
  // net_utils::printBanner("", bannerWidth);
  printf("\n");
  ret = sizeof(struct icmp6_hdr) << 16;
  return ret;
}

uint32_t
net_utils::printUDP(const uint8_t* data, ssize_t len)
{
  int ret = 0;
  if (!data || len < (ssize_t)sizeof(struct udphdr)) {
    return ret;
  }
  const struct udphdr* udph = (const struct udphdr*)data;
  net_utils::printBanner("UDP", bannerWidth);
  printf("\nSrc Port: %u", ntohs(udph->source));
  printf("\nDst Port: %u", ntohs(udph->dest));
  printf("\nLength: %u", ntohs(udph->len));
  printf("\nChecksum: %#x", udph->check);
  // net_utils::printBanner("", bannerWidth);
  printf("\n");
  ret = ((sizeof(struct udphdr)) << 16);
  return ret;
}

uint32_t
net_utils::printTCP(const uint8_t* data, ssize_t len)
{
  uint32_t ret = 0;
  if (!data || len < (ssize_t)sizeof(struct tcphdr)) {
    return ret;
  }
  const struct tcphdr* tcph = (const struct tcphdr*)data;
  net_utils::printBanner("TCP", bannerWidth);
  printf("\nSrc port: %u", ntohs(tcph->source));
  printf("\nDst port: %u", ntohs(tcph->dest));
  printf("\nSequence #: %u", ntohl(tcph->seq));
  printf("\nAcknowledge #: %u", ntohl(tcph->ack_seq));
  printf("\nFlags: ");
  if (tcph->syn) {
    printf("SYN ");
  }
  if (tcph->ack) {
    printf("ACK ");
  }
  if (tcph->fin) {
    printf("FIN ");
  }
  if (tcph->rst) {
    printf("RST ");
  }
  if (tcph->psh) {
    printf("PSH ");
  }
  if (tcph->urg) {
    printf("URG ");
  }
  printf("\nData Offset: %u * 4", tcph->doff);
  printf("\nWindow: %u", ntohs(tcph->window));
  printf("\nUrgent Pointer: %u", ntohs(tcph->urg_ptr));
  printf("\nChecksum: %#x", tcph->check);
  // net_utils::printBanner("", bannerWidth);
  printf("\n");
  ret = ((tcph->doff * 4) << 16);
  return ret;
}

int_fast8_t
net_utils::dumpPacket(const uint8_t* data, ssize_t len)
{
  if (!data || len <= 0) {
    return EXIT_FAILURE;
  }
  uint8_t byte = 0x00;
  ssize_t i, j;
  net_utils::printBanner("RAW", (len < 16) ? (50 + 2 * len) : 82);
  printf("\n");
  for (i = 0; i < len; i++) {
    byte = data[i];
    printf("%02x ", byte);
    if ((i % 16 == 15) || (i == len - 1)) {
      for (j = 0; j < 15 - (i % 16); j++, printf("%3c", ' '))
        ;
      printf("| ");
      for (j = (i - (i % 16)); j <= i; j++) {
        byte = data[j];
        if (isprint(byte)) {
          printf("%c ", byte);
        } else {
          printf(". ");
        }
      }
      printf("\n");
    }
  }
  // net_utils::printBanner("", bannerWidth);
  printf("\n");
  return EXIT_SUCCESS;
}

uint32_t
net_utils::prepareArpRequest(uint8_t* data, bool linkLayer, ssize_t len)
{
  if (!data) {
    DBG("%s", "Data is nullptr");
    return EXIT_FAILURE;
  }

  if (linkLayer) {
    if (len < (ssize_t)sizeof(struct ether_header)) {
      DBG("%s", "Too short for linkLayer");
      return EXIT_FAILURE;
    }
    struct ether_header* ethhdr = (struct ether_header*)data;
    memset(ethhdr->ether_dhost, 0xff, ETHER_ADDR_LEN);
    ethhdr->ether_type = htons(ETH_P_ARP);

    data += sizeof(struct ether_header);
    len -= sizeof(struct ether_header);
  }

  if (len < (ssize_t)sizeof(ether_arp)) {
    DBG("%s", "Too short for arp");
    return EXIT_FAILURE;
  }

  struct ether_arp* arp = (struct ether_arp*)data;
  arp->ea_hdr.ar_hln = ETHER_ADDR_LEN;
  arp->ea_hdr.ar_pro = htons(ETH_P_IP);
  arp->ea_hdr.ar_pln = IPVERSION;
  arp->ea_hdr.ar_hrd = htons(ARPHRD_ETHER);
  arp->ea_hdr.ar_op = htons(ARPOP_REQUEST);

  memset(arp->arp_tha, 0xff, ETHER_ADDR_LEN);

  return EXIT_SUCCESS;
}

uint32_t
net_utils::prepareIpHdr(uint8_t* data, ssize_t len)
{
  if (data == nullptr) {
    ERROR("%s", "Data is null");
    return EXIT_FAILURE;
  }
  if (len < ((ssize_t)sizeof(struct iphdr))) {
    ERROR("Too small for ip hdr %zi < %zi", len, sizeof(struct iphdr));
    return EXIT_FAILURE;
  }
  struct iphdr* ip = (struct iphdr*)data;
  ip->tos = 0;
  ip->version = 4;
  ip->ihl = 5;
  ip->ttl = 255;
  ip->frag_off = htons(IP_DF);
  return EXIT_SUCCESS;
}

int
net_utils::setIntFlags(const char ifname[IFNAMSIZ], uint_fast32_t flags)
{
  int ret = EXIT_FAILURE;
  struct ifreq freq;
  int sofd = -1;
  if (!ifname) {
    ERROR("%s", "ifname is nullptr");
    goto fail;
  }

  sofd = socket(PF_INET, SOCK_DGRAM, 0);
  if (sofd < 0) {
    ERROR("%s", "Could not create socket");
    goto fail;
  }

  memset(&freq, 0x00, sizeof freq);
  strncpy(freq.ifr_ifrn.ifrn_name, ifname, IFNAMSIZ - 1);

  if (ioctl(sofd, SIOCGIFFLAGS, &freq) == -1) {
    ERROR("%s", "SIOCGIFFLAGS has failed");
    goto fail;
  }

  freq.ifr_ifru.ifru_flags |= flags;

  if (ioctl(sofd, SIOCSIFFLAGS, &freq) == -1) {
    ERROR("%s", "SIOCSIFFLAGS has failed");
    goto fail;
  }

  DBG("Succesfully set flags %u for %s", (unsigned)flags, ifname);
  ret = EXIT_SUCCESS;

fail:
  if (sofd >= 0) {
    close(sofd);
  }
  return ret;
}

int
net_utils::getIntFlags(const char ifname[IFNAMSIZ], uint_fast32_t* flags)
{
  int ret = EXIT_FAILURE;
  struct ifreq freq;
  int sofd = -1;

  if (!(ifname && flags)) {
    ERROR("%s", "ifname is nullptr");
    goto fail;
  }

  sofd = socket(PF_INET, SOCK_DGRAM, IPPROTO_IP);
  if (sofd < 0) {
    ERROR("%s", "Could not create socket");
    goto fail;
  }

  memset(&freq, 0x00, sizeof freq);
  strncpy(freq.ifr_name, ifname, IFNAMSIZ - 1);

  if (ioctl(sofd, SIOCGIFFLAGS, &freq) == -1) {
    ERROR("%s", "ioctl");
    ERROR("%s", "SIOCGIFFLAGS has failed");
    goto fail;
  }

  *flags = freq.ifr_ifru.ifru_flags;
  DBG("Succesfully got flags %u", (unsigned)*flags);
  ret = EXIT_SUCCESS;

fail:
  if (sofd >= 0) {
    close(sofd);
  }
  return ret;
}

int
net_utils::unsetIntFlags(const char ifname[IFNAMSIZ], uint_fast32_t flags)
{
  int ret = EXIT_FAILURE;
  struct ifreq freq;
  int sofd = -1;
  if (!ifname) {
    ERROR("%s", "ifname is nullptr");
    goto fail;
  }

  sofd = socket(PF_INET, SOCK_DGRAM, 0);
  if (sofd < 0) {
    ERROR("%s", "Could not create socket");
    goto fail;
  }

  memset(&freq, 0x00, sizeof freq);
  strncpy(freq.ifr_ifrn.ifrn_name, ifname, IFNAMSIZ - 1);

  if (ioctl(sofd, SIOCGIFFLAGS, &freq) == -1) {
    ERROR("%s", "SIOCGIFFLAGS has failed");
    goto fail;
  }

  freq.ifr_ifru.ifru_flags &= ~flags;

  if (ioctl(sofd, SIOCSIFFLAGS, &freq) == -1) {
    ERROR("%s", "SIOCSIFFLAGS has failed");
    goto fail;
  }

  DBG("Succesfully unset flags %u for %s", (unsigned)flags, ifname);
  ret = EXIT_SUCCESS;

fail:
  if (sofd >= 0) {
    close(sofd);
  }
  return ret;
}

int
net_utils::printIntFlags(uint_fast32_t flags)
{
  if (!flags) {
    DBG("%s", "flags is zero");
    return EXIT_FAILURE;
  }
  printf("\n");
  if (flags & IFF_UP) {
    printf("IFF_UP ");
  }
  if (flags & IFF_BROADCAST) {
    printf("IFF_BROADCAST ");
  }
  if (flags & IFF_DEBUG) {
    printf("IFF_DEBUG ");
  }
  if (flags & IFF_LOOPBACK) {
    printf("IFF_LOOPBACK ");
  }
  if (flags & IFF_POINTOPOINT) {
    printf("IFF_POINTOPOINT ");
  }
  if (flags & IFF_RUNNING) {
    printf("IFF_RUNNING ");
  }
  if (flags & IFF_NOARP) {
    printf("IFF_NOARP ");
  }
  if (flags & IFF_PROMISC) {
    printf("IFF_PROMISC ");
  }
  if (flags & IFF_NOTRAILERS) {
    printf("IFF_NOTRAILERS ");
  }
  if (flags & IFF_ALLMULTI) {
    printf("IFF_ALLMULTI ");
  }
  if (flags & IFF_MASTER) {
    printf("IFF_MASTER ");
  }
  if (flags & IFF_SLAVE) {
    printf("IFF_SLAVE ");
  }
  if (flags & IFF_MULTICAST) {
    printf("IFF_MULTICAST ");
  }
  if (flags & IFF_PORTSEL) {
    printf("IFF_PORTSEL ");
  }
  if (flags & IFF_AUTOMEDIA) {
    printf("IFF_AUTOMEDIA ");
  }
  if (flags & IFF_DYNAMIC) {
    printf("IFF_DYNAMIC ");
  }
  if (flags & IFF_LOWER_UP) {
    printf("IFF_LOWER_UP ");
  }
  if (flags & IFF_DORMANT) {
    printf("IFF_DORMANT ");
  }
  if (flags & IFF_ECHO) {
    printf("IFF_ECHO ");
  }
  printf("\n");
  return EXIT_SUCCESS;
}

int
net_utils::monitorMode(const char ifname[IFNAMSIZ])
{
  int ret = EXIT_FAILURE;
  uint_fast32_t flags{ 0 };
  struct iwreq iw;
  int sofd{ -1 };

  if (!ifname) {
    ERROR("%s", "Interface name is null");
    goto fail;
  }
  sofd = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
  if (sofd < 0) {
    ERROR("%s", "Could not create socket");
    goto fail;
  }
  if (net_utils::getIntFlags(ifname, &flags) != EXIT_SUCCESS) {
    ERROR("%s", "Could not get interface flags");
    goto fail;
  }
  if (net_utils::unsetIntFlags(ifname, IFF_UP) != EXIT_SUCCESS) {
    ERROR("%s", "Could not put interface down");
    goto fail;
  }
  memset(&iw, 0x00, sizeof iw);
  strncpy(iw.ifr_ifrn.ifrn_name, ifname, IFNAMSIZ - 1);
  iw.u.mode = IW_MODE_MONITOR;

  if (ioctl(sofd, SIOCSIWMODE, &iw) < 0) {
    ERROR("%s", "Could not set monitor mode.");
    goto fail;
  }
  if (net_utils::setIntFlags(ifname, flags) != EXIT_SUCCESS) {
    ERROR("%s", "Could not restore interface flags");
    goto fail;
  }

  ret = EXIT_SUCCESS;

fail:
  if (sofd >= 0) {
    close(sofd);
  }
  return ret;
}

int
net_utils::managedMode(const char ifname[IFNAMSIZ])
{
  int ret = EXIT_FAILURE;
  uint_fast32_t flags{ 0 };
  struct iwreq iw;
  int sofd{ -1 };

  if (!ifname) {
    ERROR("%s", "Interface name is null");
    goto fail;
  }
  sofd = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
  if (sofd < 0) {
    ERROR("%s", "Could not create socket");
    goto fail;
  }
  if (net_utils::getIntFlags(ifname, &flags) != EXIT_SUCCESS) {
    ERROR("%s", "Could not get interface flags");
    goto fail;
  }
  if (net_utils::unsetIntFlags(ifname, IFF_UP) != EXIT_SUCCESS) {
    ERROR("%s", "Could not put interface down");
    goto fail;
  }
  memset(&iw, 0x00, sizeof iw);
  strncpy(iw.ifr_ifrn.ifrn_name, ifname, IFNAMSIZ - 1);
  iw.u.mode = IW_MODE_INFRA;

  if (ioctl(sofd, SIOCSIWMODE, &iw) < 0) {
    ERROR("%s", "Could not set managed mode.");
    goto fail;
  }
  if (net_utils::setIntFlags(ifname, flags) != EXIT_SUCCESS) {
    ERROR("%s", "Could not restore interface flags");
    goto fail;
  }

  ret = EXIT_SUCCESS;

fail:
  if (sofd >= 0) {
    close(sofd);
  }
  return ret;
}

bool
net_utils::isValidMac(const uint8_t* hwaddr)
{
  if (!hwaddr) {
    ERROR("%s", "hwaddr is nullptr");
    return false;
  }

  if (hwaddr[0] == 0x00 && hwaddr[1] == 0x00 && hwaddr[2] == 0x00 &&
      hwaddr[3] == 0x00 && hwaddr[4] == 0x00 && hwaddr[5] == 0x00) {
    DBG("%s", "MAC is ZERO");
    return false;
  }

  if (hwaddr[0] == 0xFF && hwaddr[1] == 0xFF && hwaddr[2] == 0xFF &&
      hwaddr[3] == 0xFF && hwaddr[4] == 0xFF && hwaddr[5] == 0xFF) {
    DBG("%s", "MAC is BROADCAST");
    return false;
  }

  return true;
}

bool
net_utils::isValidIpV4(uint_fast32_t ip)
{
  if (ip == 0 || ip == (uint_fast32_t)-1) {
    DBG("IPv4 %u.%u.%u.%u is NOT VALID", IP_FORMAT(ip));
    return false;
  } else {
    DBG("IPv4 %u.%u.%u.%u is ok", IP_FORMAT(ip));
    return true;
  }
}

uint_fast8_t
net_utils::getIpV4Mask(uint_fast32_t ipA, uint_fast32_t ipB)
{
  int_fast8_t pos{ 31 };
  uint_fast8_t count{ 0 };
  while ((ipA & (1 << pos)) == (ipB & (1 << pos)) && pos >= 0) {
    pos--;
    count++;
  }
  return count;
}

uint_fast8_t
net_utils::convertIpV4Mask(uint_fast8_t* cidr, uint_fast32_t* extended)
{
  if (!(cidr && extended)) {
    ERROR(
      "Args are null cidr(%p) - extended(%p)", (void*)cidr, (void*)extended);
    return EXIT_FAILURE;
  }
  if (!(*cidr || *extended)) {
    ERROR("%s", "Both masks are zero");
    return EXIT_FAILURE;
  }

  int8_t pos{ 0 };
  if (*cidr == 0 && *extended) {
    pos = 31;
    while (pos >= 0 && *extended & (1 << pos)) {
      (*cidr)++;
      pos--;
    }
  } else if (*extended == 0 && *cidr <= 32) {
    pos = 1;
    while (pos <= *cidr) {
      *extended |= (1 << (32 - pos));
      pos++;
    }
  } else {
    ERROR("Invalid values cidr(%u) - extended(%u)",
          (unsigned)*cidr,
          (unsigned)*extended);
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}

int
net_utils::charToIpV4(const uint8_t ip[4], uint_fast32_t* numeric)
{
  if (!(ip && numeric)) {
    ERROR("%s", "Params are nullptr");
    return EXIT_FAILURE;
  }
  *numeric = ((uint8_t)ip[0]) << 24;
  *numeric |= ((uint8_t)ip[1]) << 16;
  *numeric |= ((uint8_t)ip[2]) << 8;
  *numeric |= ((uint8_t)ip[3]);
  DBG(
    "Numeric ip is %u - %u.%u.%u.%u", (unsigned)*numeric, IP_FORMAT(*numeric));
  return EXIT_SUCCESS;
}

int
net_utils::ipV4ToChar(uint8_t ip[4], uint_fast32_t numeric)
{
  if (!ip) {
    ERROR("%s", "Param is nullptr");
    return EXIT_FAILURE;
  }
  ip[3] = numeric & 0xFF;
  ip[2] = (numeric >> 8) & 0xFF;
  ip[1] = (numeric >> 16) & 0xFF;
  ip[0] = (numeric >> 24) & 0xFF;
  DBG("Numeric ip is %u - %u.%u.%u.%u", (unsigned)numeric, IP_FORMAT(numeric));
  return EXIT_SUCCESS;
}

int
net_utils::charToMac(const uint8_t mac[IFHWADDRLEN], uint_fast64_t* numeric)
{
  if (!(mac && numeric)) {
    ERROR("%s", "Args are null");
    return EXIT_FAILURE;
  }
  *numeric = (uint_fast64_t)mac[0] << 40;
  *numeric |= (uint_fast64_t)mac[1] << 32;
  *numeric |= (uint_fast64_t)mac[2] << 24;
  *numeric |= (uint_fast64_t)mac[3] << 16;
  *numeric |= (uint_fast64_t)mac[4] << 8;
  *numeric |= (uint_fast64_t)mac[5];
  return EXIT_SUCCESS;
}

int
net_utils::macToChar(uint8_t mac[IFHWADDRLEN], uint_fast64_t numeric)
{
  if (!(mac && numeric)) {
    ERROR("%s", "Args are null");
    return EXIT_FAILURE;
  }
  mac[0] = numeric >> 40;
  mac[1] = numeric >> 32;
  mac[2] = numeric >> 24;
  mac[3] = numeric >> 16;
  mac[4] = numeric >> 8;
  mac[5] = numeric;
  return EXIT_SUCCESS;
}

int
net_utils::getMyIps4(std::set<uint_fast32_t>& ips)
{
  struct sockaddr_in* sock4{ nullptr };
  struct ifaddrs *addrs{ nullptr }, *it{ nullptr };
  if (getifaddrs(&addrs) == -1) {
    ERROR("%s", "Could not get ifaddrs");
    return EXIT_FAILURE;
  }

  for (it = addrs; it != nullptr; it = it->ifa_next) {
    if (it->ifa_addr->sa_family == AF_INET && !(it->ifa_flags & IFF_LOOPBACK)) {
      sock4 = (struct sockaddr_in*)it->ifa_addr;
      ips.insert((uint_fast32_t)ntohl(sock4->sin_addr.s_addr));
      DBG("Pushed back %u.%u.%u.%u", IP_FORMAT_NO((sock4->sin_addr.s_addr)));
    }
  }

  freeifaddrs(addrs);
  return EXIT_SUCCESS;
}

int
net_utils::getMyIfNames(std::vector<std::string>& ifNames)
{
  struct ifaddrs *addrs{ nullptr }, *it{ nullptr };
  if (getifaddrs(&addrs) == -1) {
    ERROR("%s", "Could not get ifaddrs");
    return EXIT_FAILURE;
  }

  for (it = addrs; it != nullptr; it = it->ifa_next) {
    ifNames.emplace_back(it->ifa_name);
    DBG("Pushed back %s", it->ifa_name);
  }

  freeifaddrs(addrs);
  return EXIT_SUCCESS;
}

void
net_utils::printBanner(const std::string& msg, size_t width, char comm)
{
  printBanner(msg.c_str(), width, comm);
}

void
net_utils::printBanner(const char* msg, size_t width, char comm)
{
  size_t myWidth = width;
  if (myWidth == 0) {
    struct winsize windowSize;
    memset(&windowSize, 0x00, sizeof windowSize);
    if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &windowSize) == -1) {
      myWidth = 80;
    } else {
      myWidth = windowSize.ws_col;
    }
  }

  const size_t msgLen = strlen(msg) >= myWidth ? myWidth : strlen(msg);
  const size_t tempWidth = myWidth - (myWidth - msgLen) / 2 - msgLen - 2;

  // First line
  {
    printf("\n");
    for (size_t i = 0; i < myWidth; i++) {
      printf("%c", comm);
    }
    printf("\n");
  }

  // Text Line
  {
    printf("%c", comm);
    for (size_t i = 0; i < (myWidth - msgLen) / 2; i++) {
      printf("%c", ' ');
    }
    printf("%s", msg);
    for (size_t i = 0; i < tempWidth; i++) {
      printf("%c", ' ');
    }
    printf("%c", comm);
  }

  // End Line
  {
    printf("\n");
    for (size_t i = 0; i < myWidth; i++) {
      printf("%c", comm);
    }
    printf("\n");
  }
}

bool
net_utils::isValidFd(int fd)
{
  if (fcntl(fd, F_GETFD) == -1) {
    ERROR("%d is not valid!", fd);
    return false;
  } else {
    DBG("%d is a potentially valid fd", fd);
    return true;
  }
}

int
net_utils::redirectStream(int origFd, int newFd)
{
  int backupFd = -1;
  FILE* backupPtr{ nullptr };

  if (net_utils::isValidFd(origFd) == false) {
    ERROR("original FD(%d) is not valid", origFd);
    goto fail;
  }

  if (net_utils::isValidFd(newFd) == false) {
    ERROR("new FD(%d) is not valid", newFd);
    goto fail;
  }

  backupPtr = tmpfile();
  if (dup2(origFd, fileno(backupPtr)) < 0) {
    ERROR("%s", "dup2 from backup to original has failed");
    goto fail;
  }

  if (dup2(newFd, origFd) < 0) {
    ERROR("%s", "dup2 from original to new has failed");
    goto fail;
  }

  backupFd = fileno(backupPtr);

fail:
  if (backupFd == -1) { // ERROR
    if (backupPtr) {
      fclose(backupPtr);
    }
  }
  return backupFd;
}

int_fast8_t
net_utils::restoreStream(int Fd, int origFd)
{
  int_fast8_t ret = EXIT_FAILURE;

  if (net_utils::isValidFd(Fd) == false) {
    ERROR("Fd(%d) is not valid", Fd);
    goto fail;
  }

  if (net_utils::isValidFd(origFd) == false) {
    ERROR("original FD(%d) is not valid", origFd);
    goto fail;
  }

  if (dup2(origFd, Fd) < 0) {
    ERROR(
      "dup2 from Fd(%d) to its new/original value(%d) has failed", Fd, origFd);
    goto fail;
  }
  ret = EXIT_SUCCESS;

fail:
  return ret;
}

int
net_utils::stringToMac(const std::string& mac, uint8_t arr[IFHWADDRLEN])
{
  unsigned temp[IFHWADDRLEN]{ 0x00 };
  if (mac.length() != 17) {
    DBG("Incorect MAC length %zu", mac.length());
    return EXIT_FAILURE;
  }
  if (sscanf(mac.c_str(),
             "%x:%x:%x:%x:%x:%x",
             temp,
             temp + 1,
             temp + 2,
             temp + 3,
             temp + 4,
             temp + 5) == IFHWADDRLEN) {
    for (auto i = 0; i < IFHWADDRLEN; i++) {
      arr[i] = temp[i];
    }
    return EXIT_SUCCESS;
  } else {
    DBG("Invalid MAC %s", mac.c_str());
    return EXIT_FAILURE;
  }
}

int
net_utils::macToString(std::string& mac, const uint8_t arr[IFHWADDRLEN])
{
  char temp[18]{ 0x00 };
  if (net_utils::isValidMac(arr) == false) {
    DBG("%s", "MAC is invalid");
    return EXIT_FAILURE;
  }
  snprintf(temp,
           18,
           "%02X:%02X:%02X:%02X:%02X:%02X",
           unsigned(arr[0]),
           unsigned(arr[1]),
           unsigned(arr[2]),
           unsigned(arr[3]),
           unsigned(arr[4]),
           unsigned(arr[5]));
  mac = temp;
  return EXIT_SUCCESS;
}
