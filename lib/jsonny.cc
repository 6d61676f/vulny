#include "async_io.hpp"
#include "debug.hpp"
#include "handlers.hpp"
#include "jsonny.hpp"
#include "net_utils.hpp"
#include "sntlk.hpp"
#include <arpa/inet.h>
#include <fstream>

/**
 * @class sStealth
 * @brief Scenario used to listen for ARPs and generate a heatmap of the
 * network.@n Non-intrusive scenario
 */
class sStealth : public VulnyScenario
{
private:
  /** Number of seconds to listen for traffic */
  double _time{ 0.0 };
  /** File used to save the output of the scan. If empty stdout is used */
  std::string _outFile{ "" };
  /** Future value used to get the deployment return value */
  std::future<int_fast8_t> _ret;

private:
  /**
   * @brief Internal deploy function used to scan and redirect the output.
   *
   * @return
   *    -   EXIT_FAILURE
   *    -   EXIT_SUCCESS
   */
  int_fast8_t _deploy();

public:
  /**
   * @brief Constructor for Stealth Scenario that sets the propname and the sync
   * type
   *
   * @param[in] propname    Name of the JSON object
   * @param[in] sync    Deploy sync or async
   */
  sStealth(std::string propname, bool sync = false);
  virtual ~sStealth(); /**< Destructor that waits for the deployment to finish
                        */

  /**
   * @brief The 'stealth' JSON has an example in the README
   *
   * @param[in] val 'stealth' JSON object
   *
   * @return
   *    -   EXIT_FAILURE
   *    -   EXIT_SUCCESS
   */
  int_fast8_t parse(const Json::Value& val) override;

  /**
   * @brief Deploy method that either executes in sync or async.
   * @note If the scenario runs in async the actual return value of the
   * executions is obtained with wait.
   *
   * @return
   *    -   EXIT_FAILURE
   *    -   EXIT_SUCCESS
   */
  int_fast8_t deploy() override;

  /**
   * @brief If the deployment runs in async we must wait for it to finish in
   * order to check the status.
   *
   * @return
   *    -   EXIT_FAILURE
   *    -   EXIT_SUCCESS
   */
  int_fast8_t wait() override;
  friend std::ostream& operator<<(std::ostream& out, const class sStealth& me);
};

/**
 * @class sKnock
 * @brief Scenario used to port knock user specified IPs and Ports.
 * @note Intrusive
 */
class sKnock : public VulnyScenario
{
private:
  /**Name of the interface used for changing mac and/or IP*/
  std::string _intName{ "eno1" };

  /** New mac value, leave to zero for default */
  uint8_t _newMac[IFHWADDRLEN]{ 0 };

  /** New IP value, leave to zero for default */
  uint32_t _newIp{ 0 };

  /** File used to save the output of the scan. If empty stdout is used */
  std::string _outFile{ "" };

  /** Map used to keep the IPs and Ports for TCP Syn Scan@n
   * key - destination ip
   * value - set of destination ports
   */
  std::unordered_map<uint_fast32_t, std::set<uint16_t>> _tcpPorts;

  /** Map used to keep the IPs and Ports for UDP Port Scan@n
   * key - destination ip
   * value - set of destination ports
   */
  std::unordered_map<uint_fast32_t, std::set<uint16_t>> _udpPorts;

  /** Future value used to get the deployment return value */
  std::future<int_fast8_t> _ret;

private:
  /**
   * @brief Internal function used to parse TCP or UDP ports from a JSON object
   * and into the corresponding map
   *
   * @param[in] val 'knock' JSON object
   * @param[in] opt 'tcp' or 'udp' subobject name
   * @param[out] mappy Map in which we add the parsed values
   *
   * @return
   *    -   EXIT_FAILURE
   *    -   EXIT_SUCCESS
   */
  int_fast8_t _parsePorts(
    const Json::Value& val,
    const std::string& opt,
    std::unordered_map<uint_fast32_t, std::set<uint16_t>>& mappy);

  /**
   * @brief Internal deploy function used to port knock and redirect the output.
   *
   * @return
   *    -   EXIT_FAILURE
   *    -   EXIT_SUCCESS
   */
  int_fast8_t _deploy();

public:
  /**
   * @brief Constructor for Knock Scenario that sets the propname and the sync
   * type
   *
   * @param[in] propname    Name of the JSON object
   * @param[in] sync    Deploy sync or async
   */
  sKnock(std::string propname, bool sync = false);
  virtual ~sKnock(); /**<Destructor that waits for the deployment to finish */

  /**
   * @brief Deploy method that either executes in sync or async.
   * @note If the scenario runs in async the actual return value of the
   * executions is obtained with wait.
   *
   * @return
   *    -   EXIT_FAILURE
   *    -   EXIT_SUCCESS
   */
  int_fast8_t deploy() override;

  /**
   * @brief If the deployment runs in async we must wait for it to finish in
   * order to check the status.
   *
   * @return
   *    -   EXIT_FAILURE
   *    -   EXIT_SUCCESS
   */
  int_fast8_t wait() override;

  /**
   * @brief The 'knock' JSON has an example in the README
   *
   * @param[in] val 'knock' JSON object
   *
   * @return
   *    -   EXIT_FAILURE
   *    -   EXIT_SUCCESS
   */
  int_fast8_t parse(const Json::Value& val) override;
  friend std::ostream& operator<<(std::ostream& out, const class sKnock& me);
};

/**
 * @brief Audit Scenario class that combines both scan and knock features.
 *
 * The deployment of this scenario goes like so:
 * 1. Listen to ARPs, print HeatMap, get list of unused IPs, and list of IPs
 * from heatmap@n
 * 2. Change MAC is specified, change IP if specified ( 0 default IP, -1
 * Inferred - random from unused IPs, IPv4 )@n
 * 3. Knock each IP from heatmap both TCP and UDP from port 0 to port specified
 * in _maxPort@n
 * 4. Print Knock output@n
 * 5. Revert MAC and IP changes@n
 * @warning Very Intrusive - Looks like a Christmas Tree from Wireshark
 */
class sAudit : public VulnyScenario
{
private:
  /**Name of the interface used for changing mac and/or IP*/
  std::string _intName{ "eno1" };

  /** New mac value, leave to zero for default, -1 for random.
   * @warning Check if the first octet can be 0xff in a real case
   */
  uint8_t _newMac[IFHWADDRLEN]{ 0x00 };

  /** Flag that keeps if the MAC is to be randomized */
  bool _randMac{ false };

  /** New IP Value, leave to zero for default, -1 for inferred */
  uint32_t _newIp{ 0 };

  /** Number of seconds to listen for traffic */
  double _time{ 0 };

  /** File used to save the output of the scan. If empty stdout is used */
  std::string _outFile{ "" };

  /** Future value used to get the deployment return value */
  std::future<int_fast8_t> _ret;

  /** Max Port used to port scan the IPs found in traffic. Each IP shall get
   * knocked from zero to this for both UDP and TCP */
  const uint16_t _maxPort{ 128 };

private:
  /**
   * @brief Internal deploy function used to scan, knock, and redirect the
   * output.
   *
   * @return
   *    -   EXIT_FAILURE
   *    -   EXIT_SUCCESS
   */
  int_fast8_t _deploy(void);

public:
  /**
   * @brief Constructor for Audit Scenario that sets the propname and the sync
   * type
   *
   * @param[in] propname    Name of the JSON object
   * @param[in] sync    Deploy sync or async
   */
  sAudit(std::string propname, bool sync = false);

  virtual ~sAudit(); /**< Destructor that waits for the deployment to finish */

  /**
   * @brief Deploy method that either executes in sync or async.
   * @note If the scenario runs in async the actual return value of the
   * executions is obtained with wait.
   *
   * @return
   *    -   EXIT_FAILURE
   *    -   EXIT_SUCCESS
   */
  int_fast8_t deploy() override;

  /**
   * @brief If the deployment runs in async we must wait for it to finish in
   * order to check the status.
   *
   * @return
   *    -   EXIT_FAILURE
   *    -   EXIT_SUCCESS
   */
  int_fast8_t wait() override;

  /**
   * @brief The 'audit' JSON has an example in the README
   *
   * @param[in] val 'audit' JSON object
   *
   * @return
   *    -   EXIT_FAILURE
   *    -   EXIT_SUCCESS
   */
  int_fast8_t parse(const Json::Value& val) override;
  friend std::ostream& operator<<(std::ostream& os, const sAudit& other);
};

jSonny::jSonny(std::string file)
{
  this->_configFile = file;
  std::ifstream in(this->_configFile, std::ios::in);
  if (in) {
    in >> this->_jsonData;
  } else {
    throw VULNYEX("%s could not be opened", file.c_str());
  }
}

int_fast8_t
jSonny::parseValue(class jSonny::jVally& value)
{
  const std::string& propertyName = value.getName();
  if (propertyName.length() == 0) {
    return value.parse(this->_jsonData);
  } else {
    auto& _val = this->_jsonData[propertyName.c_str()];
    if (_val.isNull()) {
      DBG("Demanded JSON object is not available : %s", propertyName.c_str());
      return EXIT_FAILURE;
    } else {
      return value.parse(_val);
    }
  }
}

jSonny::jVally::jVally(std::string property)
{
  this->_typeName = property;
}

VulnyScenario::VulnyScenario(const std::string& propname, bool sync)
  : jSonny::jVally(propname)
  , _sync(sync)
{}

VulnyScenario*
VulnyScenario::build(VulnyScenario::ScanType type)
{
  switch (type) {
    case VulnyScenario::ScanType::Stealth: {
      return new sStealth(std::string("stealth"), false);
    }
    case VulnyScenario::ScanType::Knock: {
      return new sKnock(std::string("knock"), false);
    }
    case VulnyScenario::ScanType::Audit: {
      return new sAudit(std::string("audit"), false);
    }
    default:
      ERROR("Type not recognized: %d", int(type));
      return nullptr;
  }
}

sStealth::sStealth(std::string property, bool sync)
  : VulnyScenario(property, sync)
{}

sStealth::~sStealth()
{
  if (this->_ret.valid()) {
    ERROR("%s", "Joining Scenario deployment thread in destructor...not ok!");
    this->_ret.wait_for(std::chrono::milliseconds(500));
  }
}

int_fast8_t
sStealth::parse(const Json::Value& val)
{
  int ret = EXIT_SUCCESS;
  bool done = false;
  while (!done) {
    try {
      this->_time = val.get("time", _time).asDouble();
      this->_outFile = val.get("outfile", _outFile).asString();
      done = true;
    } catch (const std::exception& ex) {
      fprintf(stderr,
              "%s:%d caught:\n%s\n",
              __PRETTY_FUNCTION__,
              __LINE__,
              ex.what());
      ret = EXIT_FAILURE;
    }
  }
  return ret;
}

int_fast8_t
sStealth::_deploy()
{
  try {
    static std::mutex mutty;
    bool ret = true;
    FILE* fptr{ nullptr };
    int backupSTDOUT{ -1 };
    asyncIo& io = asyncIo::getInstance();
    ipSniffer2 sniffy;
    ret &= sniffy.registerSocket();
    // XXX Hardcoded
    ret &= io.pollDescriptors(this->_time * 60 * 1000);
    {
      std::lock_guard<std::mutex> lg(mutty);
      if (this->_outFile.empty() == false) {
        fptr = fopen(this->_outFile.c_str(), "a");

        if (!(fptr)) {
          DBG("Could not open file(%s) for writing", this->_outFile.c_str());
        } else {
          fflush(stdout);
          if ((backupSTDOUT =
                 net_utils::redirectStream(STDOUT_FILENO, fileno(fptr))) < 0) {
            ERROR("Redirecting to %s has failed", this->_outFile.c_str());
          }
        }
      }

      sniffy.printClients();
      fflush(stdout);

      if (fptr && backupSTDOUT >= 0) {
        fclose(fptr);
        if (net_utils::restoreStream(STDOUT_FILENO, backupSTDOUT) ==
            EXIT_FAILURE) {
          ERROR("%s", "Could not restore stdout to its original self");
        }
        close(backupSTDOUT);
      } else if (fptr) {
        fclose(fptr);
      }
      // XXX
      // Not calling unregister socket creates data races between
      // pollDescriptors and genericHandler destructor. We get in the situation
      // where the packHandler destructor has finished and we don't have the
      // vtable anymore thus leading to a crash!
      sniffy.unregisterSocket();
      if (ret) {
        return EXIT_SUCCESS;
      } else {
        return EXIT_FAILURE;
      }
    }
  } catch (std::exception& ex) {
    fprintf(
      stderr, "%s:%d caught:\n%s\n", __PRETTY_FUNCTION__, __LINE__, ex.what());
    return EXIT_FAILURE;
  }
}

int_fast8_t
sStealth::wait()
{
  if (this->_ret.valid()) {
    return this->_ret.get();
  } else {
    return EXIT_FAILURE;
  }
}

int_fast8_t
sStealth::deploy()
{
  this->_ret =
    std::async(std::launch::async, &sStealth::_deploy, std::ref(*this));
  if (this->_sync) { // Do shit synchronously
    DBG("%s", "Deploying synchronously");
    return this->_ret.get();
  } else { // Do sync async
    DBG("%s", "Deploying async");
    if (this->_ret.valid() == false) {
      DBG("%s", "Failed to properly deploy");
      return EXIT_FAILURE;
    } else {
      DBG("%s", "Properly deployed");
      return EXIT_SUCCESS;
    }
  }
}

sKnock::sKnock(std::string propname, bool sync)
  : VulnyScenario(propname, sync)
{}

sKnock::~sKnock()
{
  if (this->_ret.valid()) {
    ERROR("%s", "Joining Scenario deployment thread in destructor...not ok!");
    this->_ret.wait_for(std::chrono::milliseconds(500));
  }
}

int_fast8_t
sKnock::_parsePorts(
  const Json::Value& val,
  const std::string& opt,
  std::unordered_map<uint_fast32_t, std::set<uint16_t>>& mappy)
{
  auto ret = EXIT_FAILURE;
  auto done = false;
  while (!done) {
    try {
      auto category = val[opt.c_str()];
      if (category.isArray()) {
        for (const auto& entry : category) {
          if (entry.isObject()) {
            auto ip = entry.get("ip_dest", "0.0.0.0").asString();
            uint_fast32_t key{ 0 };
            if (inet_pton(AF_INET, ip.c_str(), &key) != 1 ||
                (key == 0 || key == (uint32_t)-1)) {
              ERROR("%s is invalid or zero", ip.c_str());
              continue;
            }
            key = ntohl(key);
            auto ports = entry["ports"];
            if (ports.isArray()) {
              for (const auto port : ports) {
                mappy[key].insert(port.asUInt());
              }
            } else {
              ERROR("%s", "\'ports\' is not array");
              continue;
            }
          } else {
            ERROR("%s", "entry is not array");
            continue;
          }
        }
      } else {
        ERROR("%s %s", opt.c_str(), " is not array");
      }
      done = true;
    } catch (const std::exception& ex) {
      fprintf(stderr,
              "%s:%d caught:\n%s\n",
              __PRETTY_FUNCTION__,
              __LINE__,
              ex.what());
      ret = EXIT_FAILURE;
    }
  }
  return ret;
}

int_fast8_t
sKnock::parse(const Json::Value& val)
{
  int ret = EXIT_SUCCESS;
  bool done = false;
  while (!done) {
    try {
      this->_intName = val.get("interface", _intName).asString();
      this->_outFile = val.get("outfile", _outFile).asString();
      this->_parsePorts(val, "tcp", this->_tcpPorts);
      this->_parsePorts(val, "udp", this->_udpPorts);
      auto newIp = val.get("newip", "").asString();
      auto newMac = val.get("newmac", "-").asString();
      if (newIp.empty() == false) {
        if (inet_pton(AF_INET, newIp.c_str(), &this->_newIp) == -1 ||
            net_utils::isValidIpV4(this->_newIp) == false) {
          this->_newIp = 0;
          ERROR("Invalid IP %s - using default", newIp.c_str());
        } else {
          DBG("Using IP %s for knock", newIp.c_str());
        }
      } else {
        DBG("%s", "Using default IP");
      }

      if (newMac != "-") {
        if (newMac.length() != 17) {
          ERROR("Invalid MAC length %zu", newMac.length());
        } else {
          if (net_utils::stringToMac(newMac, this->_newMac) != EXIT_SUCCESS) {
            ERROR("Invalid MAC address %s", newMac.c_str());
          }
        }
      } else {
        DBG("%s", "Using default MAC");
      }
      done = true;
    } catch (const std::exception& ex) {
      fprintf(stderr,
              "%s:%d caught:\n%s\n",
              __PRETTY_FUNCTION__,
              __LINE__,
              ex.what());
      ret = EXIT_FAILURE;
    }
  }
  return ret;
}

int_fast8_t
sKnock::_deploy()
{
  try {
    static std::mutex mutty;
    auto ret = true;
    const auto redirect = !this->_outFile.empty();
    auto restoreMac{ false };
    auto restoreIp{ false };
    int backupSTDOUT{ -1 };
    FILE* newSTDOUT{ nullptr };
    asyncIo& io = asyncIo::getInstance();
    std::future<int_fast8_t> tcpRet;
    std::future<int_fast8_t> udpRet;
    sntlk s;
    const struct sockaddr_in sockin
    {
      AF_INET, 24, { _newIp }, { 0x00 }
    };
    tcpV4Knocker tknok;
    tknok.registerSocket();
    udpV4Knocker uknok;
    uknok.registerSocket();
    icmpHandler icmp;
    icmp.registerSocket();

    if (net_utils::isValidMac(_newMac)) {
      if (net_utils::setHwAddr(_intName.c_str(), _newMac, false) ==
          EXIT_SUCCESS) {
        DBG("Changed %s MAC addr", _intName.c_str());
        restoreMac = true;
      } else {
        ERROR("%s", "Interface or MAC are invalid");
      }
    }

    if (net_utils::isValidIpV4(_newIp) == true) {
      if (s.addIp((const struct sockaddr*)&sockin,
                  if_nametoindex(_intName.c_str())) == EXIT_SUCCESS) {
        DBG("Succesfully added %u.%u.%u.%u. to %s",
            IP_FORMAT(_newIp),
            _intName.c_str());
        restoreIp = true;
      } else {
        ERROR("Could not add IP %u.%u.%u.%u to %s",
              IP_FORMAT_NO(_newIp),
              _intName.c_str());
      }
    } else {
      DBG("%s", "Using default IP");
    }

    auto randy = net_utils::getRandomGenerator();

    if (_tcpPorts.size()) {
      tcpRet = std::async(std::launch::deferred, [&]() -> int_fast8_t {
        for (const auto& i : _tcpPorts) {
          tknok.setIpDst(i.first);
          for (const auto& j : i.second) {
            tknok.setPortDst(j);
            tknok.setPortSrc(randy());
            tknok.sendSynKnock();
            io.pollDescriptors(500);
          }
        }
        return 0;
      });
    }

    if (_udpPorts.size()) {
      udpRet = std::async(std::launch::deferred, [&]() -> int_fast8_t {
        for (const auto& i : _udpPorts) {
          uknok.setIpDst(i.first);
          for (const auto& j : i.second) {
            uknok.setPortDst(j);
            uknok.setPortSrc(randy());
            uknok.sendUdpKnock();
            io.pollDescriptors(500);
          }
        }
        return 0;
      });
    }

    if (tcpRet.valid()) {
      DBG("%s", "Waiting for TCP sKnock to finish");
      tcpRet.get();
    } else {
      ret &= false;
    }

    if (udpRet.valid()) {
      DBG("%s", "Waiting for UDP sKnock to finish");
      udpRet.get();
    } else {
      ret &= false;
    }

    {
      std::lock_guard<std::mutex> lg(mutty);

      if (redirect) {
        newSTDOUT = fopen(this->_outFile.c_str(), "a");
        if (newSTDOUT) {
          fflush(stdout);
          backupSTDOUT =
            net_utils::redirectStream(STDOUT_FILENO, fileno(newSTDOUT));
          if (backupSTDOUT <= 0) {
            ERROR("Could not redirect stdout to %s", _outFile.c_str());
          }
        } else {
          ERROR("Could not open %s for writing\n", _outFile.c_str());
        }
      }

      io.pollDescriptors(1000);
      icmp.printPortUnreachable();
      tknok.printTcpResponses();
      uknok.printUdpResponses();
      fflush(stdout);

      if (redirect) {
        if (newSTDOUT && backupSTDOUT >= 0) {
          fclose(newSTDOUT);
          net_utils::restoreStream(STDOUT_FILENO, backupSTDOUT);
          close(backupSTDOUT);
        } else if (newSTDOUT) {
          fclose(newSTDOUT);
        }
      }

      if (restoreMac) {
        DBG("Restoring %s MAC address", _intName.c_str());
        net_utils::resetHwAddr(this->_intName.c_str());
      }

      if (restoreIp) {
        if (s.delIp((const struct sockaddr*)&sockin,
                    if_nametoindex(_intName.c_str())) == EXIT_SUCCESS) {
          DBG("Succesfully deleted %u.%u.%u.%u. from %s",
              IP_FORMAT_NO(_newIp),
              _intName.c_str());
        } else {
          ERROR("Could not delete IP %u.%u.%u.%u. from %s",
                IP_FORMAT_NO(_newIp),
                _intName.c_str());
        }
      }

      icmp.unregisterSocket();
      tknok.unregisterSocket();
      uknok.unregisterSocket();
    }

    if (ret) {
      return EXIT_SUCCESS;
    } else {
      return EXIT_FAILURE;
    }
  } catch (const std::exception& ex) {
    fprintf(
      stderr, "%s:%d caught:\n%s\n", __PRETTY_FUNCTION__, __LINE__, ex.what());
    return EXIT_FAILURE;
  }
}

int_fast8_t
sKnock::deploy()
{
  auto ret = EXIT_FAILURE;

  if (this->_sync) {
    DBG("%s", "About to launch in sync");
    ret = this->_deploy();
  } else {
    DBG("%s", "About to launch async");
    this->_ret =
      std::async(std::launch::async, &sKnock::_deploy, std::ref(*this));
    if (this->_ret.valid()) {
      ret = EXIT_SUCCESS;
    }
  }
  return ret;
}

int_fast8_t
sKnock::wait()
{
  if (this->_ret.valid()) {
    return this->_ret.get();
  } else {
    ERROR("%s", "The knock scenario has not been deployed or has failed");
    return EXIT_FAILURE;
  }
}

sAudit::sAudit(std::string propname, bool sync)
  : VulnyScenario(propname, sync)
{}

sAudit::~sAudit()
{
  if (this->_ret.valid()) {
    ERROR("%s", "Joining Scenario deployment thread in destructor...not ok!");
    this->_ret.wait_for(std::chrono::milliseconds(500));
  }
}

int_fast8_t
sAudit::parse(const Json::Value& val)
{
  int ret = EXIT_SUCCESS;
  bool done = false;
  while (!done) {
    try {
      this->_time = val.get("time", _time).asDouble();
      this->_outFile = val.get("outfile", _outFile).asString();
      this->_intName = val.get("interface", _intName).asString();
      auto newMac = val.get("newmac", "").asString();
      auto newIp = val.get("newip", "").asString();
      if (newIp.size() > 6) {
        if (inet_pton(AF_INET, newIp.c_str(), &this->_newIp) == -1 ||
            net_utils::isValidIpV4(this->_newIp) == false) {
          ERROR("Invalid new IP %s", newIp.c_str());
          this->_newIp = 0;
        } else {
          DBG("Using new IP %s", newIp.c_str());
        }
      } else if (newIp == "-1") {
        DBG("%s", "Using inferred IP");
        this->_newIp = -1;
      } else {
        DBG("%s", "Using default IP");
      }

      if (newMac == "-1") {
        DBG("%s", "Using random MAC");
        this->_randMac = true;
      } else if (newMac.size() == 17) {
        if (net_utils::stringToMac(newMac, this->_newMac) == EXIT_SUCCESS) {
          DBG("Using user supplied MAC %s", newMac.c_str());
        } else {
          ERROR("User supplied MAC is invalid %s", newMac.c_str());
        }
      } else {
        DBG("%s", "Using Default Mac");
      }

      done = true;
    } catch (const std::exception& ex) {
      fprintf(stderr,
              "%s:%d caught:\n%s\n",
              __PRETTY_FUNCTION__,
              __LINE__,
              ex.what());
      ret = EXIT_FAILURE;
    }
  }
  return ret;
}

int_fast8_t
sAudit::deploy()
{
  if (this->_sync) {
    DBG("%s", "Deploying in sync");
    return this->_deploy();
  } else {
    this->_ret =
      std::async(std::launch::async, &sAudit::_deploy, std::ref(*this));
    if (this->_ret.valid()) {
      DBG("%s", "Deployed async");
      return EXIT_SUCCESS;
    } else {
      DBG("%s", "Could not properly deploy");
      return EXIT_FAILURE;
    }
  }
}

int_fast8_t
sAudit::_deploy()
{
  static std::mutex mutty;
  bool ret{ true };
  FILE* newSTDOUT{ nullptr };
  int backupSTDOUT{ -1 };
  auto& io = asyncIo::getInstance();
  std::map<uint_fast32_t, uint_fast64_t> macs;
  std::set<uint_fast32_t> unusedIps;
  const auto redirect = !this->_outFile.empty();
  // Start of sStealth Scan
  try {
    ipSniffer2 sniffy;
    sniffy.registerSocket();
    arPing arpy(this->_intName);

    io.pollDescriptors(this->_time * 60 * 1000);

    {
      std::lock_guard<std::mutex> lg(mutty);
      if (redirect) {
        if (!(newSTDOUT)) {
          DBG("Could not open file(%s) for writing", this->_outFile.c_str());
        } else {
          fflush(stdout);
          if ((backupSTDOUT = net_utils::redirectStream(
                 STDOUT_FILENO, fileno(newSTDOUT))) < 0) {
            ERROR("Redirecting to %s has failed", this->_outFile.c_str());
          }
        }
      }

      sniffy.printClients();
      fflush(stdout);

      if (redirect) {
        if (newSTDOUT && backupSTDOUT >= 0) {
          fclose(newSTDOUT);
          if (net_utils::restoreStream(STDOUT_FILENO, backupSTDOUT) ==
              EXIT_FAILURE) {
            ERROR("%s", "Could not restore stdout to its original self");
          }
          close(backupSTDOUT);
        } else if (newSTDOUT) {
          fclose(newSTDOUT);
        }
      }
    }
    backupSTDOUT = { -1 };
    newSTDOUT = { nullptr };

    sniffy.unregisterSocket();
    macs = sniffy.getArps();

    if (_newIp == (decltype(_newIp)) - 1) { // Infer Ip
      sniffy.getUnusedIps(unusedIps, arpy);
    }
  } catch (const std::exception& ex) { // End of sStealth Scan
    fprintf(
      stderr, "%s:%d caught:\n%s\n", __PRETTY_FUNCTION__, __LINE__, ex.what());
    ret &= false;
  }

  // Start of sKnock
  try {
    std::thread tcpRet;
    std::thread udpRet;
    tcpV4Knocker tknok;
    tknok.registerSocket();
    udpV4Knocker uknok;
    uknok.registerSocket();
    icmpHandler icmp;
    icmp.registerSocket();
    auto restoreMac{ false };
    auto deleteIp{ false };
    auto randy = net_utils::getRandomGenerator();
    sntlk s;
    struct sockaddr_in sockin;

    if (_randMac) {
      if (net_utils::setHwAddr(_intName.c_str(), _newMac, _randMac) ==
          EXIT_SUCCESS) {
        DBG("Changed %s MAC addr", _intName.c_str());
        restoreMac = true;
      } else {
        ERROR("%s", "Interface is invalid");
      }
    } else if (net_utils::isValidMac(_newMac)) {
      if (net_utils::setHwAddr(_intName.c_str(), _newMac, false) ==
          EXIT_SUCCESS) {
        DBG("Changed %s MAC addr", _intName.c_str());
        restoreMac = true;
      } else {
        ERROR("%s", "Interface or MAC are invalid");
      }
    }

    if (_newIp == (decltype(_newIp)) - 1) {
      if (unusedIps.size() > 0) {
        auto rand = randy() % (unusedIps.size());
        auto it = unusedIps.begin();
        for (decltype(rand) i = 0; i < rand; i++) {
          it++;
        }
        _newIp = *it;
      } else {
        DBG("%s", "Not enough sniffed IPs...using default");
        _newIp = 0;
      }
    }

    if (net_utils::isValidIpV4(_newIp)) {
      sockin = { AF_INET, 24, { _newIp }, { 0x00 } };
      if (s.addIp((const struct sockaddr*)&sockin,
                  if_nametoindex(_intName.c_str())) == EXIT_SUCCESS) {
        DBG("Succesfully added %u.%u.%u.%u. to %s",
            IP_FORMAT_NO(_newIp),
            _intName.c_str());
        // NOTE Look into this...
        // tknok.setIpSrc(ntohl(_newIp));
        // uknok.setIpSrc(ntohl(_newIp));
        deleteIp = true;
      } else {
        ERROR("Could not add IP %u.%u.%u.%u. to %s",
              IP_FORMAT_NO(_newIp),
              _intName.c_str());
      }
    }

    if (macs.size()) {

      tcpRet = std::thread([&] {
        for (const auto& i : macs) {
          tknok.setIpDst(i.first);
          for (auto j = 0; j < _maxPort; j++) {
            tknok.setPortSrc(randy());
            tknok.setPortDst(j);
            tknok.sendSynKnock();
            io.pollDescriptors(150);
          }
        }
      });

      udpRet = std::thread([&] {
        for (const auto& i : macs) {
          uknok.setIpDst(i.first);
          for (auto j = 0; j < _maxPort; j++) {
            uknok.setPortSrc(randy());
            uknok.setPortDst(j);
            uknok.sendUdpKnock();
            io.pollDescriptors(150);
          }
        }
      });

      if (tcpRet.joinable()) {
        DBG("%s", "Waiting for TCP sKnock to finish");
        tcpRet.join();
      } else {
        ret &= false;
      }

      if (udpRet.joinable()) {
        DBG("%s", "Waiting for UDP sKnock to finish");
        udpRet.join();
      } else {
        ret &= false;
      }
    }

    {
      std::lock_guard<std::mutex> lg(mutty);

      if (redirect) {
        newSTDOUT = fopen(this->_outFile.c_str(), "a");
        if (newSTDOUT) {
          fflush(stdout);
          backupSTDOUT =
            net_utils::redirectStream(STDOUT_FILENO, fileno(newSTDOUT));
          if (backupSTDOUT <= 0) {
            ERROR("Could not redirect stdout to %s", _outFile.c_str());
          }
        } else {
          ERROR("Could not open %s for writing\n", _outFile.c_str());
        }
      }

      io.pollDescriptors(1000);
      icmp.printPortUnreachable();
      tknok.printTcpResponses();
      uknok.printUdpResponses();
      uknok.printUdpUnreachable(icmp);
      fflush(stdout);

      if (redirect) {
        if (newSTDOUT && backupSTDOUT >= 0) {
          fclose(newSTDOUT);
          net_utils::restoreStream(STDOUT_FILENO, backupSTDOUT);
          close(backupSTDOUT);
        } else if (newSTDOUT) {
          fclose(newSTDOUT);
        }
      }

      icmp.unregisterSocket();
      tknok.unregisterSocket();
      uknok.unregisterSocket();
    }

    if (restoreMac) {
      DBG("Restoring %s MAC address", _intName.c_str());
      net_utils::resetHwAddr(this->_intName.c_str());
    }

    if (deleteIp) {
      DBG("Deleting Ip %u.%u.%u.%u", IP_FORMAT_NO(_newIp));
      if (s.delIp((const struct sockaddr*)&sockin,
                  if_nametoindex(_intName.c_str())) == EXIT_SUCCESS) {
        DBG("Succesfully deleted %u.%u.%u.%u from %s",
            IP_FORMAT_NO(_newIp),
            _intName.c_str());
      } else {
        ERROR("Could not delete IP %u.%u.%u.%u from %s",
              IP_FORMAT_NO(_newIp),
              _intName.c_str());
      }
    }

  } catch (const std::exception& ex) { // End of sKnock
    fprintf(
      stderr, "%s:%d caught:\n%s\n", __PRETTY_FUNCTION__, __LINE__, ex.what());
    ret &= false;
  }

  if (ret) {
    return EXIT_SUCCESS;
  } else {
    return EXIT_FAILURE;
  }
}

int_fast8_t
sAudit::wait()
{
  auto ret = EXIT_SUCCESS;
  if (this->_ret.valid()) {
    ret = this->_ret.get();
  } else {
    ERROR("%s", "Cannot wait for scenario...not deployed");
    ret = EXIT_FAILURE;
  }
  return ret;
}

std::ostream&
operator<<(std::ostream& out, const class sStealth& me)
{
  out << "Dumping sStealth Object\n";
  out << "outfile: " << me._outFile << ", time: " << me._time;
  out << "\n";
  return out;
}

std::ostream&
operator<<(std::ostream& out, const class sKnock& me)
{
  out << "DUMPING sKnock Object\n";
  out << "intname: " << me._intName << ", outfile: " << me._outFile
      << ", newMac: " << me._newMac << ", newIp: " << me._newIp << "\n";
  out << "\nTCP:\n";
  for (auto& i : me._tcpPorts) {
    out << "IP Dst: " << i.first << ":\n";
    for (auto& j : i.second) {
      out << j << " ";
    }
    out << "\n";
  }
  out << "\nUDP:\n";
  for (auto& i : me._udpPorts) {
    out << "IP Dst: " << i.first << ":\n";
    for (auto& j : i.second) {
      out << j << " ";
    }
  }
  out << "\n";
  return out;
}

std::ostream&
operator<<(std::ostream& os, const sAudit& other)
{
  os << "Dumping sAudit Object\n";
  os << "intname: " << other._intName << ", outfile: " << other._outFile
     << ", newMac: ";
  for (auto i = 0; i < 6; i++) {
    os << std::hex << (unsigned)other._newMac[i] << ":";
  }

  os << "\nrandMac: " << other._randMac << ", newIp: " << other._newIp
     << ", time: " << other._time;
  os << "\n";
  return os;
}
