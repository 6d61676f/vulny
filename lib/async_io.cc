#include "async_io.hpp"
#include <algorithm>
#include <chrono>
#include <cstring>
#include <fcntl.h>
#include <thread>

asyncIo::asyncIo(size_t qSize)
  : _qSize(qSize)
  , _recvEvents(new struct epoll_event[qSize])
{
  this->_pollFd = epoll_create1(0);
  if (_pollFd < 0) {
    throw VULNYEX("%s", "epoll_create has failed");
  }
  // NOTE Works in c++14, otherwise user list constructor
  // this->_recvEvents = std::make_unique<struct epoll_event[]>(_qSize);
}

asyncIo&
asyncIo::getInstance(void)
{
  static asyncIo inst(10);
  return inst;
}

int_fast8_t
asyncIo::prepareSocket(int sofd)
{
  int flags = -1;

  if (sofd < 0) {
    ERROR("%s", "sofd is invalid");
    return EXIT_FAILURE;
  }

  flags = fcntl(sofd, F_GETFL);
  if (flags >= 0 && fcntl(sofd, F_SETFL, flags | O_NONBLOCK) >= 0) {
    DBG("O_NONBLOCKed %d", sofd);
    return EXIT_SUCCESS;
  } else {
    ERROR("%s", "fcntl has failed");
    return EXIT_FAILURE;
  }
}

int_fast8_t
asyncIo::registerDescriptor(int sofd,
                            uint_fast32_t events,
                            class packHandler* pack)
{
  std::lock_guard<std::mutex> lg(this->_lock);
  if (pack == nullptr) {
    ERROR("%s", "Callback class in null");
    return EXIT_FAILURE;
  }

  if (events == 0) {
    ERROR("%s", "Events not specified");
    return EXIT_FAILURE;
  }

  if (asyncIo::prepareSocket(sofd) == EXIT_FAILURE) {
    ERROR("%s", "Could not prepare socket");
    return EXIT_FAILURE;
  }

  struct epoll_event ev;
  struct Callback* p = new struct Callback(sofd, pack);
  if (p == nullptr) {
    ERROR("%s", "Could not allocate data for callback structure");
    return EXIT_FAILURE;
  }
  memset(&ev, 0x00, sizeof ev);
  ev.data.ptr = p;
  ev.events = events;
  if (epoll_ctl(this->_pollFd, EPOLL_CTL_ADD, sofd, &ev) != 0) {
    ERROR("%s", "Could not add sofd to epoll");
    return EXIT_FAILURE;
  }

  if (this->_userEvents.find(sofd) != std::end(_userEvents)) {
    ERROR("sofd(%d) is already present in our registered events. THIS IS BAD, "
          "IT MEANS THAT THE DESCRIPTOR HAS BEEN CLOSED BEFORE BEING "
          "UNREGISTERED.",
          sofd);
    delete (struct Callback*)_userEvents[sofd].data.ptr;
  }

  _userEvents[sofd] = ev;

  DBG("Registered sofd(%d) - events(%u) - pointer(%p)",
      sofd,
      ev.events,
      ev.data.ptr);

  return EXIT_SUCCESS;
}

int_fast8_t
asyncIo::unregisterDescriptor(int sofd)
{
  std::lock_guard<std::mutex> lg(this->_lock);

  if (epoll_ctl(_pollFd, EPOLL_CTL_DEL, sofd, nullptr) != 0) {
    ERROR("Could not remove %d from epoll", sofd);
    return EXIT_FAILURE;
  }

  const auto& it = _userEvents.find(sofd);
  if (it == _userEvents.end()) {
    ERROR("%d not found", sofd);
    return EXIT_FAILURE;
  } else {
    delete (struct Callback*)it->second.data.ptr;
    _userEvents.erase(it);
    DBG("Succesfully removed fd(%d)", sofd);
    return EXIT_SUCCESS;
  }
}

int_fast8_t
asyncIo::pollDescriptors(int timeout)
{
  std::unique_lock<std::mutex> lg(this->_lock, std::defer_lock);
  if (lg.try_lock() == false) {
    DBG("%s", "Already processing");
    return EXIT_SUCCESS;
  } else {
    if (_userEvents.empty()) {
      return EXIT_SUCCESS;
    }
    struct epoll_event* events = _recvEvents.get();
    auto done = false;
    const auto adjustedTimeout = timeout % 7907 + 127;
    auto start = std::chrono::high_resolution_clock::now();
    while (!done) {
      ssize_t fds = epoll_wait(_pollFd, events, _qSize, adjustedTimeout);
      if (fds == -1) {
        ERROR("%s", "epoll_wait has failed");
        return EXIT_FAILURE;
      } else if (fds == 0) {
        DBG("%s %d", "epoll_wait has timed-out", adjustedTimeout);
      } else {
        struct Callback* call = nullptr;
        class packHandler* p = nullptr;
        for (ssize_t i = 0; i < fds; i++) {
          call = (struct Callback*)events[i].data.ptr;
          if (call == nullptr) {
            ERROR("%s", "Callback handler is null...wrong");
            continue;
          } else {
            p = call->_handler;
            DBG("Registered sofd(%d) - events(%u) - callback(%p) - "
                "packhandler(%p)",
                call->_sofd,
                events[i].events,
                (void*)call,
                (void*)p);
            p->process(call->_sofd, events[i].events);
          }
        }
      }
      auto stop = std::chrono::high_resolution_clock::now();
      auto milis =
        std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
      if (milis.count() >= timeout) {
        DBG("%s %u",
            "Done processing for the specified timeout",
            (unsigned)milis.count());
        done = true;
      } else {
        DBG("%s %d", "About to poll again for", adjustedTimeout);
      }
    }
    return EXIT_SUCCESS;
  }
}

asyncIo::~asyncIo()
{
  // NOTE Should we close an epoll fd?
  if (_pollFd) {
    close(_pollFd);
  }
  for (const auto i : _userEvents) {
    delete (struct Callback*)i.second.data.ptr;
  }
}
