# vulny
###Network security scanner for small linux platforms.
We currently have implemented:

   - **asyncIo** library for async file descriptor handling via a *packHandler* class
   - **net_utils** library that can:
       - print basic info about TCP/IP packets
       - set/get/unset interface flags
       - put wifi in managed or monitor
       - check IPv4 address to be valid
       - check MAC to be valid
       - convert MAC char array to uint_fast64_t and back
       - defined macro to print IPv4
       - get currently configured IPv4 addresses
       - calculate checksum for packets
       - prepare arp request
       - get/set/reset MAC address for interface
       - convert IPv4 mask from numeric to extended or viceversa
       - given two IPv4s obtain common mask
       - populate buffer with sane IPv4 header defaults
       - compute UDP/TCP checksum with pseudoheader
       - pseudornd object builder
       - banner printing function
   - **handlers** library in which we define and implement *packHandlers*. We currently have:
       - *genericV4Handler* - declare helper functions and IPv4 related members, buffers, locks
       - *arPing* - arp request for given IPs
       - *ipSniffer2* - check for ARP packets and save IPs and MACs in order to create heatmap => obtain possible gateway + network address
       - *icmpHandler* - send ICMP ping, handle ICMP REPLY and PORT UNREACHABLE
       - *ipSniffer* - simillar to *ipSniffer2*, but just prints packets and saves `struct ether_arp`s in a vector
       - *l2Handler* - dumps all received packets to stdout using the print functions defined in *net_utils*
       - *udpV4Knocker* - udp packet crafter and handler
       - *tcpV4Knocker* - tcp syn packet crafter and handler
   - **sntlk** small *NETLINK* library for kernel communication in order to access the routing table and add/delete/show IP addresses
   - **jSonny** library that creates scenario deployments based on json files.
       - *jSonny* class that opens a user provided json file and defines a *jVally* interface to handle a specific property.
       - *jVally* interface class that offers `parse`. It's designed to be inherited by the user in order to define specific property parsing behaviour.
       - *VulnyScenario* intermediate interface over *jVally* that offers the `deploy` method that actually handles all the properties from the json object and applies the proper networking calls.
       - *sStealth* class that implements the stealth scenario: listens to ARP traffic and prints the results.
       - *sKnock* class that implements the knocking scenario: gets the list of IPs and Ports from JSON, changes MAC/IP, prints results.
       - *sAudit* class that implements the auditing scenario: scan + knock.


###TODO:
  - [ ] **python interface via swig or something similar**
  - - at this moment this doesn't really work
  - - i'm considering implementing a control function over all the libraries that would use some sort of IPC with python(*all hail sockets*)
  - [-] **tcp handler**
  - - [?] *connect* scan
  - - [x] *SYN* scan
  - [-] **small NETLINK library**
  - - [x] add IP address to interface
  - - [x] delete IP address from interface
  - - [?] show ip addresses for interface - better implement in *net_utils* using `getifaddrs`
  - - [x] get Route for given IP destination
  - [x] add source ip spoof option for icmpHandler - basically IP_HDRINCL and leave it zero if not intending to spoof
  - [x] test algorithm for infering possible network address in *ipSniffer2*
  - [x] clean a bit *arPing*
  - [?] handler for radiotap packets => implies support in *net_utils*
  - - this currently is quite a hassle to implement with a minimum of result
  - - we can see the access points and their power/channel/country - not really that useful
  - - radiotap/802.11 structures must be implemented by hand and they also have TLVs in them
  - [-] clean *main*
  - [x] move/update MAC parsing command from *newMain* to *net_utils*.
  - [x] have *Knock* inherit from *VulnyScenario* and create the actual deployment
  - [x] move file descriptor redirection from *Stealth::_deploy* to a separate function
  - [?] break *jsonny* into two separate libraries...one for parsing and the other for deployment
  - [-] **start documenting**
  - - [x] async_io
  - - [x] net_utils
  - - [x] handlers
  - - [x] debug
  - - [x] sntlk
  - - [x] jsonny

